/*
Template untuk data peserta, data nantinya bisa berupa JSON
Berisi ID, Nama, NIM, Jurusan, URL image
dan Latitude dan Longitude Alamat
 */
	$(document).ready(function(){	
		var list_data= 
				{
				'post':[{
					'title':'Selamat Anda Baru Memulai Day 0',
					'url':'#',
					'category':'pengumuman',
					'date':'20 June 2013'
				},{
					'title':'Selamat kepada Kelompok 8',
					'url':'#',
					'category':'apresiasi',
					'date':'20 June 2013'
				},{
					'title':'Tugas Day 8',
					'url':'#',
					'category':'tugas',
					'date':'20 June 2013'
				},{
					'title':'Tugas Buku Sparta',
					'url':'#',
					'category':'tugas',
					'date':'20 June 2013'
				},{
					'title':'How to use Git',
					'url':'#',
					'category':'pengumuman',
					'date':'20 June 2013'
				}
				]};

		var list_template = document.getElementById('list_template').innerHTML;
		var list_render = Mustache.to_html(list_template, list_data);	
		$('#sidebar-list').html(list_render);
	});