/*
Template untuk data peserta, data nantinya bisa berupa JSON
Berisi ID, Nama, NIM, Jurusan, URL image
dan Latitude dan Longitude Alamat
 */
	$(document).ready(function(){	
		var profile_data= 
				{
					"id":1,
					"nama":"Prama Aditya",
					"nim":16511229,
					"jurusan":"Teknik Informatika",
					"image-url":"images/profile/1.jpg",
					"map":
						{
							"latitude":-6.893196008669691,
							"longitude":107.61091232299805
						}
				};

		var profile_template = document.getElementById('profile_template').innerHTML;
		var profile_render = Mustache.to_html(profile_template, profile_data);	
		$('#profile-container').html(profile_render);
		
		var map_template = document.getElementById('map_template').innerHTML;
		var map_render = Mustache.to_html(map_template, profile_data);
		$('#map-container').html(map_render);
	});