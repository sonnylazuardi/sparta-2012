/*

Ga Akan dipake nantinya karena akan pake waktu server

 */
function updateClock ( )
	{
	  var currentTime = new Date ( );

	  var currentHours = currentTime.getHours ( );
	  var currentMinutes = currentTime.getMinutes ( );
	  var currentSeconds = currentTime.getSeconds ( );

	  // Pad the minutes and seconds with leading zeros, if required
	  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
	  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

	  // Compose the string for display
	  var currentTimeString = currentHours + ":" + currentMinutes;
	  var currentSecondString = currentSeconds;
	  
	  // Update the time display
	  document.getElementById("hour-minute").firstChild.nodeValue = currentTimeString;
	  document.getElementById("second").firstChild.nodeValue = currentSecondString;
	}