SPARTA HMIF 2012 Web
================================

Currently can be accessed at : http://spartahmif2012.web.id/

##Development Requirements

- Apache
- MySQL
- Yii 1.1.13

##Application Requirements

- Web Browser (Google Chrome, Mozilla Firefox, etc)
- Some features might not working if running in IE (You've been warned :p)

##Configuration

- Change /protected/config/db.php.example to /protected/config/db.php (with your own configuration)

Reference : http://stackoverflow.com/questions/2965060/gitignore-doesnt-ignore-files

##Version List - Version 1.0.2 beta (as 26 June 2013)

//DONE
v 1.0.2

*Participant

- Discus-like upload feature for participants (individual, group, and all participants)
- Upload profile picture

v 1.0.1

*Participant

- Showing all participant list
- Profile page for each participants
- Showing Poin in widget chart

*Admin

- Edit, create, and delete Poin

v 1.0.0

*Participant

- Clock
- Profile picture from Gravatar
- Address map
- Newest post

*Admin

- Edit, create and delete Post
- Download participants info in XLS format


//NEED TO RE-CHECK

- Styling (especially in Poin management)
- /pesertaTugas/ path (why this path is available for participant?)
- Genta : Explaining SITEMAP for tugas & pesertaTugas


//TODO

- Downloadable storage file (Prama)
- Admin main interface (Prama)
- Adding Achievement and Pelanggaran list (Iskandar)
- Showing participant tasks (Genta)
- Limiting upload file (Evan) -> done?
- SPARTA timeline ; chart-based? (Prama)
- Showing achievement list of each participant (Prama)


Secondary Req:

- Feedback form for participant (Sonny)
- Subscribe RSS from portal (Sonny)

##SiteMap (Useful only)

*Admin only


ADMIN

admin/index *  : showing poin chart and post list


SITE

site/login		: login page

site/register	: register page

site/contact    : contact page (is this page usable?)


PESERTA

peserta/index   : participant homepage

peserta/list    : showing participant list


POIN

poin/index      : showing poin chart

poin/view       : showing poin chart

poin/create *   : add new poin entry

poin/list   *   : showing all entries (for delete management)


POST

post/index      : showing post list

post/view       : showing specific post

post/create *   : add new post

post/update *   : update specific post


TUGAS

tugas/index     : showing tugas list

tugas/create *  : add new tugas specification

tugas/update *  : update specific tugas


##Contributor (Alphabetical Order)

//Front-end and Back-end

- Faiz Ilham Muhammad
- Genta Indra Winata
- Ignatius Evan Daryanto
- Iskandar Setiadi ( https://github.com/freedomofkeima )
- Prama Aditya Putra
- Sonny Lazuardi Hermawan


//Other contributors (requirement, deploy, etc)

- Aria Dhanang Dewangga
- Danang Arbansa
- Pandu Kartika Putra
- Rakaputra


//Special thanks to

- Panitia SPARTA HMIF 2012

##Copyright

Copyright (c) 2013. Tim Web SPARTA HMIF 2012.
