<?php 
$peserta =  Akun::model()->findByPk(Yii::app()->user->id)->peserta;
if (Yii::app()->user->checkAccess('admin'))
  $menu='Peserta'; 
else
  $menu='Friends';

 ?>
<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <meta name="description" content="Website Sparta HMIF 2012" />
  <meta name="keywords" content="Sparta, HMIF, 2012, ITB, Ospek, Jurusan, Informatika" />
  <meta name="author" content="Tim Web Sparta" />
  <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/timeline.css" />
  <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/style.css" />
  <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
  <script src="<?php echo Yii::app()->baseUrl ?>/js/fastclick.js"></script>  
  <script src="<?php echo Yii::app()->baseUrl ?>/js/list.min.js"></script>
  <script src="<?php echo Yii::app()->baseUrl ?>/js/maplace.min.js"></script>
</head>

<body id="home">
<div class="bg"></div>


<!-- Navigation -->
  <nav class="top-bar fixified">
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        
        <h1>
          <a href="<?php echo Yii::app()->createUrl('/site/index') ?>">
            Sparta HMIF
          </a>
        </h1>
        
      </li>
      <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>
 
    <section class="top-bar-section">
      <!-- Right Nav Section -->

      <ul class="right">
        <li class="divider"></li>
        <?php if (Yii::app()->user->checkAccess('admin')): ?>
          <li ><a href="<?php echo Yii::app()->createUrl('/site/index') ?>"><span data-icon="&#xe007" class="bigger" aria-hidden="true">Home</span></a></li>
          <li class="divider"></li>
        <?php endif ?>
        <li><a href="<?php echo Yii::app()->createUrl('/post/index') ?>"><span data-icon="&#xe008" class="bigger" aria-hidden="true">News</span></a></li>
        <li class="divider"></li>
        <li><a href="<?php echo Yii::app()->createUrl('/peserta/list') ?>"><span data-icon="&#xe009" class="bigger" aria-hidden="true"><?php echo $menu ?></span></a></li>
        
        <?php if (Yii::app()->user->checkAccess('admin')): ?>
            <li>
                <?php echo CHtml::link('Logout', array('/site/logout'), array('class'=>'button alert')) ?>
            </li>
        <?php else: ?>
          <li class="divider"></li>
          <li class="has-dropdown"><a href="#"><img src="<?php echo $peserta->profpicImage ?>" class="menu-image" alt=""><?php echo $peserta->nama_lengkap ?></a>
            <ul class="dropdown">
               <li>
                <?php echo CHtml::link('Edit Profile', array('/peserta/profil')) ?>
              </li>
              <li class="divider"></li>
              <li>
                <?php echo CHtml::link('Logout', array('/site/logout'), array('class'=>'alert')) ?>
              </li>
            </ul>
          </li>  
        <?php endif ?>
      </ul>
    </section>
  </nav>
<!-- End Top Bar -->

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php echo $content; ?>

<div class="clear"></div>
<!--
<div class="row" id="footer">
  
    <p id="copyright">Copyright &copy; <?php echo date('Y'); ?> Tim Web Sparta 2012.</p>
  
</div> footer -->
 


   
  <script src="<?php echo Yii::app()->baseUrl ?>/js/foundation.min.js"></script>

<!--   
  <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.interchange.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
    
  <script src="<?php echo Yii::app()->baseUrl ?>/js/foundation/foundation.topbar.js"></script>
  <script src="<?php echo Yii::app()->baseUrl ?>/js/foundation/foundation.section.js"></script>
  
-->
  
  <script>
    $(document).foundation();
  </script>

</body>
</html>
