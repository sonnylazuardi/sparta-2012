<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <meta name="description" content="Website Sparta HMIF 2012" />
  <meta name="keywords" content="Sparta, HMIF, 2012, ITB, Ospek, Jurusan, Informatika" />
  <meta name="author" content="Tim Web Sparta" />
  
  <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/style.css" />

  <script src="<?php echo Yii::app()->baseUrl ?>/js/jquery.fittext.js"></script>


</head>

<body>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php echo $content; ?>

<div class="clear"></div>

<div class="row" id="footer">
  <div class="large-12 columns">
    Copyright &copy; <?php echo date('Y'); ?> Tim Web Sparta 2012.<br/>
  </div>
</div><!-- footer -->


  <script src="<?php echo Yii::app()->baseUrl ?>/js/foundation.min.js"></script>
    <!--
  
  <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.interchange.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
    -->
  <script src="<?php echo Yii::app()->baseUrl ?>/js/foundation/foundation.section.js"></script>

  
  <script>
    $(document).foundation();
  </script>

</body>
</html>
