<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<!--header-->
	<!DOCTYPE html>
	<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<title>SPARTA | Home</title>  
		<link rel="stylesheet" href="css/style.css" />
		<script src="js/vendor/custom.modernizr.js"></script>
		<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
	<!--	<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script> -->
		<script src="js/jquery.js"></script> 
		<script src="js/jquery.fittext.js"></script>
		<script src="js/maplace.min.js"></script>
		<script src="js/mustache.js"></script>
	<body id="text-page">
<!--header END-->
<!-- Navigation -->
  <nav class="top-bar">
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        <h1>
          <a href="home.html">
            Sparta HMIF
          </a>
        </h1>
      </li>
      <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
    </ul>
 
    <section class="top-bar-section">
      <!-- Right Nav Section -->
      <ul class="right">
        <li class="divider"></li>
        <li><a href="home.html">Home</a></li>
        <li class="divider"></li>
        <li><a href="text.html">News</a></li>
      </ul>
    </section>
  </nav>
<!-- End Top Bar -->


<div class="row">
	<div class="large-4 columns">
		<div class="gadget-container center">
		<a href=""><button class="full">Lihat Peserta</button></a>
		<a href=""><button class="full">Download All</button></a>
		<a href="<?php echo Yii::app()->createUrl('/peserta/report') ?>"><button class="full">Lihat Peserta</button></a>
		
		</div>
	</div>
</div>


<!--footer-->
		
	<script>
	document.write('<script src=' +
	('__proto__' in {} ? 'assets/js/vendor/zepto' : 'assets/js/vendor/jquery') +
	'.js><\/script>')
	</script>
	
	<script src="js/foundation/foundation.js"></script>
	<!--
	<script src="js/foundation/foundation.alerts.js"></script>
	
	<script src="js/foundation/foundation.clearing.js"></script>
	
	<script src="js/foundation/foundation.cookie.js"></script>
	
	<script src="js/foundation/foundation.dropdown.js"></script>
	
	<script src="js/foundation/foundation.forms.js"></script>
	
	<script src="js/foundation/foundation.joyride.js"></script>
	
	<script src="js/foundation/foundation.magellan.js"></script>
	
	<script src="js/foundation/foundation.orbit.js"></script>
	
	<script src="js/foundation/foundation.placeholder.js"></script>
	
	<script src="js/foundation/foundation.reveal.js"></script>
	
	<script src="js/foundation/foundation.section.js"></script>
	
	<script src="js/foundation/foundation.tooltips.js"></script>
	-->
	<script src="js/foundation/foundation.topbar.js"></script>
	
  
  <script>
    $(document).foundation();
  </script>
	</body>
	</html>
<!--footer END-->

</body>
</html>