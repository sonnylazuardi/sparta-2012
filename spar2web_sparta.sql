-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 01, 2013 at 07:56 PM
-- Server version: 5.1.70-cll
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spar2web_sparta`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE IF NOT EXISTS `akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_akun` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=232 ;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `username`, `password`, `role`, `status`, `salt`, `timestamp`) VALUES
(25, 'kambingguling', '21232f297a57a5a743894a0e4a801fc3', 'peserta', 1, '', '2013-06-19 12:50:32'),
(27, 'kambingbakar', 'e1ebdc6e18419e4c1abf8b911a7e1f25', 'peserta', 1, '51c14816029b41.54731152', '2013-06-19 12:56:38'),
(28, 'dewacinta', '97e17a1c91d45f65cd92c6da4d17272a', 'peserta', 1, '51c14abd875e62.65559501', '2013-06-19 13:07:57'),
(29, 'setyo.l', '3f991bde3b964b323cf3def0d2a4088f', 'peserta', 1, '51c14b1d12f4d8.16639010', '2013-06-19 13:09:33'),
(30, 'christian', '14b6bd38e22d96b6198bc7feafcd2624', 'peserta', 1, '51c14b508ca958.79452726', '2013-06-19 13:10:24'),
(31, 'PJSDEDE', 'f4fd3e36cb5443d979e7f2358cccf9d6', 'peserta', 1, '51c14b9a73fe37.11987946', '2013-06-19 13:11:38'),
(32, 'identityope', '66ac5927107a17a2bc39c3d2354ef64d', 'peserta', 1, '51c14d7db8cd53.54386788', '2013-06-19 13:19:41'),
(33, 'anindilla', 'd75e6ed202054da77d8b78e643a1b669', 'peserta', 1, '51c2f1ccc295b6.67949408', '2013-06-20 19:13:00'),
(36, 'jonathans121', '0fa49d78f2e47a03aefcd58137d93b92', 'peserta', 1, '51c2f5c0233776.84575241', '2013-06-20 19:29:52'),
(41, 'lotuzz', 'c10e070a2e14898d08bd65f1bd9d26d8', 'peserta', 1, '51c2f69e794cc6.55464302', '2013-06-20 19:33:34'),
(43, 'afik154', '265e68d93c780bb2f4cc63e3caeb1ee2', 'peserta', 1, '51c2f7703c4ea0.75716744', '2013-06-20 19:37:04'),
(44, 'Daniar Heri Kurniawan', 'c5a629756b71c8daa98a9d1eb8cd7325', 'peserta', 1, '51c2f77e10e579.26088439', '2013-06-20 19:37:18'),
(45, 'arinalistyarini', 'abaccfaf61a656035cd9901036b3a792', 'peserta', 1, '51c2f78941b3e7.74994121', '2013-06-20 19:37:29'),
(47, 'stephendj', 'f260756c0675d55f087f4d10c45fb987', 'peserta', 1, '51c2f99594f8a1.52073644', '2013-06-20 19:46:13'),
(49, 'kevin.zhong94', '28377e7f706164d147bdf47b41292825', 'peserta', 1, '51c2fb39460720.50708868', '2013-06-20 19:53:13'),
(51, 'MHusainJ', '0932e8be46017f6335abf78ef8255f95', 'peserta', 1, '51c2fc1d2f3e93.55249162', '2013-06-20 19:57:01'),
(52, 'fahmi.dumadi', '1033b61fd4ef5cfff8bc3ddcc04b7d9a', 'peserta', 1, '51c2fc9bb207c7.40116059', '2013-06-20 19:59:07'),
(53, 'Nimas Putri Asriningtyas', '4960bd2691f2be288c8a607ccb0c97c3', 'peserta', 1, '51c2fd005ad515.76478436', '2013-06-20 20:00:48'),
(56, 'rikysamuel', 'e870e9982305edf8c0158509ecd10b66', 'peserta', 1, '51c2fe53950a05.57043025', '2013-06-20 20:06:27'),
(60, 'KaitO95', '074d2ac7454cf99f6de5ff259bf846e0', 'peserta', 1, '51c30120380801.72273038', '2013-06-20 20:18:24'),
(61, 'nyan_cat', 'd8ef044ab6a1650e124bb14b5e1b43dc', 'peserta', 1, '51c301ec39dd62.50241155', '2013-06-20 20:21:48'),
(62, 'WilliamStefan', '11979ad05c434fdb00d4e1b2b954a260', 'peserta', 1, '51c30256be8736.87522127', '2013-06-20 20:23:34'),
(63, 'ivanaclairine', 'ed3e8a5a2cda5d167381f24e7bf094a6', 'peserta', 1, '51c3025e514ce3.93284542', '2013-06-20 20:23:42'),
(64, 'rivarave', '864c5d5d911df0fa0d6d736e02dc5416', 'peserta', 1, '51c302ef547004.68872151', '2013-06-20 20:26:07'),
(66, 'annisaurrosi', 'ef2303afe69a3f6e340fe8913ef2ed54', 'peserta', 1, '51c303d1249449.28466412', '2013-06-20 20:29:53'),
(68, 'Felix.Tanamas', 'c4aa24ed255d4b98674939574e3a5687', 'peserta', 1, '51c3051e8087d0.65711463', '2013-06-20 20:35:26'),
(69, 'gilangjulians', '66db2595054cf250fac7cba6304c9f27', 'peserta', 1, '51c305263817a4.75360013', '2013-06-20 20:35:34'),
(70, 'yanfa.adiputra', '50728f1a9b8f185c7c49307d6b6fa8ee', 'peserta', 1, '51c305d677ee45.85117000', '2013-06-20 20:38:30'),
(71, 'felicia.christie', '63f41c3a28b3d4979ac2b8390bce20b2', 'peserta', 1, '51c305fb8e2c25.58146184', '2013-06-20 20:39:07'),
(73, 'ramandika', '5af85f6c2f8f1e9ae90844498816284e', 'peserta', 1, '51c30629333068.12139728', '2013-06-20 20:39:53'),
(74, 'neverlifebeing', '5d9a1c2d2aa05feeb55d99e8cdfcaa27', 'peserta', 1, '51c3062b927324.99533122', '2013-06-20 20:39:55'),
(75, 'melvinfonda', 'c7077abdc649a97c936a64b248febd26', 'peserta', 1, '51c3087166ed99.76683158', '2013-06-20 20:49:37'),
(76, 'ghaniruhman', '4729c4faaf52f5e6631582b458f1a3a5', 'peserta', 1, '51c308e0c98423.58196639', '2013-06-20 20:51:28'),
(77, 'timothypratama', 'afb81dd0307bff26756f27a1b913b4ea', 'peserta', 1, '51c308f59c3c37.57291298', '2013-06-20 20:51:49'),
(79, 'tiaradwiputri', '825b79e5a9996cdf1a31f09f949d1c4f', 'peserta', 1, '51c30a1d97eeb7.42377564', '2013-06-20 20:56:45'),
(81, 'Never_alive', '9bf62f17aea8380dcfd7f1d915d9b2cc', 'peserta', 1, '51c30c2451e488.41025463', '2013-06-20 21:05:24'),
(82, 'steve.harnadi', 'c64420760f72e07496680c3fe687e4f2', 'peserta', 1, '51c30c969efc69.81519729', '2013-06-20 21:07:18'),
(83, 'hayyuluthfi', '5a6246e374cf0b469c8db4b7dc7497b6', 'peserta', 1, '51c30e609af0d0.62562048', '2013-06-20 21:14:56'),
(85, 'darwin.prasetio', '465353ea2bda1e94a52b0058e4164d4a', 'peserta', 1, '51c30ece468269.72060061', '2013-06-20 21:16:46'),
(86, 'winsxx', '4e3ff5ff1bf2b44328520191b57cca89', 'peserta', 1, '51c30f07941451.34668041', '2013-06-20 21:17:43'),
(90, 'wiragotama', 'a53719c9b190d0c8ccb4a849f32c07c8', 'peserta', 1, '51c310537fe1a5.04840048', '2013-06-20 21:23:15'),
(91, '16512238', '0926bee66baf5cc2910cf0499208a099', 'peserta', 1, '51c31064c90b13.54785076', '2013-06-20 21:23:32'),
(92, 'andarias', '091e055ad4e835d7aeaba758192f642a', 'peserta', 1, '51c31199dd3287.46647322', '2013-06-20 21:28:41'),
(93, 'hilmanramadhan', 'c3de2b8dbbae1b797d4d40dc659f2ace', 'peserta', 1, '51c311aabe2ae6.64069642', '2013-06-20 21:28:58'),
(94, 'syahid.nr', '824461c545621cf954c25db2ec0236d5', 'peserta', 1, '51c3126aa5d371.73252423', '2013-06-20 21:32:10'),
(95, 'christangga', '8ebb4cad9d8b853412d9c5bfc4801a8e', 'peserta', 1, '51c312e72d8c26.23050307', '2013-06-20 21:34:15'),
(96, 'hendro_tb', '249e35b87decb28e50840075040dddc7', 'peserta', 1, '51c312f84cd307.42531794', '2013-06-20 21:34:32'),
(98, 'Imballinst', '6f6e8fc84f3218cf5ef616436adc8eab', 'peserta', 1, '51c313e409bed5.04638325', '2013-06-20 21:38:28'),
(99, 'putriskandar', 'b7379037abbe4b650a2a77212755d4a4', 'peserta', 1, '51c315529401d7.66087266', '2013-06-20 21:44:34'),
(100, 'gifarikautsar', '7b2ffad9083ff4f1a3d2e547867e3fc8', 'peserta', 1, '51c316774f0d81.26293633', '2013-06-20 21:49:27'),
(103, 'riady.sk', '62c1a8547eacb5bedf7191d4d37d7f48', 'peserta', 1, '51c318a29e70f5.66907565', '2013-06-20 21:58:42'),
(104, 'andreysima', '5f1ec598621ff5c94d90672cd1dd2534', 'peserta', 1, '51c31a42061477.58110437', '2013-06-20 22:05:38'),
(105, 'anakarwin', 'd50adca16076fbdfdc37bd9ad6fffcc4', 'peserta', 1, '51c31add6d56b4.69342040', '2013-06-20 22:08:13'),
(106, 'mariotj', '3b6ccaa757b54862581ca3d238d32d5a', 'peserta', 1, '51c31b304abfa3.22279445', '2013-06-20 22:09:36'),
(107, 'Dennis Jonathan', '72abe464e8c694264d7df154f6b17d11', 'peserta', 1, '51c31b65c0cf88.54142509', '2013-06-20 22:10:29'),
(108, 'miftakhul.ricko', '06832aada5a1884a3bd93494473b36f3', 'peserta', 1, '51c31cca608706.70319924', '2013-06-20 22:16:26'),
(109, 'mamat_rahmat', '5e152276fb68cc5b9222fd91d6bf40f4', 'peserta', 1, '51c31d19d73177.08812325', '2013-06-20 22:17:45'),
(110, 'eric2861994', 'edd5cbe557c6f61680c61a18424fff8e', 'peserta', 1, '51c31d212d9654.09028492', '2013-06-20 22:17:53'),
(112, 'linda.sekawati', 'af6e9570f286c8877368dbe41ecb2982', 'peserta', 1, '51c3200bb6e001.51938154', '2013-06-20 22:30:19'),
(113, 'eldwin.christian', 'fdafeda7f63205e2a23d6c26debf0c77', 'peserta', 1, '51c325e2470700.86539949', '2013-06-20 22:55:14'),
(114, 'ardiwii', 'feb60c243734fabef65c251231b0118e', 'peserta', 1, '51c325e6191751.96604475', '2013-06-20 22:55:18'),
(115, 'Aldyaka Mushofan', 'c2ad6e832031905a20943553f0ade42e', 'peserta', 1, '51c328b51b22a3.00925692', '2013-06-20 23:07:17'),
(118, 'stanleysantoso', 'cead2416f78b739845b471cc70633503', 'peserta', 1, '51c32a78619198.39303841', '2013-06-20 23:14:48'),
(120, 'Tegar Aji Pangestu', 'c21c626a68320ac80c1bc2cb6dcd63d3', 'peserta', 1, '51c32dc53f1676.40889208', '2013-06-20 23:28:53'),
(123, 'monika.sembiring', 'b862741c2d0ee562e0882754a401339e', 'peserta', 1, '51c32e6d443a99.26422914', '2013-06-20 23:31:41'),
(124, 'ibnuqoyim', 'de8b097c3725469e6de073a2bc1bc8ca', 'peserta', 1, '51c3318608a424.69989226', '2013-06-20 23:44:54'),
(125, 'dalva24', '685a9123ef68295a7d2abe68e9c87b7c', 'peserta', 1, '51c331c462b630.55310122', '2013-06-20 23:45:56'),
(127, 'adwisatya', '0d13ff1e6a45db7f4e96d9b9aa685aca', 'peserta', 1, '51c33772af0e80.22287180', '2013-06-21 00:10:10'),
(128, 'danangafnanhudaya', 'b01a144c89b0a8ad70f6cec453152f4b', 'peserta', 1, '51c33c027cbe42.96195872', '2013-06-21 00:29:38'),
(129, 'michaelaw', 'f701e12d2f0fa085a74f79d318492af3', 'peserta', 1, '51c33cdf198877.15462990', '2013-06-21 00:33:19'),
(130, 'taufiqar', '8cb8f8974e6331cb5f2de811175dbda1', 'peserta', 1, '51c33d4ae624a8.19882643', '2013-06-21 00:35:06'),
(132, 'womzz_willy2706', 'f03a253acc85bbb973cd560aabdb30ba', 'peserta', 1, '51c33e7bb4b705.85090641', '2013-06-21 00:40:11'),
(133, 'fauzanhr', '9ebba1341379a3905e2d12ec422bf5bc', 'peserta', 1, '51c34152c36333.70474956', '2013-06-21 00:52:18'),
(134, 'kevhnmay94', 'e37ceb7aa74dc7f7ae7d4ce241af7898', 'peserta', 1, '51c344a61d15c0.15471786', '2013-06-21 01:06:30'),
(137, 'edmundophie', 'bc181a3982c15695c2656d6177503b60', 'peserta', 1, '51c349ddafa279.94153174', '2013-06-21 01:28:45'),
(138, 'cssparta2012', 'f1ff6d4a765f0436278423a8000809b7', 'peserta', 1, '51c3643e7397f9.29268659', '2013-06-21 03:21:18'),
(139, 'fitriindah', '8eecb2a5f8bf2b4a5b255aaf51d731db', 'peserta', 1, '51c37b31697d45.05052349', '2013-06-21 04:59:13'),
(140, 'Yollandasekarrini', '7df0efd71c37d7a3cbddbc6e7f234fce', 'peserta', 1, '51c37f6d85cb83.25939986', '2013-06-21 05:17:17'),
(141, 'luthfi_hamid_m', '0d7cbd9a835579a5a01fba8f570e15bb', 'peserta', 1, '51c38615808db3.48020614', '2013-06-21 05:45:41'),
(142, 'joshua.bezaleel', '0b0592206a549d2c07e08417bb2326bd', 'peserta', 1, '51c3910a39ff69.43601434', '2013-06-21 06:32:26'),
(143, 'ichakid', 'a1efa734001185bd4fcb0e9dc55e63b4', 'peserta', 1, '51c3934dbbc4b6.15528014', '2013-06-21 06:42:05'),
(147, 'fafajar94', 'c771dac17456fbe5e5a8b0914c5b2470', 'peserta', 1, '51c398f9c90085.14227324', '2013-06-21 07:06:17'),
(150, 'Jacqueline', '6077f92ca7f89be3cec1e8bc5fbb8958', 'peserta', 1, '51c39b5a598340.70764944', '2013-06-21 07:16:26'),
(151, 'sheilamoon', '6718601d3cf7b772435454da9f03f10a', 'peserta', 1, '51c3a1ab5a5299.02206580', '2013-06-21 07:43:23'),
(152, 'tirtawr', 'b1c23879d1a0fa4c472b59a80dd8cb92', 'peserta', 1, '51c3a6f52039a0.39212001', '2013-06-21 08:05:57'),
(155, 'Erlangga', 'f9c835ae37d8a2ff67b33101bc3deeb6', 'peserta', 1, '51c3adfebeed84.32913570', '2013-06-21 08:35:58'),
(156, 'arumadiningtyasa', '10112b8f6adb263b7124f6684164c705', 'peserta', 1, '51c3af4ba09153.25411469', '2013-06-21 08:41:31'),
(157, 'Bagaskara Pramudita', 'b635d2d23b9bbfd65d7e520447c63928', 'peserta', 1, '51c3c456d35a20.96532125', '2013-06-21 10:11:18'),
(158, 'ashahab', 'f04e46e1e02acf89f2bf37dd6e0fb17f', 'peserta', 1, '51c3c55df22c44.45429063', '2013-06-21 10:15:41'),
(160, 'vidianindhita', 'd36f8d807165805687e6de0f78592cc0', 'peserta', 1, '51c3cfaf7ec802.57277961', '2013-06-21 10:59:43'),
(161, 'muzavan', '062ed573352529021f2d010b81f7c5d4', 'peserta', 1, '51c3d2d64f6a84.18293976', '2013-06-21 11:13:10'),
(162, 'Kuroneko', '5d18e174032e9b01e12916bbaf93b5f2', 'peserta', 1, '51c3d58a3ed4a4.41484559', '2013-06-21 11:24:42'),
(163, 'muntahailmi', 'b50f15737fb8e60057db71352b97f810', 'peserta', 1, '51c3d70e0a1944.60493778', '2013-06-21 11:31:10'),
(164, 'rakhmatullah25', '97175e76201d9407e417bbcdff1fee8e', 'peserta', 1, '51c3f25f8e1643.58401767', '2013-06-21 13:27:43'),
(165, 'nisadianr', '977d89a3bf5ebd0d0de94a2b23b65425', 'peserta', 1, '51c3fa19c1e732.03576539', '2013-06-21 14:00:41'),
(167, 'kev_yu', '7ecf45533af6fff46a1dee8855bf73a1', 'peserta', 1, '51c4260b1b4981.66291539', '2013-06-21 17:08:11'),
(169, 'diahfauziah', 'fa240ff8a77e503a8c4d86c0896287e8', 'peserta', 1, '51c42d37989bd1.99934669', '2013-06-21 17:38:47'),
(179, 'Nura Tresna Priantni', '6a899b19b3cba2b5ca5181839d084e42', 'peserta', 1, '51c42ec96d0c23.87993797', '2013-06-21 17:45:29'),
(181, 'bellaclaudia', 'dbc5fcdceef18b288923b43bd03dc046', 'peserta', 1, '51c433e3c94b70.21322525', '2013-06-21 18:07:15'),
(182, 'andresusanto', '356cbee0798688b97ca4319221d3692b', 'peserta', 1, '51c4394be6c630.29126238', '2013-06-21 18:30:19'),
(183, 'windyamelia', '0a865ecc383ffd84965c2c39369fa61c', 'peserta', 1, '51c439f49fc6b4.31289189', '2013-06-21 18:33:08'),
(186, '2ndless', '37359a1b86c05b66e46c180b5795db75', 'peserta', 1, '51c43d93238345.71735261', '2013-06-21 18:48:35'),
(187, 'Ichwanhs96', 'c2fe14f8d4ad9bdf7361b14a8a40c3cc', 'peserta', 1, '51c446a09ce7e1.14540173', '2013-06-21 19:27:12'),
(188, 'yafithekid', 'e4e8e5278ff50527b78fbabd87ca5209', 'peserta', 1, '51c447d39cd190.47462736', '2013-06-21 19:32:19'),
(189, 'luqmankusnadi', '84c307e9fd17261cb83aba31252fda23', 'peserta', 1, '51c448386dc261.91482116', '2013-06-21 19:34:00'),
(190, 'cilvia', 'c5e0e97870245dc27bccd9d86b781c6c', 'peserta', 1, '51c449b41f52f8.39579943', '2013-06-21 19:40:20'),
(191, 'just riska', '687589601e6a111930b4773360459d2c', 'peserta', 1, '51c449b928aa48.35230594', '2013-06-21 19:40:25'),
(192, 'susantigojali', '41c6d28b9fa9e020cb114d65a6433e02', 'peserta', 1, '51c449c8c32077.61037834', '2013-06-21 19:40:40'),
(194, 'Azfina Putri Anindita', '51cc83fd529d713326a7bdc2b3b7325b', 'peserta', 1, '51c4511116c0e4.90061740', '2013-06-21 20:11:45'),
(195, 'atianadya', '6044bd90a70512d788f879e5306d018b', 'peserta', 1, '51c451c6837e44.03569050', '2013-06-21 20:14:46'),
(197, 'Bintang', '85d9e23fec1282ab6dda9bcdde724b10', 'peserta', 1, '51c4541cd997e5.24405097', '2013-06-21 20:24:44'),
(199, 'opelhoward', '8209b74391146792ab7489d5408bf65f', 'peserta', 1, '51c45ae1ac7a79.86433516', '2013-06-21 20:53:37'),
(200, 'taufikakbar', '91f1845cfa57e6d7bda805b3d97efabc', 'peserta', 1, '51c45b14ed41f6.50720384', '2013-06-21 20:54:28'),
(201, 'ahmadzaky', '235779f98caa4565d832a55614289af9', 'peserta', 1, '51c45c00330090.15070136', '2013-06-21 20:58:24'),
(202, 'bramgalih36', 'e9ba017da896e61b71e143777d456833', 'peserta', 1, '51c45c37105101.11351727', '2013-06-21 20:59:19'),
(203, 'untari_zaeca', '90fac56120a3fd78ea37e552e691e748', 'peserta', 1, '51c45d528f54b9.41801979', '2013-06-21 21:04:02'),
(204, 'AfiffaHuwaidah', 'b661048504318f6be59682de3f97b102', 'peserta', 1, '51c45f7750e616.12457014', '2013-06-21 21:13:11'),
(205, 'fahziar', '122b5637023448375d533f7cecf15f46', 'peserta', 1, '51c460e04e83a9.54549833', '2013-06-21 21:19:12'),
(206, 'junita', '11f58cece8473403ec081453d27f6f60', 'peserta', 1, '51c462b34756c4.67484545', '2013-06-21 21:26:59'),
(207, 'verenude', 'fa7ab23180fd6db25af9c396cfc6f0f7', 'peserta', 1, '51c462bbd90980.06600525', '2013-06-21 21:27:07'),
(208, 'Farid Fadhil Habibi', '5aa3694058ce53ce164c8e0f08d601e0', 'peserta', 1, '51c463b92d02b5.51186115', '2013-06-21 21:31:21'),
(210, 'aurellerua', 'e861cd08de145e50841ef31732220fd1', 'peserta', 1, '51c46444a48af7.76010901', '2013-06-21 21:33:40'),
(211, 'henrymenori', '108e368201aef5173e45b2f10b4c6113', 'peserta', 1, '51c465ed37f1a1.63824929', '2013-06-21 21:40:45'),
(212, 'poetra', '82d9b0eef65c6e35c2373c43c2d7642b', 'peserta', 1, '51c46bb212e740.30989855', '2013-06-21 22:05:22'),
(214, 'JeffreyLingga', '78745f6d6b25db9720178cbd5275337a', 'peserta', 1, '51c46d80194e17.35037878', '2013-06-21 22:13:04'),
(215, 'almuwahidan', '5bf6efbe94c392a0f5a8c7cce98edb41', 'peserta', 1, '51c46ec300e318.70134272', '2013-06-21 22:18:27'),
(216, 'chrestellastephanie', '3b4b2b8672aa28cee4ecf45ceaf8d384', 'peserta', 1, '51c46f5ea9cb22.65733823', '2013-06-21 22:21:02'),
(217, 'khaidzir', 'ed3f1427258fc2eec43af5af30f3b59f', 'peserta', 1, '51c4718759f8f8.48766297', '2013-06-21 22:30:15'),
(220, 'aaratnaputri', 'cd2dccf84826912692a9c6c5aea2a5c1', 'peserta', 1, '51c4732a09a336.74373836', '2013-06-21 22:37:14'),
(221, 'Lutfifadlan', 'bfdd4428b4db1e2b60ad6b0ff51b85f3', 'peserta', 1, '51c4767ba72445.08050018', '2013-06-21 22:51:23'),
(222, 'indmhmmd', '0856bd6dbd2d382576b7f7c0fca794c9', 'peserta', 1, '51c4775629de33.46617930', '2013-06-21 22:55:02'),
(223, 'alvin_nt', '0ec062555c1a31c0ed77b924834eade9', 'peserta', 1, '51c47bb063c675.86413484', '2013-06-21 23:13:36'),
(224, 'joenathan', '2d760c75eee3df206d6ca5a64b8bb65c', 'peserta', 1, '51c47c5b66d6f7.89166966', '2013-06-21 23:16:27'),
(225, 'kanyaap', 'e48edb1556cde757863dd621827d9fc2', 'peserta', 1, '51c4c2983da748.53911740', '2013-06-22 04:16:08'),
(226, 'ardantoihsan', '691f470306622410b0a9cd0d91d45e98', 'peserta', 1, '51c4dd3d622970.26745118', '2013-06-22 06:09:49'),
(227, 'adhikasigit', '0183d52579eb6e3a670ec174f301930c', 'peserta', 1, '51c4fbdfae99a8.76869939', '2013-06-22 08:20:31'),
(229, 'Teofebano', 'f729d0c4b17da8de109fe8438b69da9e', 'peserta', 1, '51c57693583e36.33776697', '2013-06-22 17:04:03'),
(1, 'admin', '3fdaefc8fe98a1995aa66c465cb0f182', 'admin', 1, '', '2013-06-24 02:42:47'),
(230, 'Sample Account', 'd1cf6ea5f4208b00fcd256ad95b0512f', 'peserta', 1, '51c7961c08b9d1.25538376', '2013-06-24 07:43:08'),
(231, 'freedomofkeima', '11ff7b4aca301407902094497e69ffd9', 'peserta', 1, '51cb1d6c0911d9.52646543', '2013-06-26 23:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NOT NULL,
  `id_mamet` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_peserta` (`id_peserta`),
  KEY `inbox_mamet` (`id_mamet`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE IF NOT EXISTS `kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kelompok` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `kelompok`
--

INSERT INTO `kelompok` (`id`, `nama`) VALUES
(1, '0'),
(2, '1'),
(3, '2'),
(4, '3'),
(5, '4'),
(6, '5'),
(7, '6'),
(8, '7'),
(9, '8'),
(10, '9');

-- --------------------------------------------------------

--
-- Stand-in structure for view `medik`
--
CREATE TABLE IF NOT EXISTS `medik` (
`nama_lengkap` varchar(100)
,`nim_stei` int(8)
,`penyakit_pernah` text
,`penyakit_sedang` text
,`golongan_darah` varchar(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE IF NOT EXISTS `peserta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) NOT NULL,
  `id_kelompok` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_panggilan` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `nim_stei` int(8) NOT NULL,
  `nim_jurusan` int(8) DEFAULT NULL,
  `alamat` varchar(128) NOT NULL,
  `alamat_latitude` varchar(100) NOT NULL,
  `alamat_longitude` varchar(100) NOT NULL,
  `alamat_zoom` varchar(10) NOT NULL,
  `alamat_rumah` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_peserta` (`id`),
  KEY `id_akun` (`id_akun`),
  KEY `id_kelompok` (`id_kelompok`),
  KEY `id_peserta_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=183 ;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id`, `id_akun`, `id_kelompok`, `nama_lengkap`, `nama_panggilan`, `tempat_lahir`, `tanggal_lahir`, `jurusan`, `nim_stei`, `nim_jurusan`, `alamat`, `alamat_latitude`, `alamat_longitude`, `alamat_zoom`, `alamat_rumah`, `no_telp`, `email`) VALUES
(13, 25, NULL, 'Danang Arbansa Nur', NULL, 'Mana', '2012-12-12', 'Sistem Teknologi Informasi', 16511040, NULL, 'nyos', '-6.911542499999999', '107.60821499999997', '6', 'di mana aja', '0812345678', 'danangan@ymail.com'),
(14, 27, NULL, 'danangan', NULL, 'tuban', '0000-00-00', 'Teknik Informatika', 16511030, NULL, 'Cigadung', '-6.911542499999999', '107.60821499999997', '11', 'Tuban', '181234567', 'danangan@ymail.com'),
(15, 28, NULL, 'danang', NULL, 'grobogan', '0000-00-00', 'Teknik Informatika', 16511000, NULL, 'cigadung', '-6.9147444', '107.6098111', '11', 'cigadung', '081234567', 'danangan@ymail.com'),
(16, 29, NULL, 'Setyo L', NULL, 'Sukab', '1994-12-01', 'Teknik Informatika', 16511301, NULL, 'Hopi', '-6.89293', '107.61100899999997', '11', 'Hop', '0808', 'email@email.com'),
(17, 30, NULL, 'Christian', NULL, 'Bandung', '1992-10-27', 'Teknik Informatika', 16511059, NULL, 'Jl. Belakang BTC', '-6.911542499999999', '107.60821499999997', '8', 'Jl. Belakang BTC', '08997978828', 'christian_gunardi@hotmail.com'),
(18, 31, NULL, 'PJS DE', NULL, 'Bandung', '2013-06-01', 'Teknik Informatika', 16511999, NULL, 'sekre hmif', '55.8031916', '37.53421549999996', '11', 'sekre hmif', '08111222333', 'hmifdummy@hmif.com'),
(19, 32, NULL, 'Taufik Hidayat', NULL, 'Bandung', '1993-08-30', 'Teknik Informatika', 16511321, NULL, 'haji nawawi no 12b', '-6.911542499999999', '107.60821499999997', '10', 'haji nawawi no 12b', '087825996140', 'taufik.hidayat.me@gmail.com'),
(20, 33, 7, 'Dilla Anindita', NULL, 'Jakarta', '1996-01-18', 'Sistem Teknologi Informasi', 16512338, NULL, 'Jalan Kanayakan Baru Nomor 31A, Dago, Coblong. Kode Pos 40135. ', '-6.9147444', '107.6098111', '11', 'Jalan Bojana Tirta Raya No.8B, Pulogadung, Jakarta Timur, Jakarta. Kode Pos 13230. ', '081324639499', 'anindilla@gmail.com'),
(21, 36, 5, 'Jonathan Sudibya', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512095, NULL, 'Jalan Tubagus Ismail XV No. 16A', '-6.8852803', '107.6189005', '10', 'Jalan Raden Saleh II, Cikini, Jakpus', '087878180839', 'jonathans121@gmail.com'),
(26, 41, 6, 'Reinaldo Michael Hasian', NULL, 'Jakarta', '1994-09-20', 'Teknik Informatika', 16512099, NULL, 'Tubagus Ismail 5', '-6.911542499999999', '107.60821499999997', '10', 'Perumahan Bumi Anggrek,Bekasi', '085795638723', 'reinaldomh2009@gmail.com'),
(28, 43, 10, 'Khoirunnisa Afifah', NULL, 'Karanganyar', '1995-09-23', 'Teknik Informatika', 16512154, NULL, 'Jl Cisitu Lama Gang 2 No 132/154C', '-6.879207699999999', '107.61265400000002', '11', 'Bangun Asri, RT 16 RW 05, Plumbungan, Sragen', '087835688435', 'k.afis3@rocketmail.com'),
(29, 44, 9, 'Daniar Heri Kurniawan', NULL, 'Trenggalek, 4 Januari 1994', '0000-00-00', 'Teknik Informatika', 16512175, NULL, 'Jl.Plesiran 52/56', '-6.895699', '107.60705669999993', '10', 'Rt8/Rw 4, desa kendalrejo, kec Durenan, kab Trenggalek, Jawa Timur', '089679558799', 'ddhhkk2@gmail.com'),
(30, 45, 10, 'Arina Listyarini Dwiastuti', NULL, 'Bandung', '1994-02-20', 'Teknik Informatika', 16512257, NULL, 'Komp. Taman Citeureup Jl. Nusasari Utara II No. 8 Cimahi', '-6.880239', '107.53549999999996', '11', 'Komp. Taman Citeureup Jl. Nusasari Utara II No. 8 Cimahi', '085722773269', 'arinalistyarini@yahoo.com'),
(32, 47, 7, 'Stephen', NULL, 'Medan', '1994-11-18', 'Teknik Informatika', 16512033, NULL, 'Jln. Tubagus Ismail 5 no. 2A', '-6.9147444', '107.6098111', '11', 'Apartemen Green Bay Pluit B-9/AB', '081513765370', 'stephendjohan@yahoo.com'),
(34, 49, 3, 'Kevin', NULL, 'Medan', '1994-03-11', 'Teknik Informatika', 16512343, NULL, 'Jl. Bukit Tunggul no 4', '-6.911542499999999', '107.60821499999997', '10', 'Jl. D.I. Panjaitan no 111.Medan', '089625547803', 'kevin.zhong94@yahoo.com'),
(36, 51, 3, 'Muhammad Husain Jakfari', NULL, 'Bandung', '1994-11-28', 'Teknik Informatika', 16512348, NULL, 'gg Intan IV no 4, Sadang Serang', '-6.896786812370717', '107.62159136199341', '12', 'Jl. Dr. M. Isa no 823, Pasar Kuto, Palembang ', '08973056008', 'emha.husain104@gmail.com'),
(37, 52, 2, 'Fahmi Dumadi', NULL, 'Bandung', '1994-09-09', 'Teknik Informatika', 16512171, NULL, 'Jalan Gunung Batu Gang H. Juariah 35', '-6.894699834409014', '107.56081569939852', '19', 'Jalan Gunung Batu Gang H. Juariah 35', '085320107514', 'fah.midum.adi@gmail.com'),
(38, 53, 9, 'Nimas Putri Asriningtyas', NULL, 'Tegal', '1994-07-28', 'Sistem Teknologi Informasi', 16512002, NULL, 'Jl. Ciheulang No 233 Tubagus Ismail Bandung', '-6.886013953826163', '107.6193187134918', '18', 'Jl. Arum Indah II No 14 Kota Tegal', '089652670807', 'asriningtyas.nimas@gmail.com'),
(40, 56, 2, 'Rikysamuel', NULL, 'Bandung', '1994-11-06', 'Teknik Informatika', 16512050, NULL, 'Jalan Kresna no 1', '-6.911037023020467', '107.59437288703305', '18', 'Jalan Kresna no 1', '085722064771', 'rikz.samuel@gmail.com'),
(42, 60, 7, 'Tony', NULL, 'Jakarta', '1995-07-09', 'Teknik Informatika', 16512324, NULL, 'Jl. Cisitu Baru Dalam no. 72', '-6.9147444', '107.6098111', '10', 'Jl. Kerajinan no. 19', '085717566229', 'kaito1412green95@yahoo.com'),
(43, 61, 4, 'Muhammad Haris Maulana', NULL, 'Bandung', '1993-11-27', 'Sistem Teknologi Informasi', 16512304, NULL, 'Jl. Sidomukti No. 10', '-6.895203899999999', '107.63189030000001', '11', 'Jl. Sidomukti No. 10', '085721600427', 'ikyfy@yahoo.com'),
(44, 62, 3, 'William Stefan Hartono', NULL, 'Purworejo', '0000-00-00', 'Teknik Informatika', 16512046, NULL, 'Jalan Ciumbuleuit 47, Bandung', '-6.882022953879274', '107.6042069179382', '17', 'Jalan A. Yani 106, Purworejo', '089671536604', 'williamstefanh@yahoo.com'),
(45, 63, 9, 'ivana clairine irsan', NULL, 'malang', '1994-08-21', 'Teknik Informatika', 16512152, NULL, 'tubagus ismail', '-6.9147444', '107.6098111', '9', 'garbis 5 malang', '0817385226', 'ivana.clairine@gmail.com'),
(46, 64, 1, 'Riva Syafri Rachmatullah', NULL, 'Palembang', '1995-08-14', 'Teknik Informatika', 16512311, NULL, 'Jl. Cisitu Lama no. 14/160C', '-6.878104234161113', '107.6114847984253', '15', 'Jl. Wahyu Asri IIIA no. 2, Ngaliyan, Semarang', '08995602430', 'rivahero@gmail.com'),
(47, 66, 1, 'Annisa''ur Rosi Lutfiana', NULL, 'Cilacap', '0000-00-00', 'Teknik Informatika', 16512245, NULL, 'Jl. Taman Hewan No. 63A/56', '-6.890841699999999', '107.61117430000002', '11', 'Perum Cisait Puri Kragilan Serang - Banten', '085776877500', 'rosilutfi@gmail.com'),
(49, 68, 1, 'Felix Riady Tanamas', NULL, 'Bandung', '1994-02-04', 'Sistem Teknologi Informasi', 16512355, NULL, 'Jl. Leuwisari 7 no 6a', '-6.9423461327534035', '107.59714538231492', '18', 'Jl. Leuwisari 7 no .6a', '08997984403', 'felixtanamas@gmail.com'),
(50, 69, 5, 'Gilang Julian Suherik', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512192, NULL, 'Jl. Tamansari no 60/56 rt. 04 rw. 07', '', '', '15', 'Jl. Jeruk no.18 rt. 10 rw. 04 Bumi Mekar Indramayu', '083824455975', 'gilang.9h@gmail.com'),
(51, 70, 1, 'Yanfa Adi Putra', NULL, 'Bekasi', '0000-00-00', 'Teknik Informatika', 16512112, NULL, 'Jln. Cisitu Lama 8 No. 11a', '-6.2333333', '107', '12', 'Jln. Flamboyan Blok E6/14 Pondok Hijau Permai Bekasi Timur', '085703047464', 'yanfa.adiputra@yahoo.com'),
(52, 71, 6, 'Felicia Christie', NULL, 'Medan', '1994-11-27', 'Teknik Informatika', 16512267, NULL, 'Tubagus Ismail', '-6.9147444', '107.6098111', '11', 'Medan', '08566369311', 'feli-c-s@hotmail.com'),
(54, 73, 2, 'Ramandika Pranamulia', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512102, NULL, 'Jl. Imam Bonjol No. 47', '-6.2176426', '106.89159480000001', '11', 'Jl. Y No. 61 Cipinang Muara, Jatinegara, Jakarta Timur', '085881315920', 'ramandika@students.itb.ac.id'),
(55, 74, 7, 'Binanda Smarta Aji', NULL, 'Malang', '1993-07-30', 'Teknik Informatika', 16512006, NULL, 'Jalan Cisitu Baru 13', '-6.911542499999999', '107.60821499999997', '11', 'Jalan Simpang Asahan 5 Malang', '085755190027', 'binand94@yahoo.com'),
(56, 75, 5, 'Melvin Fonda', NULL, 'Jakarta', '1994-03-29', 'Teknik Informatika', 16512149, NULL, 'Jalan Cisitu Indah 2 no 3', '-6.877085999999999', '107.61235850000003', '10', 'Green Garden blok n9 nomor 37', '087887470382', 'melvin_f1994@yahoo.com'),
(57, 76, 4, 'Ghani Ruhman', NULL, 'Bandung', '1994-05-25', 'Sistem Teknologi Informasi', 16512215, NULL, 'Griya Bukit Mas II Blok D6 No.22, Bojong Koneng, Cikutra, Kab. Bandung', '-6.8780895', '107.64224880000006', '11', 'Griya Bukit Mas II Blok D6 No.22, Bojong Koneng, Cikutra, Kab. Bandung', '085720138535', 'ghaniruhman@gmail.com'),
(58, 77, 3, 'Timothy Pratama', NULL, 'Bandung', '1993-12-08', 'Teknik Informatika', 16512260, NULL, 'Jalan Griya Raya I no. 1 perumahan Griya Mas daerah Cibogo', '-6.8869243', '107.57862380000006', '11', 'Jalan Griya Raya I no. 1 perumahan Griya Mas daerah Cibogo', '0811221832', 'timothy.pratama@gmail.com'),
(60, 79, 4, 'Tiara Dwiputri', NULL, 'Jakarta', '1994-11-12', 'Sistem Teknologi Informasi', 16512268, NULL, 'idm', '-6.879207699999999', '107.61265400000002', '11', 'Jln. Sangkuriang G2, Bandung', '085722688068', 'rei.koizumi@yahoo.com'),
(61, 81, 9, 'Kevin', NULL, 'Pematangsiantar', '0000-00-00', 'Teknik Informatika', 16512094, NULL, 'Jl.Ciumbulit Gang Suhari 2 No 46-48', '-6.8586297', '107.61265400000002', '11', 'Jl.Thamrin No 27 Pematangsiantar', '085760949600', 'kevin_huang1994@yahoo.com'),
(62, 82, 4, 'Steve Immanuel Harnadi', NULL, 'Bandung', '1994-05-14', 'Teknik Informatika', 16512194, NULL, 'Jalan Ancol Timur III No.41', '-6.9147444', '107.6098111', '6', 'Jalan Ancol Timur III No.41, Bandung', '081222324170', 'steve.harnadi@gmail.com'),
(63, 83, 10, 'Hayyu'' Luthfi Hanifah', NULL, 'Semarang', '0000-00-00', 'Teknik Informatika', 16512203, NULL, 'Jalan Kebon Bibit Barat I No. 50', '-6.9012472', '107.60821499999997', '11', 'Jalan Sriwijaya Utara IV No. 4, Nusukan, Solo 57135', '085229167235', 'hayyu94@yahoo.com'),
(64, 85, 2, 'Darwin Prasetio', NULL, 'Medan', '1995-12-17', 'Teknik Informatika', 16512081, NULL, 'Jl.Ciumbuleuit 66', '-6.911542499999999', '107.60821499999997', '11', 'Jl.S.Parman 154 Medan 20112', '082165113030', 'prasetiodarwin@gmail.com'),
(65, 86, 9, 'Winson Waisakurnia', NULL, 'Medan', '1994-05-25', 'Teknik Informatika', 16512308, NULL, 'Ciumbuleuit', '-6.9147444', '107.6098111', '8', 'Jl. Cik Ditiro , Medan', '085262816093', 'winson_w94@hotmail.com'),
(67, 90, 3, 'Jan Wira Gotama Putra', NULL, 'Singaraja', '1994-11-04', 'Teknik Informatika', 16512105, NULL, 'Dago Asri Blok B 30', '-6.923018663503987', '107.69760131835938', '12', 'Jalan A.Yani no 11 Singaraja', '08563734273', 'wiragotama@gmail.com'),
(68, 91, 8, 'Eric Ongkowijoyo', NULL, 'Surabaya', '0000-00-00', 'Sistem Teknologi Informasi', 16512238, NULL, 'Tirtayasa 21', '-6.911542499999999', '107.60821499999997', '11', 'Margorejo Indah A 416 Surabaya', '083849502626', 'eric_ong@rocketmail.com'),
(69, 92, 1, 'Andarias Silvanus', NULL, 'Bandung', '1994-11-05', 'Teknik Informatika', 16512116, NULL, 'Gang Saleh gang V', '-6.9107601', '107.59489859999996', '11', 'Gang Saleh gang V', '08997007714', 'asilvanus5@gmail.com'),
(70, 93, 6, 'Hilman Ramadhan', NULL, 'Palangkaraya', '0000-00-00', 'Sistem Teknologi Informasi', 16512030, NULL, 'Jalan Cisitu Lama no. 36', '-6.881928151264529', '107.61189249419556', '16', 'Jalan Raya Lembang no. 436 - 438 Lembang, Bandung Barat', '085799901996', 'hilmanrmdhn@gmail.com'),
(71, 94, 9, 'Syahid Naufal Ramadhan', NULL, 'Jakarta', '0000-00-00', 'Sistem Teknologi Informasi', 16512124, NULL, 'Jl. Kalijati 20 No. 2 Antapani Kulon, Bandung', '-6.911542499999999', '107.60821499999997', '11', 'Jl. H. Selong F/115 Rt 006 Rw 03 Jakarta 11750', '085776442166', 'syahid.nr@gmail.com'),
(72, 95, 5, 'Christ Angga Saputra', NULL, 'Bandung', '1994-09-16', 'Teknik Informatika', 16512283, NULL, 'Taman Hijau 36', '', '', '5', 'Taman Hijau 36', '08997170052', 'christ.angga.saputra@gmail.com'),
(73, 96, 8, 'Hendro Triokta Brianto', NULL, 'Bandung', '1992-10-21', 'Teknik Informatika', 16512118, NULL, 'JL. Cikutra Gg. Sekepondok 1 No. 154/138A', '-6.9147444', '107.6098111', '11', 'JL. Cikutra Gg. Sekepondok 1 No. 154/138A', '02291563991', 'hendro.tb@gmail.com'),
(75, 98, 6, 'Try Ajitiono', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512219, NULL, 'Jalan Cisitu Lama 8 No 51', '-6.880832', '107.61223389999998', '11', 'Jalan Loka Indah Blok C 35', '08159778609', 'lucent_rays@yahoo.com'),
(76, 99, 3, 'Khairina Putri Iskandar', NULL, 'Medan', '1994-11-16', 'Sistem Teknologi Informasi', 16512382, NULL, 'cisitu baru no.32', '-6.9147444', '107.6098111', '11', 'mahagoni park b15/11 bintaro jaya, tangerang selatan', '081284481703', 'kputriskandar@gmail.com'),
(77, 100, 3, 'Gifari Kautsar', NULL, 'Garut', '1994-04-09', 'Teknik Informatika', 16512334, NULL, 'Jalan Pelesiran Gang Mama Rustama No. 61/56', '-6.895719245426227', '107.6079682367706', '10', 'Jalan Subyadinata RT/RW 01/01, Tarogong Kidul, Garut, 44151', '081313899535', 'gifarikautsar@gmail.com'),
(78, 103, 4, 'Riady Sastra Kusuma', NULL, 'Jakarta', '1994-09-06', 'Teknik Informatika', 16512378, NULL, 'kebon bibit barat no32B', '-6.911542499999999', '107.60821499999997', '9', 'Pondok Kopi blok A2/2', '08978670424', 'riady_saz@yahoo.com'),
(79, 104, 4, 'Andrey Simaputera', NULL, 'Tangerang', '1994-10-22', 'Teknik Informatika', 16512289, NULL, 'Jalan Tubagus Ismail VI no 15', '-6.9147444', '107.6098111', '8', 'Premier Residence kavling 82, Modernland, Tangerang', '085694797040', 'andrey.simaputera@yahoo.com'),
(80, 105, 10, 'Yusuf Rahmatullah', NULL, 'Bandung', '1994-06-07', 'Teknik Informatika', 16512329, NULL, 'Jalan Panglayungan No.4 RT03 RW08 Kel.Cipadung Kulon Kec. Panyileukan Bandung', '-6.927598404469352', '107.70022988319397', '13', 'Jalan Panglayungan No.4 RT03 RW08 Kel.Cipadung Kulon Kec. Panyileukan Bandung', '085721632951', 'anakarwin@students.itb.ac.id'),
(81, 106, 8, 'Mario Tressa Juzar', NULL, 'Padang', '1994-04-29', 'Teknik Informatika', 16512011, NULL, 'Jalan Cisitu Lama III no. 24A/154C', '-6.9147444', '107.6098111', '9', 'Jalan Adinegoro no.91 Lubuk Buaya', '082268175686', 'mariotj.tj@gmail.com'),
(82, 107, 1, 'Dennis Jonathan', NULL, 'Bandung', '1994-11-22', 'Teknik Informatika', 16512117, NULL, 'Komplek taman bumi prima n-5 cibabat cimahi', '-6.8773772570950635', '107.56017901003361', '15', 'Komplek Taman Bumi Prima N-5', '089655153070', 'dennis.do.that@gmail.com'),
(83, 108, 9, 'Miftakhul Afrizal Ricko Primantara', NULL, 'Karanganyar', '0000-00-00', 'Sistem Teknologi Informasi', 16512281, NULL, 'Jalan Dago Asri IV No 14', '-7.175996899999999', '110.42918789999999', '14', 'Perum Depot Palan V No C24, Karangjati, Kab. Semarang', '081901648641', 'miftakhul_9a_15@yahoo.co.id'),
(84, 109, 2, 'Mamat Rahmat', NULL, 'Ciamis', '0000-00-00', 'Teknik Informatika', 16512266, NULL, 'Jalan ', '-6.9147444', '107.6098111', '16', 'Jalan H Hasan No 47 Cigembor Ciamis', '085721247847', 'm2t.math@gmail.com'),
(85, 110, 5, 'Eric', NULL, 'Medan', '0000-00-00', 'Teknik Informatika', 16512373, NULL, 'Jl Ciumbeluit Gang Bapak Jali No. 18', '3.585242', '98.67559789999996', '11', 'Kompleks Cemara Hijau Blok F No 12, Medan', '085262312602', 'eric2861994@gmail.com'),
(87, 112, 4, 'Linda Sekawati', NULL, 'Kuningan', '1994-10-13', 'Teknik Informatika', 16512079, NULL, 'Jl. Plesiran 40/58', '-6.9012472', '107.60821499999997', '11', 'Desa Maniskidul Rt.03/01 no.190, Kec. Jalaksana, Kab. Kuningan', '085759759176', 'sekawati.linda@gmail.com'),
(88, 113, 5, 'Eldwin Christian', NULL, 'Jambi', '1995-12-22', 'Teknik Informatika', 16512083, NULL, 'Jl. Kebon Bibit Barat I No. 52', '-6.897124984676415', '107.60616280138493', '16', 'Jl. Brigjen Katamso No. 64 Kec. Jambi Timur Kota Jambi', '085795156576', 'eldwinchristian@yahoo.com'),
(89, 114, 9, 'Ardi Wicaksono', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512031, NULL, 'Tubagus Ismail V no.16, Sekeloa, Coblong, Bandung', '-6.885359999999999', '107.62005240000008', '11', 'Jalan Kebon Jeruk Raya no. 33 Kebon Jeruk, Jakarta Barat', '087880219119', 'aredee@windowslive.com'),
(90, 115, 2, 'Aldyaka Mushofan', NULL, 'Klaten', '1995-03-26', 'Teknik Informatika', 16512072, NULL, 'Jalan Cisitu Lama 2, Dago, Coblong', '-6.879207699999999', '107.61265400000002', '11', 'Bungkusan, Jurangjero, Karanganom, Klaten', '085728300606', 'mushofan1@gmail.com'),
(93, 118, 10, 'Stanley Santoso', NULL, 'Medan', '1995-03-07', 'Teknik Informatika', 16512350, NULL, 'Jl. Tubagus Ismail II no 10', '-6.911542499999999', '107.60821499999997', '11', ' Jl. Taman Polonia III no. 70', '087868919833', 'stanleysantoso@hotmail.com'),
(94, 120, 3, 'Tegar Aji Pangestu', NULL, 'Mojokerto', '0000-00-00', 'Teknik Informatika', 16512342, NULL, 'Jalan Pelesiran 57a/56', '-6.889499199999999', '107.61265400000002', '11', 'Jalan Wuni no 9 Perum Magersari Indah Mojokerto', '085646172290', 'tegarnization@gmail.com'),
(95, 123, 2, 'Monika Sembiring', NULL, 'Kabanjahe', '1994-11-02', 'Sistem Teknologi Informasi', 16512287, NULL, 'Jl. Pelesiran 115/56', '-6.911542499999999', '107.60821499999997', '11', 'Jl. Jamin Ginting Comp. Pijer Podi Kabanjahe', '085297526352', 'monikasembiring34@yahoo.com'),
(96, 124, 6, 'Muhamad Ibnu Qoyim', NULL, 'Bandung', '1994-03-01', 'Sistem Teknologi Informasi', 16512063, NULL, 'jl. taman hewan no.7', '-6.9147444', '107.6098111', '11', 'jl. saar no. 94, cililin, bandung barat', '089673569437', 'muhamadibnu9@gmail.com'),
(97, 125, 1, 'Dariel Valdano', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512328, NULL, 'Jl. Cisitu Lama 9 no. 32', '-6.322028644063397', '106.76360634230957', '14', 'Jl. Puspa Asri no. D5 Pisangan Ciputat Tangerang', '087788616270', 'dariel24@gmail.com'),
(99, 127, 3, 'Aryya Dwisatya Widigdha', NULL, 'Lumajang', '0000-00-00', 'Teknik Informatika', 16512255, NULL, 'Jalan Kebon Kembang 1', '-6.9012472', '107.60821499999997', '10', 'Jalan Raya Tempeh 114 Lumajang', '087757214299', 'a.dwisaty4@yahoo.com'),
(100, 128, 6, 'Danang Afnan Hudaya', NULL, 'Surakarta', '0000-00-00', 'Teknik Informatika', 16512379, NULL, 'Jl Bijaksana III no 9, Sukajadi', '-6.9147444', '107.6098111', '11', 'Jl Siwalan no 36A Kerten Surakarta', '081326455055', 'afnanhudaya@gmail.com'),
(101, 129, 10, 'Michael Alexander Wangsa', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512331, NULL, 'Terusan Purabaya 80/66', '-6.9107601', '107.59489859999996', '11', 'Terusan Purabaya 80/66', '08986438008', 'michaelaw320@gmail.com'),
(102, 130, 9, 'Taufiq Akbar Rosyadi', NULL, 'Cirebon', '1994-08-03', 'Sistem Teknologi Informasi', 16512390, NULL, 'jalan Cisitu lama VA/160B RT 07 RW 12', '-6.9147444', '107.6098111', '11', 'Desa Gamel RT 02 RW 02 Kec Plered Kab Cirebon', '085724242563', 'taufiqar@ymail.com'),
(103, 132, 5, 'WILLY', NULL, 'Medan', '0000-00-00', 'Teknik Informatika', 16512093, NULL, 'Jalan Ciumbuleuit gang U Sukandi 29/155C', '-6.9147444', '107.6098111', '11', 'Jalan Kuningan nomor 7D Medan', '081988476', 'womzz_willy2706@yahoo.co.id'),
(104, 133, 7, 'Fauzan Hilmi Ramadhian', NULL, 'Tasikmalaya', '0000-00-00', 'Teknik Informatika', 16512303, NULL, 'Jl. Cisitu Lama No.45B', '-6.911542499999999', '107.60821499999997', '10', 'Jl. Pelabuan 2 No. 20 Sukabumi', '085795630596', 'fhilmir@gmail.com'),
(105, 134, 3, 'Kevin Maulana', NULL, 'Bandung', '1994-05-26', 'Teknik Informatika', 16512044, NULL, 'Komp. Biofarma No. 12 Jalan Gunung Batu', '-6.9147444', '107.6098111', '2', 'Jalan Pondok Jingga Mas II Blok F3/6 Bekasi Selatan', '081361342705', 'kevhnmay94@gmail.com'),
(108, 137, 8, 'Edmund Ophie', NULL, 'Medan', '1971-10-13', 'Teknik Informatika', 16512164, NULL, 'Cisitu Baru 46', '-6.9147444', '107.6098111', '4', 'Hayam Wuruk Kedamaian Asri 3/12', '081224268134', 'edmund.ophie@yahoo.com'),
(109, 138, 8, 'Calvin Sadewa', NULL, 'Rantauprapat', '1995-12-28', 'Teknik Informatika', 16512018, NULL, 'jl.dago timur', '-6.875876705100802', '107.61869072914124', '13', 'JL.Agus salim', '082364996779', 'master23680@gmail.com'),
(110, 139, 2, 'Fitri Indah Cahyani', NULL, 'Ciamis', '1994-05-16', 'Sistem Teknologi Informasi', 16512309, NULL, 'Komplek Puri Cipageran Indah 1 Blok H1 No.83, Cimahi', '-6.9147444', '107.6098111', '11', 'Komplek Puri Cipageran Indah 1 Blok H1 No.83, Cimahi', '085721107512', 'fitri_indah_cahyani@yahoo.com'),
(111, 140, 8, 'Yollanda Sekarrini', NULL, 'Bukittinggi', '1994-10-17', 'Teknik Informatika', 16512062, NULL, 'Jl. Cisitu Lama I No. 112', '-6.8799774', '107.61329279999995', '17', 'Jl. Berumbung IV No. 1, Aur Kuning, Bukittinggi', '085767438495', 'yollandasekarrini@gmail.com'),
(112, 141, 1, 'Luthfi Hamid Masykuri', NULL, 'Wonosobo', '0000-00-00', 'Teknik Informatika', 16512119, NULL, 'Jl. Cisitu Lama Gang 5 No 42B', '-6.9147444', '107.6098111', '11', 'Kusuma Baru RT 20/08, Kertek, Wonosobo, Jawa Tengah', '085292871348', 'luthfi_hamid_m@yahoo.co.id'),
(113, 142, 10, 'Joshua Bezaleel Abednego', NULL, 'Sukabumi', '1996-08-07', 'Teknik Informatika', 16512301, NULL, 'Jl. Cisitu Lama 9 no 32', '-6.88185252929417', '107.6118243915405', '14', 'Jl. Bhayangkara no 12, Sukabumi', '081936063113', 'joshua.bezaleel@gmail.com'),
(114, 143, 1, 'Choirunnisa Fatima', NULL, 'Semarang', '1994-09-09', 'Teknik Informatika', 16512010, NULL, 'Jalan Dago Timur no. 4', '-6.879207699999999', '107.61265400000002', '11', 'Jl. Pucang Kerto II/14 Pucang Gading, Mranggen, Demak', '085876817870', 'choirunnisa.fatima@gmail.com'),
(115, 147, 2, 'Fajar Nurhaditia Putra', NULL, 'Pati', '0000-00-00', 'Sistem Teknologi Informasi', 16512153, NULL, 'Jalan cisitu lama no.5/160b rt7 rw12 kel.dago Kec.Coblong 40136', '-6.911542499999999', '107.60821499999997', '11', 'Kel.Parenggan Rt4 Rw1 no.208 Pati 59119', '089636596790', 'fafajar94@gmail.com'),
(116, 150, 3, 'Jacqueline Ibrahim', NULL, 'Hamburg', '0000-00-00', 'Teknik Informatika', 16512277, NULL, 'Bangbayang', '-6.911542499999999', '107.60821499999997', '11', 'Bangbayang, Bandung', '085720286295', 'hotaru_i@ymail.com'),
(117, 151, 6, 'Sheila Hana Fitriani', NULL, 'jakarta', '0000-00-00', 'Sistem Teknologi Informasi', 16512121, NULL, 'jl. sultan tirtayasa no. 34. citarum, bandunhg wetan. (deket gempol)', '-6.9147444', '107.6098111', '11', 'jl. h. lebar no.100. meruya utara, kembangan, jakarta barat', '085716461695', 'dexelmine@yahoo.com'),
(118, 152, 6, 'Tirta Wening Rachman', NULL, 'Bandung', '1993-09-27', 'Teknik Informatika', 16512239, NULL, 'sda', '-6.895933', '107.649608', '11', 'Jl.Pasir Leutik No F27 06/09 kel. Sukapada kec. Cibeunying Kidul, Bandung, 40125', '085795980141', 'tirtawening@gmail.com'),
(120, 155, NULL, 'Erlangga', NULL, 'Bandung', '1993-10-09', 'Sistem Teknologi Informasi', 16512147, NULL, 'jl purbasari II no 8', '-6.8510071', '107.54163949999997', '11', 'jl purbasari II no 8', '085721256747', 'erlanggawulung@yahoo.com'),
(121, 156, 5, 'Arum Adiningtyas', NULL, 'Jakarta', '1994-03-27', 'Sistem Teknologi Informasi', 16512068, NULL, 'Jl. Ir. H. Juanda Gg. Abah Tamim no. 22/158B', '-6.878561', '107.616989', '15', 'Jl. Horison Jaya A27/4 Bekasi', '085719205470', 'arum.adiningtyas@live.com'),
(122, 157, 7, 'Bagaskara Pramudita', NULL, 'Surabaya', '1994-10-24', 'Teknik Informatika', 16512197, NULL, 'Tubagus Ismail V no.34', '-6.88320499924914', '107.61727452278137', '16', 'Perumahan persada asri g-11, kediri', '085784041155', 'bagas_party_dorks@yahoo.com'),
(123, 158, 6, 'Ahmad Shahab', NULL, 'Bandung', '0005-11-28', 'Teknik Informatika', 16512282, NULL, 'Jl. Cisitu Indah 1 no.40', '', '', '16', 'Jl. AMD xx no.27 Condet jaktim', '087883403928', 'ashahab28@gmail.com'),
(125, 160, 8, 'Vidia Anindhita', NULL, 'Jakarta', '1993-05-16', 'Teknik Informatika', 16512041, NULL, 'Jl. Kanayakan Baru No. 31A', '-6.9147444', '107.6098111', '8', 'KPP IPB Baranangsiang IV/D-4, Bogor', '08567175005', 'vidianindhita@yahoo.com'),
(126, 161, 5, 'Muhammad Reza Irvanda', NULL, 'Muara Enim', '1994-12-08', 'Teknik Informatika', 16512191, NULL, 'Jl. Pelesiran No. 19', '-6.895699', '107.60705669999993', '5', 'Jl. Guru Sinumba II No. 18 Medan', '085762378535', 'muzavan@gmail.com'),
(127, 162, 8, 'Mario Filino', NULL, 'Jakarta', '1994-05-17', 'Teknik Informatika', 16512216, NULL, 'Jl. Ir. H. Juanda no. 454', '-6.868087272893472', '107.62115651741624', '17', 'PTB Duren Sawit Blok Q5/1, Jakarta Timur', '081384497255', 'mario.phantasy@gmail.com'),
(128, 163, 7, 'Muntaha Ilmi', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512057, NULL, 'Jl. Pelesiran No. 19', '-6.9012472', '107.60821499999997', '13', 'Jl. Swadaya 1, Gg. Dadap no. 12, Jakarta Selatan', '081282345008', 'surambisius@gmail.com'),
(129, 164, 2, 'Rakhmatullah Yoga Sutrisna', NULL, 'Semarang', '1993-10-11', 'Teknik Informatika', 16512178, NULL, 'Jl. Pelesiran No. 29', '-6.896370746299447', '107.60773658752441', '16', 'Jl. Delima II No. 25 RT 03 RW 02 Kel. Procot, Kec. Slawi, Kab. Tegal, Jawa Tengah', '085721629079', 'rakhmatullahyoga@students.itb.ac.id'),
(130, 165, 5, 'Nisa Dian Rachmadi', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512157, NULL, 'jl. venus barat VI no. 86e', '-6.9147444', '107.6098111', '11', 'jl. venus barat VI no.86e', '085221464301', 'nniso.ong@gmail.com'),
(132, 167, 1, 'Kevin Yudi Utama', NULL, 'Medan', '1994-06-07', 'Teknik Informatika', 16512180, NULL, 'Jl Ciumbuleuit Gg U Sukandi No. 29/155C', '-6.911542499999999', '107.60821499999997', '6', 'Jl. Brigjend Katamso Dalam No.130', '087731802871', 'kevin.kayu@gmail.com'),
(133, 169, 9, 'Diah Fauziah', NULL, 'Bukittinggi', '0000-00-00', 'Teknik Informatika', 16512070, NULL, 'jalan cisitu lama no.89/154C', '-6.879207699999999', '107.61265400000002', '11', 'jalan hamka no 38 gurun panjang, Bukittinggi', '085374566905', 'diahfauziah09@yahoo.com'),
(135, 179, 5, 'Nura Tresna Priantini', NULL, 'Ciamis', '1994-04-29', 'Sistem Teknologi Informasi', 16512310, NULL, 'Jl. Bangbayang No 26/157D Dago', '-6.911542499999999', '107.60821499999997', '10', 'Jl. Jalatrang RT 06 RW 04 Dusun Kota Desa Buniseuri Kecamatan Cipaku Kabupaten Ciamis-Jawa Barat', '08972003738', 'nura.tresna29@ymail.com'),
(136, 181, 3, 'Bella Claudia Nur Azizah', NULL, 'Bandung', '1994-10-21', 'Sistem Teknologi Informasi', 16512054, NULL, 'Jl Cisitu Lama XI no 4', '-6.9147444', '107.6098111', '11', 'Jl Haruman IV no 48 RT02/RW06 Ujungberung 40611 Bandung', '085721494106', 'bellaclaudia21@gmail.com'),
(137, 182, 4, 'Andre Susanto', NULL, 'Tangerang', '0000-00-00', 'Teknik Informatika', 16512246, NULL, 'Cisitu Indah V No 72', '-6.875567', '107.61291660000006', '11', 'Bugel Mas Indah A7/14, Tangerang', '08568906325', 'z@andresusanto.info'),
(138, 183, 8, 'Windy Amelia', NULL, 'Jakarta', '1994-10-08', 'Teknik Informatika', 16512359, NULL, 'Jl. Kanayakan Baru no. 31A Dago', '-6.9147444', '107.6098111', '11', 'Kav. DPRD Blok J No. 10 cibubur Jakarta Timur', '08568554061', 'windyamelia94@yahoo.com'),
(141, 186, 7, 'Denny Astika Herdioso', NULL, 'Semarang', '0000-00-00', 'Teknik Informatika', 16512213, NULL, 'Jl. Sadang Luhur no.12', '-6.911542499999999', '107.60821499999997', '11', 'Rt3 RwX Panjangsari Baru, Parakan, Temanggung', '085725792688', 'herdi_oso@yahoo.co.id'),
(142, 187, 10, 'Ichwan Haryo Sembodo', NULL, 'Bekasi', '0000-00-00', 'Teknik Informatika', 16512226, NULL, 'jalan cisitu lama 5 no 46 blok 154b', '-6.9147444', '107.6098111', '10', 'Pulo RT02/06, Eromoko, Wonogiri', '085728165503', 'ichwanharyosembodo96@gmail.com'),
(143, 188, 4, 'Muhammad Yafi', NULL, 'Jakarta', '1994-09-06', 'Teknik Informatika', 16512337, NULL, 'Jalan Tubagus Ismail VIII no. 17', '-6.9147444', '107.6098111', '11', 'Jalan Ontorejo no. 14 Wirobrajan Yogyakarta', '085729592442', 'yafithekid212@gmail.com'),
(144, 189, 2, 'Luqman Faizlani Kusnadi', NULL, 'Malang', '0000-00-00', 'Teknik Informatika', 16512220, NULL, 'Jalan Cisitu Lama 54', '-6.9147444', '107.6098111', '10', 'Jalan Margobasuki 31, Dau, Malang', '081214593043', 'luqmankusnadi@gmail.com'),
(145, 190, 7, 'Cilvia Sianora Putri', NULL, 'Sidoarjo', '0000-00-00', 'Teknik Informatika', 16512074, NULL, 'Jalan Tubagus Ismail VI no.17', '-6.883716544868408', '107.61842262571713', '15', 'Taman Pinang Indah F4-15 Sidoarjo', '08993831581', 'yagamicilvi@yahoo.com'),
(146, 191, 7, 'Riska', NULL, 'Bajamas', '1994-05-14', 'Teknik Informatika', 16512073, NULL, 'jalan pelesiran nomor 26', '-6.911542499999999', '107.60821499999997', '10', 'kel. Bajamas, kec.Sirandorung,kab.Tapanuli Tengah,Sumatera Utara', '085370294043', 'riskasaja50@yahoo.com'),
(147, 192, 3, 'Susanti Gojali', NULL, 'Jakarta', '0000-00-00', 'Teknik Informatika', 16512242, NULL, 'tubagus ismail 7 no 15', '-6.883386074852849', '107.61921644210815', '17', 'Teluk Gong jln B raya no 176A Jakarta', '081905153850', 'susantigojali@hotmail.com'),
(148, 194, 5, 'Azfina Putri Anindita', NULL, 'Bogor', '1995-05-15', 'Sistem Teknologi Informasi', 16512224, NULL, 'Dago Asri C-33', '-6.8772291790998095', '107.61511228518066', '13', 'Wisma Harapan 2 Jalan Edelweis Blok G2 nomor 5 Depok', '08999545678', 'azfinap@gmail.com'),
(149, 195, 10, 'Arieza Nadya Sekariani', NULL, 'Jakarta', '1996-03-21', 'Teknik Informatika', 16512349, NULL, 'Dago asri', '-6.911542499999999', '107.60821499999997', '9', 'Tebet Timur, Jakarta Selatan', '081295032052', 'ariezanadya@gmail.com'),
(151, 197, 8, 'Bintang Subuh Puntodewo', NULL, 'Jakarta', '1994-05-31', 'Sistem Teknologi Informasi', 16512231, NULL, 'Jalan cisitu indah 2', '-6.911542499999999', '107.60821499999997', '11', 'Jalan haji najih no 6 Petukangan utara Jakarta', '083820548309', 'bintangsubuh@hotmail.com'),
(153, 199, 5, 'Willy', NULL, 'Medan', '1994-04-25', 'Teknik Informatika', 16512230, NULL, 'ciumbeluit GG Suhari 96C/155A', '-6.911542499999999', '107.60821499999997', '11', 'jalan Kpt Maulana Lubis 14C, Medan', '081361714046', 'opelhoward@yahoo.com'),
(154, 200, 8, 'Taufik Akbar Abdullah', NULL, 'Bandung', '0000-00-00', 'Sistem Teknologi Informasi', 16512045, NULL, 'Komp. Bakung Endah no 23 Margasari Buah Batu', '-6.9551286', '107.64857180000001', '11', 'Komp. Bakung Endah no 23 Margasari Buah Batu, Bandung', '082129136223', 'taufik.akbar7@gmail.com'),
(155, 201, 7, 'Ahmad Zaky', NULL, 'Jakarta', '1995-04-17', 'Teknik Informatika', 16512398, NULL, 'Jl. Cisitu Lama III no. 24a/154c, Bandung 40135', '-6.9147444', '107.6098111', '11', 'Jalan Al-Washliyah no. 19 RT 003/04 kel. Jati, kec. Pulogadung, Jakarta Timur 13220', '08170009891', 'a_zaky003@yahoo.com'),
(156, 202, 4, 'Bram Galih Arianto', NULL, 'Surakarta', '1994-06-10', 'Sistem Teknologi Informasi', 16512227, NULL, 'Jalan Dago Biru 26A Dago, Coblong, Bandung 40135', '-6.874870287415836', '107.61527988128364', '17', 'Jalan Siwalan 37A Kerten, Laweyan, Surakarta', '085878823876', 'bramgalih36@gmail.com'),
(157, 203, 8, 'Untari Zaeca Warjani', NULL, 'Waru', '1994-01-26', 'Sistem Teknologi Informasi', 16512080, NULL, 'Jalan Tamansari 52/56', '-6.911542499999999', '107.60821499999997', '9', 'Jalan Padat Karya, Kalimantan Timur', '085753473245', 'untari.zaeca@gmail.com'),
(158, 204, 6, 'Noor Afiffah Huwaidah', NULL, 'Belinyu', '1994-05-06', 'Sistem Teknologi Informasi', 16512307, NULL, 'Jalan Sanggar Kencana VII Sanggarhurip', '-6.911542499999999', '107.60821499999997', '7', 'Jalan Sanggar Kencana VII Sanggarhurip', '088802365358', 'nifa.afiffah@yahoo.com'),
(159, 205, 8, 'Fahziar Riesad Wutono', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512381, NULL, 'Jl. Abadi Regency Raya No. 9 Komplek Abadi Regency Gegerkalong Girang Bandung', '-6.8660066', '107.58750090000001', '11', 'Jl. Kampung Melati 10H Kec. Kesambi Kel. Kesambi Kota Cirebon', '08562390272', 'fahziar_rw@yahoo.co.uk'),
(160, 206, 2, 'Junita Sinambela', NULL, 'Medan', '1995-06-11', 'Teknik Informatika', 16512047, NULL, 'Jalan Cisitu Baru Dalam 6', '', '', '16', 'Laehole II Parbuluan, Kab Dairi, SumUt', '085360174698', 'jujunale@gmail.com'),
(161, 207, 10, 'Viktor Trimulya Buntoro', NULL, 'Yogyakarta', '1995-04-24', 'Teknik Informatika', 16512351, NULL, 'Jalan Sangkuriang', '-6.9147444', '107.6098111', '11', 'Jalan Sutopadan', '081804168524', 'viktor_713@yahoo.com'),
(162, 208, 10, 'Farid Fadhil Habibi', NULL, 'Kab.Semarang', '0000-00-00', 'Sistem Teknologi Informasi', 16512150, NULL, 'Jl.Cisitu Lama No.24/160B RT:06 RW:12, Kel.Dago, Kec.Coblong, Bandung, Jawa Barat', '-6.9147444', '107.6098111', '20', 'Ds.Rejosari RT:10 RW:02 No.15, Kel.Genuk, Kec.Ungaran Barat, Kab.Semarang, Jawa Tengah', '085727977733', 'fadhil.habibi@gmail.com'),
(163, 210, 7, 'Aurelia', NULL, 'Jakarta', '1994-10-11', 'Teknik Informatika', 16512089, NULL, 'Jl. Kanayakan Baru No. 19, Dago.', '-6.9147444', '107.6098111', '11', 'Jl. Percetakan Negara Raya No. 19, Jakarta Pusat.', '085691141448', 'aurellerua@ymail.com'),
(164, 211, 3, 'Marcelinus Henry M.', NULL, 'Bandung', '1994-05-27', 'Teknik Informatika', 16512240, NULL, 'Permata Cimahi V7 no .6', '-6.862339571367621', '107.52110508345947', '6', 'Permata Cimahi V7 no. 6', '081394410971', 'henrymenori@yahoo.com'),
(165, 212, 9, 'poetra lumaksono', NULL, 'jakarta', '0000-00-00', 'Sistem Teknologi Informasi', 16512071, NULL, 'Jl. Ir. H. Juanda 319B', '-6.911542499999999', '107.60821499999997', '10', 'Komp. Samudera Indonesia 17', '087781455581', 'poetra_roos@yahoo.com'),
(167, 214, 1, 'Jeffrey Lingga Binangkit', NULL, 'Surakarta', '1994-06-15', 'Teknik Informatika', 16512037, NULL, 'Jalan Tubagus Ismail XVII No. 55c, Sekeloa, Coblong, Kota Bandung, Jawa Barat, 40134', '-6.885359999999999', '107.62005240000008', '11', 'Sidomulyo, RT 24/I, Ngembatpadas, Gemolong, Sragen, Jawa Tengah, 57274', '085724715978', 'jeffhorus19@gmail.com'),
(168, 215, 2, 'Muhammad Ibrahim Al Muwahidan', NULL, 'Jakarta', '1994-05-04', 'Sistem Teknologi Informasi', 16512383, NULL, 'Jalan Cisitu Lama no. 7 Bandung, Jawa Barat', '-6.881745740391982', '107.61155605316162', '17', 'Jalan Karang Pola no. 5 Pasar Minggu, Jakarta Selatan', '085714858905', 'almuwahidan@gmail.com'),
(169, 216, 4, 'Chrestella Stephanie', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512387, NULL, 'Taman Holis Indah F1 - 28 Bandung', '-6.9667107724300115', '107.57591620087624', '16', 'Taman Holis Indah F1 - 28 Bandung', '081312117979', 'chres_tella08@yahoo.com'),
(170, 217, 10, 'Khaidzir Muhammad Shahih', NULL, 'Bandung', '1994-12-06', 'Teknik Informatika', 16512279, NULL, 'Jl. Surapati No. 143/144 C', '-6.899204626018615', '107.62394048273563', '21', 'Jl. Surapati No. 143/144 C, Bandung', '081322975540', 'dzir.shhh@gmail.com'),
(171, 220, 6, 'Andhina Amalia Ratnaputri', NULL, 'Jakarta', '0000-00-00', 'Sistem Teknologi Informasi', 16512104, NULL, 'Jl. Dago Asri IV No H-6', '-6.9147444', '107.6098111', '11', 'Taman Aries A10 No 19 Meruya Jakarta Barat 11620', '085781333435', 'adinratnaputri@gmail.com'),
(172, 221, 9, 'Mochamad Lutfi Fadlan', NULL, 'Jakarta', '1994-04-23', 'Teknik Informatika', 16512176, NULL, 'Jalan babakan ciamis no.73B', '-6.9147444', '107.6098111', '11', 'Jalan kemuning no.206, komplek p&k, tangerang', '085694357352', 'Mochamadlutfifadlan@gmail.com'),
(173, 222, 6, 'Indam Muhammad', NULL, 'Bandung', '0000-00-00', 'Teknik Informatika', 16512293, NULL, 'Jalan Guntur Sari Kulon no. 5 Buah Batu Bandung', '-6.911542499999999', '107.60821499999997', '10', 'Jalan Guntur Sari Kulon no. 5 Buah Batu Bandung', '08562230777', 'indam.muhammad@yahoo.com'),
(174, 223, 4, 'Alvin Natawiguna', NULL, 'Jakarta', '1994-06-26', 'Teknik Informatika', 16512040, NULL, 'Jl. Ciumbuleuit No. 85', '-6.9147444', '107.6098111', '11', 'Jl. Cabe Rawit III No. 16, Bojong Indah, Cengkareng, Jakarta Barat', '081282222028', 'alvin.nt.gg@gmail.com'),
(175, 224, 6, 'Jonathan', NULL, 'Jakarta', '1994-06-17', 'Teknik Informatika', 16512270, NULL, 'Ence azis nomor 40', '-6.9187501', '107.59867110000005', '16', 'Taman Pegangsaan Indah blok E nomor 3', '08988881551', 'Light_jo@ymail.com'),
(176, 225, 2, 'Kanya Paramita', NULL, 'Bandung', '1992-05-28', 'Teknik Informatika', 16512317, NULL, 'Jl. Tilil no 1 Bandung', '-6.892855', '107.62597129999995', '11', 'Jl. Tilil no 1 Bandung', '085659355236', 'kanyaparamita@gmail.com'),
(177, 226, 1, 'Ihsan Naufal Ardanto', NULL, 'Jakarta', '1995-03-25', 'Teknik Informatika', 16512259, NULL, 'Jl. Cisitu Lama IX no.21', '-6.911542499999999', '107.60821499999997', '12', 'Jl. Arjuna 6 No. 9 Perumahan Bumi Satria Kencana, Kota Bekasi, Jawa Barat', '081319279337', 'ardantoihsan@yaho.com'),
(178, 227, 7, 'Adhika Sigit Ramanto', NULL, 'Jakarta', '1995-05-06', 'Teknik Informatika', 16512333, NULL, 'Jl. Cisitu Lama VIII No. 1 A', '-6.879449662059862', '107.61363375931978', '16', 'Jl. Duta Permai 1 No. 10 Pondok Indah Jakarta Selatan', '0811191255', 'adhikasigit@gmail.com'),
(180, 229, 4, 'teofebano kristo', NULL, 'pangkal pinang', '1994-02-19', 'Teknik Informatika', 16512067, NULL, 'jalan cisitu lama VIII no 1a', '-6.9147444', '107.6098111', '11', 'perumahan citra garden 6 blok G01 no 60', '08979465639', 'teofebano.stei@gmail.com'),
(181, 230, NULL, 'Sample Account', NULL, 'Mexico City', '2013-07-10', 'Teknik Informatika', 16500000, NULL, 'Jl. Ganesha no. 2', '-6.911542499999999', '107.60821499999997', '9', 'Sunset Blvd. E5', '0822500700', 'vivinurafiah@live.co.uk'),
(182, 231, NULL, 'Iskandar Setiadi', NULL, 'Jakarta', '1994-01-20', 'Teknik Informatika', 16511303, NULL, 'Jl. Tubagus Ismail VI No. 33', '-6.883557833594946', '107.61869457625733', '15', 'Jl. Pademangan III Gg. 7 No. 12A', '085697681829', 'iskandarsetiadi@students.itb.ac.id');

-- --------------------------------------------------------

--
-- Table structure for table `peserta_tugas`
--

CREATE TABLE IF NOT EXISTS `peserta_tugas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tugas` int(11) NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `link` varchar(128) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pranala_tugas_unggah` varchar(100) NOT NULL,
  `status` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tugas` (`id_tugas`),
  KEY `tugas_peserta` (`id_peserta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poin`
--

CREATE TABLE IF NOT EXISTS `poin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) DEFAULT NULL,
  `id_kelompok` int(11) DEFAULT NULL,
  `penerima` int(2) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `poin` int(5) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `peserta_poin` (`id_peserta`),
  KEY `kelompok_poin` (`id_kelompok`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text,
  `kategori` varchar(15) NOT NULL,
  `timestamp_published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `judul`, `deskripsi`, `kategori`, `timestamp_published`, `timestamp_modified`, `status`) VALUES
(7, 'Website Sparta v 1.0.0', '<p>Selamat datang di Website Sparta HMIF 2012!<br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'pengumuman', '2013-06-23 20:39:16', '2013-06-23 20:39:16', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sakit`
--
CREATE TABLE IF NOT EXISTS `sakit` (
`id` int(11)
,`id_peserta` int(11)
,`motivasi` text
,`penyakit_pernah` text
,`penyakit_sedang` text
,`golongan_darah` varchar(2)
,`nomor_kerabat` varchar(100)
,`nomor_ortu` varchar(100)
,`kebersamaan` text
,`keaktifan` text
,`inisiatif` text
,`kesadaran_mahasiswa` text
,`kesadaran_bangsa` text
,`id_akun` int(11)
,`id_kelompok` int(11)
,`nama_lengkap` varchar(100)
,`nama_panggilan` varchar(100)
,`tempat_lahir` varchar(50)
,`tanggal_lahir` date
,`jurusan` varchar(100)
,`nim_stei` int(8)
,`nim_jurusan` int(8)
,`alamat` varchar(128)
,`alamat_latitude` varchar(100)
,`alamat_longitude` varchar(100)
,`alamat_zoom` varchar(10)
,`alamat_rumah` varchar(100)
,`no_telp` varchar(20)
,`email` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `tambahan`
--

CREATE TABLE IF NOT EXISTS `tambahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NOT NULL,
  `motivasi` text NOT NULL,
  `penyakit_pernah` text NOT NULL,
  `penyakit_sedang` text NOT NULL,
  `golongan_darah` varchar(2) NOT NULL,
  `nomor_kerabat` varchar(100) DEFAULT NULL,
  `nomor_ortu` varchar(100) NOT NULL,
  `kebersamaan` text NOT NULL,
  `keaktifan` text NOT NULL,
  `inisiatif` text NOT NULL,
  `kesadaran_mahasiswa` text NOT NULL,
  `kesadaran_bangsa` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tambahan_peserta` (`id_peserta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `tambahan`
--

INSERT INTO `tambahan` (`id`, `id_peserta`, `motivasi`, `penyakit_pernah`, `penyakit_sedang`, `golongan_darah`, `nomor_kerabat`, `nomor_ortu`, `kebersamaan`, `keaktifan`, `inisiatif`, `kesadaran_mahasiswa`, `kesadaran_bangsa`) VALUES
(9, 13, 'bagus', 'gak ada', 'gak ada', 'A', '', '081234567', '1', '2', '1', '1', '1'),
(10, 14, 'bagus', 'kencing lancar', 'kudis', 'A', 'ndak ada', '0812334566', '1', '1', '1', '1', '1'),
(11, 15, 'bagus', 'kudis', 'kudis', 'A', '1', '1', '1', '1', '1', '1', '1'),
(12, 16, '.''', '.''', '.''', '.''', '.''', '.''', '.''', '.''', '.''', '.''', '.'''),
(13, 17, 'Mau menjadi koorlap SPARTA', 'Sakit hati', 'tidak ada', 'A', '08122008515', '08122008515', '100', '9', '7', '8', '8'),
(14, 18, 'senang-senaang', 'pusing forsil', 'qwe', 'qw', 'qeqwe', 'qweqer', 'qweqwe', 'qeqe', 'qwrqr', 'qwrqer', 'qwerqer'),
(15, 19, 'pengen aja', 'tidak ada', 'sakit hati mungkin', 'B', 'ga ada', '087825996140', '10', '10', '10', '10', '10'),
(16, 20, 'Ingin mengaplikasikan salah satu budaya kampus yaitu berhimpun, mengembangkan potensi diri, ingin menambah teman dengan rumpun yg sama, ingin menjadi bagian dari HMIF.', 'Asma, maag, migrain', 'Asma, maag, migrain', 'O', '081220185798', '08159224775', 'Captcha sedang dalam proses untuk menjadi lebih solid, namun pergaulannya masih terkotak-kotak, walau sudah mulai mencoba untuk membaur satu sama lain. 6. ', 'Captcha tergolong belum terlalu aktif, karena hanya segelintir orang yang berani untuk mengutarakan pendapatnya secara langsung, perlu ditingkatkan lagi. 4. ', 'Captcha tergolong kurang inisiatif. Saat forum dan diminta pendapat hanya sedikit yg mau berpendapat, untuk berinisiasi memberi ide atau menjadi PJ suatu tugas juga masih harus ditekankan lagi. 3. ', 'Captcha cukup banyak yang mengikuti diklat terpusat. Keaktifan STEI 2012 dalam pemilihan pemira juga cukup tinggi. 7. ', 'Pada awal masuk ITB saat ada baksos angkatan ITB 2012 1stmovement, partisipasi Captcha masih kurang, namun setelah diklat OSKM sepertinya sudah cukup meningkat. 5. '),
(17, 21, 'Ingin mempelajari dan mendalami kegiatan berhimpun seperti berdiskusi dan musyawarah serta kepemimpinan', 'Typhus dan Maag *kambuhan*', '-', 'A', '081392254571 ', '081319426266', '9 karena semua sangat kooperatif walaupun belum semua dapat hadir semua', '8 karena banyak yang sudah aktif dalam berdikusi dan aktif menyatakan pendapatnya', '9 karena banyak inisiatif yang dilakukan, mulai dari membuat temu angkatan hingga logo yang dikerjakan secara spontan tanpa dipilih-pilih', '7 menurut saya cukup sadar akan isu isu dari pusat dan mulai untuk membahas dalam sekala kecil', '8 karena sering diangkat dalam PLO maka angkatan kami jadi lebih sadar akan malasah bangsa'),
(18, 26, 'Ingin menambah pengalaman dan koneksi', 'Amputasi', 'Tidak ada', 'A', '', '085262249949', 'Cukup baik (8)', 'Menurut saya baik,karena banyak yang mengikuti banyak kegiatan (9)', 'Tidak begitu baik,karna butuh perintah dahulu (6)', 'Banyak yang ikut di KM,menurut saya cukup baik (8)', 'Belum begitu terlihat (7)'),
(19, 28, 'Ingin masuk himpunan', 'Demam Berdarah, sering batuk', '-', 'o', '085642193742', '08122603915', '7', '8', '5', '6', '4'),
(20, 29, 'ingin aktif di himpunan', 'tipus', '-', 'B', '089660352505', '08563519017', 'menurut saya sudah baik, namun perlu ditingkatkan. 8 ', 'menurut saya sudah aktif, namun kurang menyeluruh, hanya beberapa anak saja. 8', 'menurut saya , angkatan 2012 ini cukup inisiatif untuk memberikan sesuatu yang lebih dari yang diminta. 9', 'menurut saya kesadarannya masih belum maksimal, karena sampai saat ini belum ada kerjasama nyata antara kemahasiswaan dan angkatan kami. 7', 'menurut saya sangat baik, karena kita sering  melakukan kegiatan sosial yang membantu menyelesaikan masalah negara mulai dri yang sederhana. 9'),
(21, 30, 'Agar dapat bergabung dengan keluarga besar HMIF', '-', '-', 'O', '085722199602', '081320642636', '8', '7', '7', '7', '6'),
(22, 32, 'Saya ingin menjadi anggota HMIF, dan tentu saja ingin berkontribusi dan menambah pengalaman berorganisasi', '-', '-', 'A', '02292028264', '081510374412', '6, sebagian besar teman2 terpisah karena prodi yang berbeda, jd mgkin sekarang angkatan calon peserta hmif belum begitu menyatu.', '8, saat masih di STEI, angkatan kami melakukan kegiatan pengmas yang dinilai berjalan dengan sangat baik.', '7, masih ada orang2 yang belum berani berinisiatif, mengungkapkan pendapat, dan berbicara dalam forum', '7, cukup banyak dari kami yang hadir saat hearing calon presiden km-itb, dan banyak juga dari kami yang turut serta  dalam pemilihan', '8, seperti yang sudah dijelaskan angkatan STEI 2012 telah melakukan kegiatan pengabdian masyarakat yang dinilai cukup berhasil'),
(23, 34, '- semakin kenal dengan teman sejurusan maupun dengan STI.\r\n- jadi anggota HMIF.', 'Asma, tifus, cacar air', '-', 'A', '08982996122/087867743407/085260467717', '(061)4569472/087868958758', '5', '5', '5', '6', '5'),
(24, 36, 'Ingin menjadi bagian dari himpunan', 'Demam Berdarah', 'tidak ada', 'B', '089605380130', '089676093032', 'beberapa sudah berteman dengan baik, namun kami masih terpecah di beberapa kelompok, dan perlu disatukan 5', 'ini keaktifan yang bagaimana? kalo yang sering maju dan mengutarakan pendapat menurut saya 5, karena beberapa saja yang terlihat seperti itu, jadi menurut saya 5, kalo untuk di itb sendiri belum terlihat jadi 4', 'beberapa menunjukkan inisiatifnya, beberapa tidak, kalau dirata-ratakan menurut saya 5', 'menurut saya sudah cukup baik, cukup banyak yang ikut oskm, jadi nialinya menurut saya 7', 'karena menurut saya yang ikut oskm di ajarkan untuk sadar jadi penilaian saya seperti pertanyaan di atas 7'),
(25, 37, 'Ingin mengenal dunia himpunan dan ingin mengenal angkatan sendiri', 'Typhus', 'Tidak ada', 'A', '', '081321206306', 'Sudah mulai terlihat kebersamaannya dengan adanya kegiatan angkatan, namun masih kurang\r\nnilai : 8', 'Sudah ada segelintir orang yang aktif dalam kumpul angkatan, namun hanya segelintir\r\nnilai : 7', 'Inisiatif sudah bagus, sudah saling mengingatkan jika ada apa-apa, namun masih dari segelintir orang\r\nnilai : 8', 'Lumayan, dilihat dari jumlah orang yang mengikuti OSKM 2013, mungkin ada kegiatan lain sehingga tidak mengikuti OSKM 2013\r\nnilai : 6', 'Belum tau pasti, namun dengan adanya pengmas 2 bulan lalu angkatan saya sudah mulai sadar terhadap masalah ini\r\nnilai : 7'),
(26, 38, 'Ingin mengenal satu angkatan lebih dekat, ingin mengembangkan softskill melalui proses pengkaderan', 'sobek selaput otot engkel kaki', 'darah rendah', 'A', '081`282115267', '081357530228', 'saat ini kebersamaan dan kepedulian satu sama lain dalam angkatan jauh lebih besar dibandingkan sebelumnya. skala 7', 'angkatan 2012 sudah cukup aktif tetapi hanya pada beberapa orang saja. skala 6', 'semakin kesini inisiatif angkatan semakin tinggi. skala 8', 'angkatan 2012 sangat menghargai dan menjunjung tinggi kemahasiswaan terpusat. dapat dilihat dari banyaknya mahasiswa yang turut serta dalam kegiatan terpusat. skala 8', 'kesadaran angkatan juga cukup tinggi, salah satunya dengan pengadaan pengabdian masyarakat'),
(27, 40, 'Ingin mengenal lebih jauh dengan teman teman semua', 'Gejala tiffus', '-', 'B', '08562070384', '082316015410', '8', '8', '8', '7', '7'),
(28, 42, 'Ingin memperluas jaringan dan tentunya akrab dengan satu angkatan dong.~', 'Demam Berdarah, Diare, Campak Jerman, Cacar Air', 'Sedikit Batuk dan Pilek', 'B', '081932354848', '085691907959', 'Angkatan yang solid, kompak, dan akrab. Terbukti dengan diadakannya makrab yang berjalan cukup sukses~ Nilai : 9', 'Angkatan yang sangat aktif terbukti dalam diadakannya kegiatan-kegiatan angkatan seperti Pengmas STEI, dll. Nilai : 8', 'Inisiatif yang cukup baik terbukti dari kepedulian teman-teman seangkatan dalam berjalannya interaksi ini dengan gencar saling mengajak satu sama lain dan mengingatkan. Nilai : 8', 'Kesadaran angkatan dalam kemahasiswaan terpusat cukup baik. Nilai : 7', 'Kesadaran terhadap masalah bangsa dan masyarakat ckup baik. Contoh dalam Pengmas STEI, ada sumbangan beberapa buku terhadap beberapa anak terlantar seperti menyumbangkan buku, ilmu, dsb. Nilai : 8'),
(29, 43, 'Experience, family and glory', '-', '-', '0', '0222506940', '08156222827', 'Teman-temannya baik, banyak yang kenal ane tapi ane kadang lupa yang lain\r\n7', 'Aktif banget, \r\ntutorial sebelum uts? ada. Baksos? ada juga. Makan2 di Hanamasa? nyusul kayaknya....\r\n8', 'Rata2 orangnya lumayan inisiatif, inisiatif aktif dan pasif juga sih :D\r\n8', 'I don''t really know to be honest...\r\nBut rasanya banyak anak STEI yang aktif di KM juga so...\r\n6', 'Rasanya OSKM 2012 dan khusus untuk yang ikut OSKM 2013 cukup memacu kesadaran angkatan sih, walau masih ada juga yang biasa aja....\r\n6'),
(30, 44, 'Melatih soft skill, ingin mengenal satu angkatan dengan lebih baik, pengalaman', 'Malaria', 'Tidak ada', 'O', '08987001572', '089668099221', 'Akhir-akhir ini semakin membaik / makin akrab. 8', 'Yang aktif biasanya hanya beberapa, setelah itu menular. 6', 'Inisiatif biasanya muncul dari beberapa orang yang kemudian disetujui angkatan. 6', 'Baik, terbukti dengan banyaknya mahasiswa STEI yang turut serta dalam kegiatan OSKM. 8', 'Baik, terbukti dengan adanya kegiatan seperti first movement dan pengmas. 9'),
(31, 45, 'ingin masuk himpunan dan mengenal teman seangkatan serta kakak kelas', '-', '-', 'B', '085861136518', '0818384008', 'kami sudah cukup dekat satu sama lain dan menurut saya lumayan banyak yang dapat bekerja sama dengan baik (7)', 'angkatan kami aktif, dan ingin memberikan lebih dari yang diharapkan panitia (8)', 'inisiatif angkatan bagus karena dapat menemukan banyak ide dalam kumpul angkatan (8)', 'menurut saya sudah baik karena banyak dari kami yang ingin mengikuti oskm dan ikut dalam kegiatan kemahasiswaan lainnya (8)', 'kesadaran angkatan menurut saya sudah baik, hal ini dapat dilihat dari pengmas yang telah dilakukan (9)'),
(32, 46, 'Ingin masuk Himpunan, ingin meningkatkan softskills dan jaringan', 'TB', 'tidak ada', 'B', '0222534847', '0811262444', 'Mau terbuka dan bekerja sama satu dengan lainnya. 8', 'Angkatan saya banyak yang aktif dalam berbagai unit dan kepanitiaan serta aktif berbicara. 7', 'Angkatan saya cepat menginisiasi suatu hal untuk digunakan bersama. Contoh : pengmas, database angkatan. 7', 'Sebagian besar mengikuti kegiatan kemahasiswaan terpusat seperti oskm. 6', 'Dari cita-cita dan topik pembicaraan, sebagian besar ingin berperan aktif dalam membangun masyarakat dan mampu berdebat tentang masalah hal-hal masa kini sesuai pendapat masing-masing. 6'),
(33, 47, 'Mencari pengetahuan dan pengalaman, mengenal teman angkatan lebih akrab, belajar menjadi pribadi mahasiswa yang lebih baik, syarat masuk HMIF.', '-', '-', 'o', '087722019351', '085718556688', '8', '8', '8', '8', '7,5'),
(34, 49, 'Ingin masuk HMIF dan mengenal lebih jauh teman satu angkatan', 'demam berdarah', 'tidak ada', 'A', '08997974016', '081321287878', 'kebersamaan berjalan baik, kami kompak dan saling membantu, saya beri nilai 8', 'angkatan aktif, dengan mengadakan berbagai event kebersamaan, saya beri nilai 8', 'inisiatif sangat baik, setiap ada hal yang perlu dikerjakan ada inisiatif yang baik,saya beri nilai 8', 'kesadaran juga cukup baik dengan kita mengikuti hearing dan ada yang ikut diklat terpusat oskm, saya beri nilai 7', 'kesadaran sangat baik dibuktikan dengan adanya pengmas yang telah kami lakukan, saya beri nilai 9'),
(35, 50, '-Ingin menanamkan nilai-nilai yang seharusnya dimiliki oleh seorang mahasiswa pada diri saya.\r\n-Ingin menambah teman, mengenal satu angkatan.\r\n-Ingin mengubah diri saya menjadi lebih dewasa.', '-', '-', 'O', '085318138368', '083877452588', 'Kebersamaan angkatan sangat berubah dari yang awalnya terkotak-kotak hingga bisa melebur menjadi 1 beberapa bulan terakhir.\r\nnilai: 8', 'Keaktifan angkatan sudah lumayan bagus karena tidak begitu banyak orang dalam angkatan yang hanya diam dalam menghadapi berbagai macam hal.\r\nnilai: 8', 'Inisiatif sangat baik, berbagai macam solusi masalah serta acara-acara bermanfaat bisa didapatkan dengan adanya ide dan usulan dari teman-teman seangkatan.\r\nnilai: 9', 'Dengan banyaknya massa STEI 2012 yang menjadi panitia dalam berbagai kegiatan kemahasiswaan terpusat serta mengikuti diklat calon panitia OSKM 2013 saya rasa itu sudah bisa menjadi jawaban.\r\nnilai: 9', 'Diadakannya pengmas STEI beberapa waktu yang lalu juga saya rasa sudah cukup menjelaskan bagaimana angkatan kami menyikapi permasalahan yang ada di negeri ini.\r\nnilai: 8'),
(36, 51, 'Pingin aktif di himpunan. Pingin merasakan serunya osjur. Pingin bisa mengenal IF lebih dalam.', 'typhus', '-', '0', '0811915045', '0811104957', 'Sudah lumayan kompak, saya kasih nilai 8 dari 10', 'Aktif semuanya, tapi pada passion nya masing" berkisar 7 dari 10', 'Belum semuanya memiliki inisiatif masing" berkisar 7 dari 10', 'Menurut saya sudah lumayan walau belum semuanya yang turut serta berkisar 6 dari 10', 'sadar akan fenomena" yang terjadi di bangsa, walau masih ada yang perlu dorongan, 7 dari 10'),
(37, 52, 'Lebih mengenal angkatan dan untuk masuk himpunan', 'Tifus', 'tidak ada', '0', '081392614131', '081534697782', '8', '7', '9', '7', '8'),
(38, 54, 'Ingin belajar banyak tentang keorganisasian', 'Tifus, Demam Berdarah', 'tidak ada', 'B', '', '08159114131', 'Walaupun belum semua mengenal satu sama lain namun setelah beberapa kali wawancara saya merasa bahwa angkatan saya adalah angkatan yang solid jika kita semua sudah saling kenal. Ada juga yang masih malu malu untuk berkenalan tetapi mereka semua adalah teman yang baik dan dapat diandalkan. Skalanya 8', 'Angakatan kami kebanyakan kurang aktif mungkin mereka masih malu dan enggan dalam mengutarakan pendapat. Walaupun ada beberapa orang teman yang terbilang sudah sangat aktif dalam berbagai kegiatan angkatan. Skala 6', 'Inisatif angkatan sudah cukup baik terbukti adanya kegiatan pengmas yang cukup unik baru baru ini. Skala 7', 'Belum semuanya mementingkam kemahasiswaan terpusat skala 5', 'Saya pikir angakatan saya adalah angkatan yang peduli dengan bangsa dan masyarakat. Salah satu contohnya pengmas, dan sering kali kami berdebat sesama teman satu angkatan tentang masalah pemerintahan seperti bbm yang baru baru ini akan naik. Skala 7'),
(39, 55, 'Ingin masuk HMIF', 'Demam, Cacar Air, Flu', 'Flu', 'O', '', '081555651929', '8', '4', '7', '7', '7'),
(40, 56, 'ingin menjadi bagian dari informatika', 'darah rendah', 'darah rendah , penyakit tulang belakang', 'A', '', '0816871646', '7', '7', '7', '7', '7'),
(41, 57, 'Motivasi utama adalah ingin masuk HMIF karena saya memilih non-unit dan ingin fokus di himpunan', 'Maag', '-', '0', '', '08122048287', '7, karena secara umum masih agak terkubu-kubu belum semua membaur', '8, karena saya mengikuti keanggotaan kabinet STEI bagian olahraga', '7, inisiatif sudah cukup baik karena terkadang masih menunggu orang lain atau tidak peduli', '8, dilihat dari diklat OSKM untuk calon panitia STEI cukup banyak walaupun tidak diwajibkan', '7, dilihat dari PLO sewaktu diskusi realita bangsa tidak semua termasuk saya pengetahuan tentang bangsanya sendiri baik'),
(42, 58, 'Ingin mengenal teman-teman seangkatan lebih baik lagi dan mengenal kakak-kakak kelas di HMIF.', 'Maag', 'Tidak ada', 'A', '0811226216', '0811226216', '8, karena dalam mengerjakan tugas-tugas yang diberikan ketika interaksi angkatan saya selalu mengadakan kumpul angkatan. Selain itu, angkatan saya juga melakukan kumpul angkatan untuk membuat sebuah keputusan angkatan.', '8. Dalam interaksi-interaksi sebelumnya cukup banyak yang melakukan interupsi untuk bertanya mengenai tugas dan sebagainya, dan teman saya yang melakukan interupsi itu beda-beda, bukan orang yang sama. Selain itu, dalam kumpul angkatan juga banyak dari angkatan kami yang memberikan pendapat, bukan hanya menjadi penonton, sehingga menurut saya angkatan saya sudah cukup aktif.', '8. Menurut saya, angkatan saya memiliki inisiatif yang cukup baik karena dalam mengerjakan tugas-tugas angkatan ataupun sesuatu yang berhubungan dengan angkatan, angkatan saya berinisiatif memilih seorang PJ agar tugas itu lebih terkoordinasi dengan baik.', '5, karena dari data peserta SPARTA, yang mengikuti diklat terpusat hanya sekitar setengah dari keseluruhan peserta SPARTA. Jadi menurut saya angkatan saya lumayan aktif terhadap kemahasiswaan terpusat.', '8. Misalnya ketika ada isu kenaikan BBM kemarin, angkatan saya melakukan diskusi mengenai isu tersebut dan juga isu-isu lainnya yang ada di masyarakat.'),
(43, 60, 'Ingin masuk himpunan', '-', 'Gangguan pencernaan', 'O', '08164866135 (nenek)', '081905054231', '8.5', '7', '8', '7', '8'),
(44, 61, 'Pingin masuk himpunan dan kenal lebih banyak teman', 'Asma, Maag, Flu tulang', 'Asma, Maag', 'B', '085761943263', '062223792', 'Angkatan saya sudah makin kompak kian hari (8)', 'Saya rajin ikut dalam kegiatan angkatan tetapi tidak pernah menjadi pj atau pun panitia dari sebuah acara angkatan (6)', 'Baik dan luar biasa (8)', 'Kurang peduli (5)', 'Tidak pedulu (4)'),
(45, 62, 'Karena ingin bersosialisasi secara lebih luas, ingin menambah pengalaman berorganisasi, dan ngisi waktu libur.', 'Diare, demam berdarah, flu dan demam, sakit kepala.', 'Tidak ada.', 'O', '', '081220229228/02291652761', 'Kebersamaan angkatan saya mungkin berkisar di angka 7, karena mayoritas angkatan masih kompak menghadiri acara angkatan. Namun, tidak semuanya mendukung keberlangsungan acara kebersamaan angkatan.', 'Keaktifan angkatan saya ya saya nilai 5 saja, karena anggota yang benar-benar aktif itu saya rasa sedikit sekali. Biarpun sedikit, dampaknya ke angkatan tetap besar.', 'Inisiatif angkatan saya berkisar di angka 6, karena sebagian besar anggota masih menunggu informasi dari pusat.', 'Kesadaran angkatan saya terhadap kemahasiswaan terpusat berkisar di nilai 6, karena menurut saya angkatan saya hampir semuanya mengetahui kemahasiswaan terpusat, namun tidak semuanya ingin terlibat.', 'Kesadaran angkatan saya terhadap masalah bangsa dan masyarakat nilainya kurang lebih 4, alasannya boro-boro mikirin masalah bangsa, masalah sendiri saja kadang-kadang dilupakan.'),
(46, 63, 'Ingin menjadi anggota HMIF ; Ingin lebih mengakrabkan diri dengan teman seangkatan', 'Radang saluran pencernaan', '-', 'O', '085325625149', '088806759634; 085229167195', 'Lumayan; kebersamaan semakin meningkat sejak interaksi dimulai. (8)', 'Masih belum merata pada masing-masing anggota. (7)', 'Inisiatif angkatan bagus, tapi tidak merata pada masing-masing anggota. (7)', 'Angkatan kami sadar bahwa kami adalah bagian dari 1 KM-ITB. (8)', 'Cukup peduli dan mau ikut berusaha menanggulangi. (7) '),
(47, 64, 'Ingin menjadi anggota HMIF  untuk mendapat lebih banyak teman dan mengasah kemampuan bekerja-sama dalam tim', 'Tidak ada', 'Tidak ada', 'O', '085697716833', '08116160225', 'Angkatan yang cukup solid meskipun kadang-kadang masih ada perselisihan karena perbedaan pendapat, nilai untuk angkatan saya 8.', 'Lumayan aktif,terlihat dari banyaknya kegiatan yang dilakukan bersama-sama satu angkatan,seperti olahraga bareng yang rutin. Nilainya 7', 'Angkatan saya tidak terlalu inisiatif,saya melihat ada banyak kegiatan yang lebih dulu dimulai oleh fakultas lain barulah angkatan kami ikut melaksanakannya. Nilainya 5.', 'Angkatan saya cukup sadar trhdp mahasiswa terpusat karena banyak yang berkecimpung di KM. Nilainya 7.', 'Angkatan saya sangat sadar terhadap masalah bangsa ini. Terbukti dari beberapa diskusi kami di grup tentang masalah ini dan beberapa teman saya yang ikut berdemonstrasi menolak kenaikan harga BBM yang diadakan kemarin. Nilainya 8.'),
(48, 65, 'Untuk lebih lebih mengenal teman seangkatan dan masuk himpunan.', 'batuk, influenza, pertusis, varicella simplex, miopi, diare', 'miopi', 'A', '081361121928', '081361121928', 'Menurut saya, angkatan saya sudah cukup kompak namun banyak yang belum saling mengenal oleh karena banyaknya teman satu angkatan. Dan belum mengenal semuanya berada dalam batas wajar. Nilai: 8', 'Saya melihat angkatan saya lumayan aktif. Nilai: 7', 'Saya melihat angkatan saya lumayan inisiatif. Nilai: 7', 'Untuk kesadaran angkatan terhadap kemahasiswaan terpusat saya tidak dapat menentukan. Nilai: 6', 'Angkatan STEI ''12 pernah melakukan pengabdian masyarakat pada akhir semester 2. Ini meunjukkan angkatan ini cukup sadar dan peduli dengan masyarakat. Nilai: 8'),
(49, 67, 'Ingin masuk himpunan dan supaya dekat dengan teman seangkatan', 'TBC, Asma.', 'Maag', 'A', '087762585504', '08123998891', '6', '6', '8', '7', '6'),
(50, 68, 'Ingin bergabung dengan HMIF\r\nMenambah kenalan', 'sinusitis', 'tidak ada', 'O', '087822603063', '08883030416', 'Kebersamaan angkatan cukup baik karena cukup mewadah meskipun dengan halangan banyaknya anggota. Skala nilai saya beri 7', 'Angkatan cukup aktif, karena beberapa kali mengadakan acara angkatan  misalnya futsal bareng, makrab, dll.. Skala nilai saya beri 8', 'Angkatan cukup kreatif dan berinisiatif. Banyak kegiatan yang berjalan lancar dan kreatif karena inisiatif2 angkatan kami. Skala nilai saya beri 8', 'Saya merasakan adanya kesadaran, namun belum terlalu terwujud dalam tindakan. Skala nilai saya beri 7', 'Saya merasakan adanya kesadaran, namun belum terlalu terwujud dalam tindakan. Skala nilai saya beri 6'),
(51, 69, 'ingin masuk himpunan', 'Maag', 'tidak ada', 'O', '082128469899', '082128469899', '7\r\ndalam angkatan kami masih belum menyatunya semua kalangan, namun dalam angkatan ca-HMIF kondisi ini sudah mulai mencair', '8\r\nangkatan kami sudah cukup aktif, hal itu dapat dibuktikan dari adanya makrab, baksos, dll', '7\r\nkeinisiatifan angkatan kami cukup baik', '8\r\nbanyak dari kami yang tahu dan sadar terhadap kemahasiswaan terpusat', '8\r\nangkatan kami cukup mengetahui dan menyadari permasalahan bangsa. hal yang bisa angkatan kami lakukan salah satunya melaksanakan baksos'),
(52, 70, 'Ingin ikut berorganisasi, dapat berkumpul dengan teman - teman dengan keprofesian yang hampir sama', 'Typhes', 'tidak ada', 'A', '', '08156535775', 'Kebersamaan angkatan kami baik, terlihat saat mengerjakan tugas bersama, 8', 'Cukup aktif, meskipun baru beberapa anak,terlihat saat interaksi, 7', 'Baik, angkatan kami telah berinisiatif dalam beberapa hal seperti inisiatif untuk kumpul angkatan, 8', 'Cukup, masih ada beberapa teman yang cukup apatis terhadap kemahasiswaan terpusat, terlihat dari jumlah teman-teman yang ikut OSKM, 6', 'Cukup baik, sebagian besar memiliki kesadaran, terlihat dari jumlah partisipan pengmas STEI 2012, 7'),
(53, 71, '- Sparta = Gerbang menuju HMIF -> untuk belajar berorganisasi\r\n- Memperluas jaringan -> kenal & faham orang lain\r\n- Seru-seruan', 'Maag kronis', '-', 'O', '', '085697748454', 'Kurang, 5', 'Aktif, 8', 'Lumayan, 7', 'Kurang, 5', 'Belum terlihat, 5'),
(54, 72, 'ingin mengenal teman seangkatan lebih dalam lagi.', '-', 'sariawan, pilek', 'A', '', '081320636641', 'kebersamaan angkatan saya baik. buktinya dalam membuat tugas, sangat jarang saya melihat ada yang kesal, hal ini membuktikan kami semua senang saat kebersamaan ini terjadi. skalanya 9', 'angkatan saya cukup aktif, dapat dilihat saat kumpul angkatan yang cukup banyak teman yang saling memberikan ide. skalanya 7', 'inisiatif angkatan saya masih harus ditingkatkan. menurut saya dalam skala 1-10 nilainya adalah 5', 'kesadaran angkatan saya dalam kemahasiswaan terpusat menurut saya sudah cukup baik. walaupun belum seluruhnya telah sadar, tapi minimal lebih dari 60% sudah sadar dengan hal ini. skalanya 7', 'kesadaran angkatan saya terhadap masalah bangsa dan masyarakat masih butuh ditingkatkan, belum ada contoh kongkret yang pernah saya lihat dalam masalah ini. skalanya masih 3'),
(55, 73, 'Ingin masuk ke dalam himpunan.\r\nBelajar berorganisasi.\r\nMengenal angkatan.', '-', '-', 'o', '', '085721124269', 'Saya orangnya tidak begitu aktif, walaupun begitu banyak dari teman angkatan yang inisiatif untuk berteman.\r\nUntuk diri sendiri : Nilai 7', 'Untuk keaktifan angkatan, biasanya tergantung issue-nya. Karena banyak juga teman - teman yang tidak begitu peduli jika tidak begitu mendesak.\r\nNilai 6', 'Untuk inisiatif, biasanya (yang saya tahu) ketika ada suatu hal kami diminta untuk berkumpul untuk mendiskusikannya.\r\nNilai 7', 'Saya tidak begitu memperhatikan, tetapi dengan ikut hearing KM-ITB ''12 yang lalu dan info-info dari grup FB, cukup banyak juga yang peduli dengan kemahasiswaan pusat. Mereka juga ikut kepanitiaan yang diadakan oleh Kabinet KM.\r\nNilai 8', 'Menurutku lumayan, dengan diadakannya Pengmas STEI yang lalu adalah awal bagi kami untuk mencoba lebih peduli terhadap bangsa ini.\r\nNilai 8'),
(56, 75, 'Ingin masuk himpunan, ingin mengenal lebih dekat teman-teman dan kakak-kakak senior', 'TBC, DBD, Typhus', '-', 'O', '085795980141', '0816806975', 'Angkatan saya sudah memiliki rasa kebersamaan yang cukup baik, hal ini dapat dibuktikan dengan rasa nyaman ketika berkumpul bersama. Nilai untuk angkatan saya adalah 8.', 'Angkatan saya cukup aktif, tetapi mungkin masih ada beberapa orang yang hatinya belum terpanggil untuk turut serta aktif. Nilai untuk angkatan saya adalah 7,5.', 'Angkatan saya memiliki rasa inisiatif yang tinggi. Tidak hanya inisiatif, tetapi inisiatif yang bertanggung jawab. Nilai untuk angkatan saya adalah 8,5.', 'Angkatan saya cukup berperan dalam kemahasiswaan terpusat, hal ini dibuktikan dengan banyaknya teman saya yang mengikuti kegiatan OSKM. Nilai untuk angkatan saya adalah 7,5.', 'Angkatan saya sadar terhadap masalah bangsa dan masyarakat, hal ini dapat dibuktikan dengan kegiatan Pengmas STEI yang cukup banyak diikuti oleh peserta-peserta SPARTA. Nila untuk angkatan saya adalah 8.'),
(57, 76, 'ingin bergabung dalam himpunan', '-', 'asma dan anemia', 'A', '085213754798', '08128692963', 'awalnya memang tidak dekat dan bahkan ada yang belum kenal. tapi sekarang sudah lumayan. Nilainya 8', 'Keaktifannya, beberapa ada yang sangat aktif tapi juga ada yang pasif. Nilainya 8.5', 'Inisiatif angkatan sangat bagus, cepat bergerak. Nilainya 8.5', 'kesadarannya lumayan walaupun belum semuanya. Nilai 7.5', 'kesadaran angkatan lumayan. ada yang sangat peduli bahkan ada juga yang kurang. Nilai 7'),
(58, 77, 'Mengenal angkatan lebih baik lagi, mengenal himpunan (dan segala yang ada di dalamnya) lebih jauh lagi, dan agar bisa menjadi bagian dari HMIF', 'Molbiri, typhus', 'Sinusitis, alergi dingin', 'A', '085223320798', '081323881437', 'masih ada kubu-kubu, belum menyatu semua\r\nnilai: 6.5', 'keaktifan internal (dalam kegiatan internal angkatan) sudah baik contohnya saat supporter-an, keaktifan di kegiatan luar (contohnya kegiatan kampus yang terpusat) masih kurang\r\nnilai: 7', 'Baik, banyak kegiatan angkatan yang dilakukan contohnya olah raga bersama (futsal), foto angkatan, makrab 2x, pengabdian masyarakat, dll\r\nnilai: 8.5', 'masih kurang, contohnya masih banyak (kalau tidak salah lebih dari 100) yang tidak mengikuti dikalt OSKM\r\nnilai: 6.5', 'Sudah baik, contohnya kegiatan akhir semester 2 kemarin yaitu pengmas di rumah asuh\r\nnilai: 8'),
(59, 78, 'Saya ingin masuk HMIF', 'tipes, demam berdarah, cacar', 'tidak ada', 'O', '085781067178', '08161991002', 'baik, karena dalam kegiatan angkatan yang penting, yang datang cukup banyak. nilai 8', 'baik, dalam memberikan tanggapan saat kumpul angkatan atau menyelesaikan tugas, banyak yang aktif memberi pendapat. nilai 7', 'sedang, dalam kumpul angkatan hanya beberapa yang berinisiatif. nilai 6', 'Sedang, dapat dilihat dari yang ikut diklat OSKM yaitu sekitar 50-60%. nilai 6', 'sedang, dapat dilihat dari peserta yang ikut berpartisipasi aktif dalam pengmas hanya sekitar 50%. nilai 5'),
(60, 79, 'ingin masuk himpunan dan juga mengenal lebih baik lagi satu angkatan', '-', '-', 'A', '081320011751', '08128421967', 'kebersamaan angkatan saya sudah cukup baik, hal ini dibuktikan dengan adanya kumpul-kumpul bareng (7)', 'menurut saya, angkatan saya sudah cukup aktif yang dibuktikan dengan cukup banyaknya pertanyaan-pertanyaan saat interaksi ataupun juga cukup banyak yang datang saat sharing oskm (8)', 'menurut saya, angkatan saya juga sudah cukup inisiatif, hal ini dibuktikan dengan adanya volunteers untuk menjadi PJ tugas-tugas yang telah diberikan dalam waktu yang relatif singkat saat kumpul angkatan setelah interaksi dilaksanakan (8)', 'angkatan saya juga sudah sadar terhadap kemahasiswaan terpusat, hal ini dibuktikan dengan banyaknya angkatan saya yang ikut diklat calon panitia oskm 2013 walaupun tidak diwajibkan untuk ikut. (8)', 'angkatan saya juga sudah cukup sadar terhadap masalah masyarakat. hal ini dibuktikan dengan diadakannya pengmas. (7)'),
(61, 80, 'untuk aktif di himpunan', 'demam berdarah, cacar', 'maag ringan', 'A', '085222090969', '085797249945', 'sudah baik namun beberapa orang kurang rasa kebersamaannya. nilai 9', 'cukup aktif. namun beberapa orang kurang proaktif. nilai 8', 'inisiatif angkatan terpusat pada beberapa orang saja, yang lainnya hanya mengikuti. nilai 7', 'sudah cukup baik. nilai 6', 'jawaban memuaskan jika sedang membicarakan masalah negara. nilai 8'),
(62, 81, 'Ingin masuk himpunan', '-', '-', 'A', 'ibu kos (081910266007)', 'ALnizar', 'menurut saya sudah cukup baik, meskipun belum semua terangkul, nilainya 7', 'angkatan cukup aktif, nilai 8', 'angkatan sudah sangat inisiatif, nilainya 8', 'kesadaran angkatan sudah cukup baik, nilai 6', 'sudah cukup baik, nilai 6'),
(63, 82, 'Karena saya ingin mengenal dan memahami nilai-nilai yang sudah tertanam di hmif sejak dahulu dan ingin bisa lebih akrab dan kompak dengan teman-teman saya', 'tidak ada', 'tidak ada', 'b', '08997812191', '0817618696', 'angkatan saya kompak kalau soal have fun. nilai : 7', 'saya masih kurang aktif di angkatan karena malu-malu. nilai 5', 'inisiatif angkatan saya sangat baik dalam hal yang positif. nilai : 8', 'angkatan saya sudah cukup aktif dalam kegiatan km itb. nilai : 7', 'angkatan saya sangat peduli pada masyarakat sekitar dan kritis dalam membahas permasalahan negara. nilai : 8'),
(64, 83, 'Untuk melatih diri menjadi kader HMIF yang baik untuk berkontribusi bagi almamater dan bangsa', '-', '-', 'o', '08995602430', '081325521742', 'angkatan kami sudah cukup akrab dengan sering diadakannya interaksi sesama serta tugas wawancara yang lebih merekatkan angkatan. \r\n7', 'angakatan kami cukup aktif dalam kegatan kemahasiswaan seperti contohnya OSKM. dalam kegiatan terpusat tersesbut cukup banyak dari angkatan kami yang mengikutinya. Dan keanggotaan unit di angkatan , cukup beragam, menandakan keikutsertaan dalam unit cukup tinggi.\r\n8 ', 'inisiatif angkatan kami dalam memecahkan masalah atau kendala angkatan sudah mulai menuju baik walaupun belum sempurna. Hal ini dilihat dari kesigapan angakatan kami dalam membahas tugas angkatan dan individu, walaupun hasilnya belum sempurna.\r\n7', 'Cukup baik, dapat dilihat dari keikutsertaan dalam kepanitiaan terspusat seperti OSKM, Olimpiade KM, Pemira yang antusias. \r\n8', 'untuk yang satu ini, jujur saya belum begitu mengetahuinya. tapi dari beberapa hal dapat saya sampaikan yaitu keikutsertaan teman-teman angkatan dalam Pengmas STEI dapat merepresentasikan kepedulian kepada masyarakat, selain itu dalam serangkaian kegiatan OSKM dapat dilihat banyak aktifitas yang merepresentasikan kepedulian terhadap bangsa.\r\n6'),
(65, 84, 'Ingin mendapatkan ilmu dan pengalaman', 'Asthma, Bronkhitis', 'Asthma', 'B', '085794158076', '085721474198', '7', '7', '6', '5', '5'),
(66, 85, 'Bisa mengetahui lebih banyak tentang Himpunan dan bisa mencari teman', 'Tidak ada', 'Tidak ada', 'O', '', '06175240522', '9', '8', '8', '9', '8'),
(67, 87, 'Ingin menjadi anggota HMIF', 'DBD', '-', 'O', '087821133423', '081324402405', 'Usaha untuk mensolidkan angkatan namun belum merata.\r\n7', 'Secara personal, sudah banyak yang menyebar dan aktif baik di kepanitiaan maupun di unit\r\n8', 'Sudah banyak yang sadar untuk berinisiatif jika ada sesuatu yang harus dilakukan angkatan.\r\n7', 'Sepertiga dari angkatan mengikuti OSKM, itu bisa jadi bukti kesadaran tersebut.\r\n7', 'Beberapa orang peduli terhadap issue nasional.\r\n6'),
(68, 88, 'Kenal lebih baik dengan 1 angkatan\r\nmasuk Himpunan', 'flu, diare, demam berdarah', 'gangguan pencernaan', 'O', '085264061535', '081366390062', 'Sedang dalam kondisi meningkat menjadi lebih baik\r\nMenurut saya, nilai sekarang adalah 6,5', 'Cukup banyak orang yang ingin berkontribusi dalam angkatan yang menjadikan angkatan ini lebih aktif untuk menjalin hubungan sesama 1 angkatan.\r\nMenurut saya, nilai sekarang adalah 7', 'Banyak orang dari angkatan saya yang memiliki kesadaran untuk memulai sesuatu sebelum yang lainnya. Menurut saya, nilai sekarang adalah 8.', 'Cukup banyak peserta SPARTA yang mengikuti diklat OSKM. Menurut saya, nilai sekarang adalah 7', 'Saya masih belum melihat mengenai kesadaran angkatan terhadap masalah bangsa. Menurut saya, nilai sekarang adalah 5'),
(69, 89, 'Ingin masuk himpunan, lebih dekat dengan teman-teman yang memiliki passion di keprofesian yang serupa', 'Demam berdarah, herpes', '-', 'B', '', '081288870004', '8', '8', '8', '6', '7'),
(70, 90, 'Ingin berperan di HMIF', '-', '-', 'o', '085797301610', '082323555655', 'Lumayan kompak, saat kumpul angkatan, lumayan banyak yang bisa kumpul mengikuti acara, tapi terkadang ada acara yang tidak dihadiri banyak teman kami. 7', 'Sekarang ini, saya masih belum baik dalam manajemen waktu, saat ini saya sedang banyak kegiatan diluar kegiatan angkatan saya, jadi saya belum bisa hadir secara intensif dalam kegiatan angkatan saya. Untuk skala, saya menilai keaktifan saya 6', '8. Lumayan banyak teman saya yang memiliki banyak ide dalam berbagai hal. ', '7, banyak teman saya yang mengikuti kegiatan dari berbagai kegiatan kemahasiswaan terpusat, seperti panitia olim KM, OSKM, ', '6, saat ini sudah kira-kira 50% lebih dari teman saya yang risih dengan keadaan bangsa ini dan mulai merangkai langkah untuk memberi perubahan'),
(71, 93, 'Sebagai syarat masuk himpunan', '-', '-', 'O', '', '087868919833', 'Kurang kompak 6', 'Cukup aktif 7', 'Baik 8', 'Angkatan kami cukup berpartisipasi thdp kemahasiswaan terpusat buktinya kami ikut memilik presiden km itb, 8', 'Angkatan masih kurang peduli , 5'),
(72, 94, 'Karena ingin menjadi bagian dari HMIF dan ingin mengenal lebih jauh kultur himpunan ini dan mengenal captcha 2012 lebih dekat', 'DBD, Typhus', 'Tidak ada', 'O', '082121164405', '081331968557', 'Untuk permulaan, rasa peduli dan kompak untuk satu angkatan sudah terbentuk, namun belum menyeluruh ke semua komponen captcha 2012. Diperlukan suatu sarana untuk menyatukan dengan lebih erat lagi yaitu melalui sparta 2012. Nilai 8 dalam skala 10', 'Cukup bagus untuk bersuara, bertanya, maupun memberikan pendapat secara rasional. Nilai 7 dari skala 10', 'Belum menyeluruh, karena masing-masing dari kami belum terkonsentrasi pada SPARTA ini karena sedang menempuh diklat maupun mengemban amanah di lain tempat. Nilai 6 dari skala 10', 'Didasarkan parameter terhadap keikutsertaan di OSKM 2013, yang mencapai sepertiga dari angkatan, saya rasa kepedulian terhadap kemahasiswaan terpusat dapat dikategorikan baik. Nilai 7 dari 10', 'Kesadaran terhadap masalah bangsa dan masyarakat masih berupa pemikiran-pemikiran ataupun wacana, diperlukan suatu sarana untuk merealisasikannya. Nilai 6 dari 10'),
(73, 95, 'Ingin bergabung di HMIF dan berkontribusi di dalamnya.', 'Maag', 'Maag', 'A', '087868919833', '081396036557', 'Kurang kompak dan kenal satu sama lain\r\n7', 'Bisa dikatakan hanya sebagai anggota pasif\r\n5', 'Angkatan kami penuh inisiatif dan kreatif. Tetapi tetap saja pengimplikasiannya masih kurang.', 'Angkatan kami peka dan berkontibusi di kemahasiswaan terpusat, misalnya banyak yang aktif di KM, OSKM, PEMIRA, dsb.\r\n8', 'Angkatan kami cukup sadar akan masalah kebangsaan, hanya bukti nyatanya yang masih kurang.\r\n5'),
(74, 96, 'ingin mengenal satu angkatan dan aktif dalam himpunan', 'demam', 'tidak ada', 'o', '085759301733', '085294727212', '7', '6', '6', '8', '7'),
(75, 97, 'Untuk mengenal angkatan lebih jauh, Sebagai prasyarat memasuki HMIF, Untuk menerima materi materi yang akan diberikan nanti', 'Asma\r\nAlergi Debu\r\nDBD', '-', 'B+', '', '087771665691', '7', '6', '6', '5', '4'),
(76, 99, 'Ingin menempa diri menjadi pribadi yang siap menghadapi segala tantangan di Informatika', 'Demam Berdarah', '-', 'B', '', '081358112023', 'Untuk saat ini, kebersamaan angkatan saya masih belum terlalu kental. Terbukti dari kehadiran ketika kumpul angkatan jarang full team atau mendekati full team. 6', 'Belum semua anggota aktif dalam pengambilan keputusan. beberapa wajah masih sering mendominasi.6', 'Kesadaran untuk mencari info mandiri masih kurang. Lebih sering menunggu.6', 'Cukup baik. Banyak yang aktif di berbagai kegiatan seperti olim, IEC, dan OSKM.', 'Baru segelintir orang yang jelas nampak peduli terhadap masalah bangsa dan masyarakat. Nampak diartikan sebagai ikut dalam organisasi kajian ataupun aktif di Kabinet KM ITB. 5'),
(77, 100, 'Supaya bisa berkomunikasi, berelasi, dan kenal dekat dengan teman', 'Demam Berdarah, maag, diare', 'tidak ada', 'O', '08179220357', '081329560067', 'Menurut saya cukup baik.8', 'Lumayan aktif, karena waktu diskusi banyak yang berebut mengeluarkan pendapat. 8 ', 'Menurut saya cukup. Karena keadaran akan kebersamaan yang tinggi.9', 'Baik. Karena kami selalu merasa dekat. 8', 'Lumayan. Sebagian sudah melakukan kontribusi untuk bangsa meski tidak secara langsung. 7'),
(78, 101, 'Nambah kenalan', 'Demam berdarah', 'Mager', 'O', '08122109028', '08122109028', '6 karena kadang suka tidak memikirkan waktu yang benar benar semua bisa untuk berkumpul', '5 cukup aktif dengan mendukung kebijakan yg diambil angkatan', '4 saya agak kurang berinisiatif di angkatan', '7 cukup banyak STEI yang mendaftar panitia OSKM', '2 mereka masih terlalu memandang bandung sesempit dago, itb dan daerahnya saja, belum bisa menerima bandung yang begitu luas'),
(79, 102, 'Belajar dan berkeluarga untuk Indonesia', 'DBD, ', 'Alergi Pedas', 'B', '085722482833', '081324303172', 'Banyak even kebersamaan tapi belum bisa ngehadirin banyak variasi yang bisa ngewakili tiap-tiap hobi atau kepribadian masing-masing. nilai 7.5', 'program kerja setahun di stei aktif dilaksanakan. cuma untuk per orang banyak yang aktif tapi itu-itu aja, nilai 7', 'inisiatif ngadain acara ada tapi kurang kreatif mengadakan acara tersebut. terkesan formalitas ,nilai 6', 'cukup baik, ketika hearing rame. waktu pemira sebelum referendum sekitar 90% milih. ya waktu referendum juga aktif dateng d forum altera. nilai 8', 'kurang sadar kayanya. contoh dari PKM , dikit yang ikut.  jarang ada kajian realitas bangsa. nilai 5'),
(80, 103, 'Supaya kenal dengan satu angkatan dan kakak senior serta bergabung dalam himpunan.', 'tifus,DBD,demam,flu', 'tidak ada', 'AB', '', 'Harli', '9. cukup kompak. semua saling mengingatkan mengerjakan tugas. semua saling perhatian ibarat satu keluarga.', '9. cukup aktif. ketika ada tugas yang tidak dimengerti, mereka aktif bertanya dan pasti selalu dijawab.', '9. mereka inisiatif untuk mengerjakan segala sesuatu tanpa diperintah sebab uda dewasa.', '7. ada beberapa mahasiswa yang tidak terlalu memperhatikan kegiatan kemahasiswaan terpusat.', '6. tidak cukup baik, sebab ada yang cenderung belajar terus dan kurang peka terhadap sekitar.'),
(81, 104, 'Mengenal angkatan dan ingin masuk himpunan\r\n', 'Hepatitis dan alergi dingin', 'Alergi dingin', 'A', '085223208223', '081513137862', 'Angkatan saya sudah semakin solid dengan adanya tugas-tugas dari interaksi, namun masih ada yang tidak pernah hadir di interaksi (Nilai : 7.5)', 'Mayoritas elemen angkatan sudah mulai menunjukkan keaktifannya untuk angkatan walau beberapa belum. (Nilai : 7)', 'Masih hanya segelintir orang saja yang mau menunjukkan inisiatifnya untuk angkatan (Nilai : 6)', 'Menurut saya, poin ini agak sulit diukur.  Namun, melalui PLO, saya yakin angkatan saya sudah lebih terbuka pemikirannya mengenai kemahasiswaan terpusat. (Nilai : 7)', 'Saya yakin, melalui PLO serta Diklat OSKM, sudah banyak anggota angkatan yang peduli terhadap masalah bangsa dan masyarakat. (Nilai : 7)'),
(82, 105, 'Ingin mencari pengalaman dalam berorganisasi, ingin mengenal satu sama lain', 'DBD, Tipes, Demam biasa', 'Demam biasa', 'B', '', '081265401301', 'Menurut saya angkatan sudah cukup kompak dengan menghadapi sesuatu bersama-sama, 8/10', 'Menurut saya angkatan sudah sangat aktif dalam bidang akademik maupun non-akademik, 9/10', 'Beberapa orang dalam angkatan sudah mengambil inisiatif, dan sisanya masih menunggu keajaiban yang akan terjadi, 6/10', 'Sepertinya masih belum banyak dari angkatan yang cukup mengerti masalah kemahasiswaan terpusat, 4/10', 'Angkatan kami sudah cukup update tentang masalah bangsa dan masyarakat saat ini, 8/10'),
(83, 108, 'Mengasah softskill, hardskill, dan mencari teman utk berinovasi.', 'Gampang laper', 'gada', 'O', '087899737778', '0816284129', 'Terlalu dini untuk menilai, tp karena terpaksa yaudah 6 I think pretty good for a starter.', '6', '8', '6', '6'),
(84, 109, 'Ingin masuk himpunan.', '-', 'Hipertensi', '0', '', 'Marina', '7', '8', '7', '6', '6'),
(85, 110, 'untuk menambah wawasan, untuk menambah kedisiplinan, belajar manajemen waktu dengan baik', 'Demam, batuk', '-', 'B', '085759301733', '083820260709', 'kebersamaan itu sudah ada, tapi yang saya rasa ini baru secara fisik belum secara hati . 7', 'menurut saya angkatan saya sudah aktif, tapi terkadang keaktifan itu 7', 'inisiatif sendiri (untuk mendukung SPARTA) untuk saat ini sebenarnya masih kurang karena untuk saat ini yang saya rasakan kami masih dalam tahap adaptasi dengan beberapa kewajiban yang harus kami lakukan juga sebagai komitmen kami dengan suatu kegiatan lain yang kami jalani (diklat OSKM, unit, dll) , sehingga saya memberi nilai  6', 'menurut saya ini dapat kita tinjau dari berapa orang yang mendaftar menjadi panitia OSKM , sejujurnya saya belum tau pasti berapa orang calon anggota HMIF yang mendaftar kepanitiaan OSKM, tetapi saya kira cukup banyak yang mendaftar sehingga saya memberikan 7', 'untuk secara angkatan sendiri kita belum sampai membicarakan masalah bangsa dan masyarakat dalam  kumpul angkatan kami, tapi yang saya yakin masing - masing orang dari angkatan kami memiliki kesadaran akan masalah dalam bangsa dan masyarakat ini. sehingga saya beri nilai 7'),
(86, 111, 'mau masuk himpunan, mau lebih mengenal angkatan', 'Hipotensi', '-', 'A', '(022)60908743', '081267011294', 'bagus, terlihat dari kekompakannya dan banyak temen yang berkata senang di angkatan.\r\nSkala 7', 'belum bisa diamati, tapi menurut saya sejauh ini mungkin keaktifan angkatan ini skala 5', 'menurutku biasa aja, karena inisiatif itu sendiri masih dari perseorangan.\r\nSkala 5', 'belum pernah membahas, tapi mungkin jika diskalakan skala 6', 'belum pernah membahas, tapi mungkin jika diskalakan skala 6'),
(87, 112, 'Ingin mengenal HMIF lebih dalam.', 'Cacar air, Campak.', 'Tidak Ada', 'B', '', '085292871348', 'Awalnya angkatan kami sulit untuk bareng. Tapi lama kelamaan kami bisa kompak, seru-seruan bareng. Ya bisa saya beri nilai 8 lah untuk angkatan kami.', 'Teman-teman angkatan saya rata-rata adalah orang-orang aktif. Nilai nya 8 untuk ini.', 'untuk ini saya beri nilai 8. Mungkin karena notabene anak STEI itu cerdas dan kritis. Jadi angkatan kami inisiatifnya cukup bagus', 'Untuk ini nilainya 8. Anak-anak angkatan saya cukup banyak yang aktif di kemahasiswaan terpusat.', 'Ini juga 8. Karena anak-anak STEI itu cerdas, tanggap, dan kritis. Jadi bisa dibilang mereka itu cukup sadar terhadap masalah bangsa dan masyarakat.'),
(88, 113, 'Saya ingin mengikuti Sparta sebagai langkah awal saya mengikuti dan berkontribusi di HMIF nantinya.', 'Demam Berdarah', 'Tidak ada', 'B', '', '081806402447', '8. Saya merasa angkatan saya cukup solid melalui beberapa event angkatan seperti makrab, pengmas, supporteran dan lainnya. ', '6. Angkatan saya cukup aktif dalam menyelesaikan masalah, dan bersinergis satu dengan yang lainnya. ', '7. Angkatan saya cukup berinisiatif dalam berbagai hal.', '7. Saya merasa angkatan saya cukup menyadari pentingnya kemahasiswaan terpusat yang dapat dilihat dari jumlah yang cukup banyak dari kami yang mengikuti diklat OSKM. ', '6. Saya rasa, kesadaran tersebut sudah mulai dipupuk melalui event seperti pengabdian masyarakat serta melalui kegiatan-kegiatan diklat OSKM. '),
(89, 114, 'Ingin menjadi anggota keluarga HMIF', '-', '-', 'O', '085220187361', '081575951669', 'Karena sudah 1 tahun kenal bareng jadi ya bagus pasti kebersamaannya. Kami cukup solid. Nilainya: 8.5', 'Dari banyaknya anak yang datang saat kumpul angkatan dan interkasi, saya beri nilai: 7', 'Banyak anak-anak yang berinisiatif untuk berkontribusi di angkatan kami, tanpa ada tekanan atau suruhan, melainkan karena kemauan sendiri. Saya beri nilai 8', 'Dilihat dari banyaknya anak yg bekontribusi dalam acara kemahasiswaan tepusat, seperti pemira, oskm, ddat, saya beri nilai 7.5', 'Angkatan kami saat TPB mengadakan pengmas, dan semua orang ikut berkontribusi dalam acara tersebut. Jadi saya beri nilai 9'),
(90, 115, 'Karena saya ingin sekali masuk himpunan dan saya tidak ingin sendirian. Saya ingin mengenal lebih dekat teman2 saya selama kaderisasi. Sekalian saya juga mempelajari budaya - budaya yang ada di HMIF.', 'sinusitis, tipus,cacar, influenza, demam,', 'demam, sinusitis.', 'B', '083820344671', '0811490155', '5. Karena saya baru mengenal sekitar setengahnya dan tak terlalu dekat dengan sisanya.', '8.Aktif memang. tapi sayang kadang beberapa tidak berpartisipasi menyampaikan pendapatnya .', '6.Agak lama dalam memulai sesuatu tapi akhirnya tetap terlaksana.', '5.Saya lihat masih sedikit kawan - kawan saya yang peduli dengan seputar berita KM - ITB. tapi kadang mereka aktif di unit mereka masing2 dan kadang organisasi luar macam unit di salman.', '3. Kalau ini saya merasa mereka kurang sadar sekali karena menurut saya teman2 saya banyak yang ambis dan, maaf lebih mementingkan diri sendiri. tapi saya harap ending dari SPARTA ini , kami bisa terbuka matanya.'),
(91, 116, 'Agar dapat masuk himpunan dan kelak melakukan berbagai kegiatan di sana.', 'Darah rendah.', '-', 'A', '', '081224742795', 'Telah banyak terlaksananya acara angkatan yang dapat dibilang sukses.\r\n8.', 'Cukup aktif, ikut-ikut kegiatan dari kampus. Seperti pemilu, panitia pemilu, panitia OSKM.\r\n8.', 'Salah satunya ditunjukkan dengan Pengmas STEI. Dapat dikatakan cukup berinisiatif.\r\n7.', 'Kesadaran ini belum terlalu menyeluruh.\r\n6.', 'Cukup sadar. Dari kegiatan Pengmas dan beberapa pandangan yang saya dengar saat diskusi pada kegiatan angkatan.\r\n7.'),
(92, 117, '-supaya bisa masuk himpunan\r\n-dihimpunan biar bisa berbagi dan berdiskusi dengan kelompok yg mempunyai prodi yang sama atau berkaitan\r\n-untuk membina diri dari kegiatan sparta ini\r\n-untuk lebih mengenal teman seangkatan\r\n-untuk mempererat angkatan', 'DBD, cacar', 'darah rendah', 'B', '08161861627', '08158836450', 'baik, tapi masih perlu ditingkatkan\r\nmasih ada beberapa belum pernah ngobrol\r\n7', 'cukup aktif. tapi yang aktif kayaknya yang itu-itu saja\r\n6', 'inisiatif, inisiatif muncul dari yang biasanya aktif dan hal itu menular ke yang kurang aktif.\r\n7', 'sejujurnya saya kurang mengerti konteks ''kemahasiswaan terpusat''\r\nsaya asumsikan kalau kemahasiswaan terpusat itu adalah acara satu ITB seperti pemira dan oskm.\r\nmenurut saya cukup, banyak yang berpartisipasi banyak juga yang tidak.\r\n5', 'baik, terkadang kita berdiskusi di kelompok kecil tentang masalah masalah yang ada di Indonesia. \r\n6\r\n'),
(93, 118, 'Saya mengikuti SPARTA karena saya ingin berhimpun dan mengembangkan diri.', 'Maag, Campak', 'Maag', 'B', '081321129293', '081573443744', '8. Menurut saya kondisi angkatan kami baik dan harmonis. Tapi memang kalau dibandingkan dengan beberapa fakultas lain di ITB maupun di luar akan terlihat tidak se-solid mereka.', '9. Angkatan kami aktif dalam bergerak. Pada masa TPB kami mengadakan berbagai macam kegiatan.', '9. Saya melihat bahwa kami selalu berinisiatif untuk membantu satu sama lain apa lagi dalam bidang akademik.', '6. Menurut saya hanya separuh dari angkatan kami yang peka terhadap kemahasiswaan terpusat.', '8. Angkatan kami cukup sadar dan ingin membina Indonesia. '),
(94, 120, 'Menambah teman', 'Hepatitis', 'Maag', 'O', '085720018565', '6627071', '7', '7', '8', '8', '8'),
(95, 121, 'ingin masuk himpunan', 'radang tenggorokan, alergi dingin', '-', 'B', '02293417828', '081511488314', '7. Menurut saya kebersamaan di angkatan cukup baik karena saya melihat dari proses pengerjaan tugas yang bersama-sama. Hanya saja masih ada beberapa anggota yang belum ikut mengerjakan tugas bersama-sama tanpa alasan yang jelas', '6. Menurut saya, angkatan saya lumayan aktif, hanya saja masih ada anggota angkatan yang kurang aktif ', '8. Saya melihat di angkatan saya bahwa kami memiliki kemauan yang cukup tinggi untuk memberikan yang terbaik', '8. Menurut saya, kesadaran angkatan terhadap kemahasiswaan terpusat cukup bagus, karena banyak di angkatan saya yang mengikuti kegiatan-kegiatan yang diadakan kemahasiswaan terpusat', '8. Kesadaran di angkatan saya terhadap masalah dan masyarakat bangsa menurut saya cukup baik, tapi masih ada sebagian kecil yang terlihat kurang menyadari masalah bangsa dan masyarakat'),
(96, 122, 'Ingin tergabung dalam HMIF', 'Bronkhitis', 'Tidak ada ', 'A', '', 'Agus Aminoto', 'Untuk di angkatan STEI saya cukup mudah bergaul (8) , namun di angkatan captcha12 ini masih perlu adaptasi lagi, masih banyak yang belum kenal baik (5)', 'Angkatan kami tergolong cukup aktif, tiap kumpul angkatan lumayan banyak yang datang meskipun ada diklat oskm. Jarkom juga lancar. (8)', 'Bisa dibilang angkatan kami termasuk yang cukup insiatif, tugas angkatan sesegera mungkin langsung dilaksanakan atau setidaknya direncanakan hari itu juga, (7)', 'Cukup banyak yang ikut oskm, jadi dapat digolongkan cukup sadar (7)', 'Untuk masalah ini mungkin tidak bisa dinilai dari saya individu, namun tentang berita" yang hangat kami cukup tau meskipun jarang membahas sebagai satu angkatan (5)');
INSERT INTO `tambahan` (`id`, `id_peserta`, `motivasi`, `penyakit_pernah`, `penyakit_sedang`, `golongan_darah`, `nomor_kerabat`, `nomor_ortu`, `kebersamaan`, `keaktifan`, `inisiatif`, `kesadaran_mahasiswa`, `kesadaran_bangsa`) VALUES
(97, 123, 'Ingin mengenal HMIF dan menjadi salah satu anggota himpunan tersebut', '-', '-', 'B', '085695823820', '08161348097', '7, sedang dalam proses lebih kompak melalui tugas2 yang diberikan', '8, selalu aktif memberi ide yang membangun  dan solutif dalam setiap pengerjaan tugas atau penentuan keputusan lainnya', '8, kritis dan proaktif', '5, kurang peduli', '6, belum begitu sadar'),
(98, 125, 'Saya ingin terus belajar dalam keorganisasian, karena menurut saya soft skill sangatlah penting. Selain itu saya juga ingin berperan serta dalam masyarakat sebagai mahasiswa yang aktif dalam melakukan pergerakan untuk lingkungan.', 'Tidak ada', 'Tidak ada', 'B', '081220889962', '08111104034', 'Menurut saya, saat ini angkatan saya memang belum memiliki kebersamaan secara optimal, namun kami semua sedang berproses untuk menuju kebersamaan yang optimal tersebut. Dari skala 1-10, kebersamaan angkatan saya masih pada nilai 6.', 'Saya rasa angkatan saya sudah cukup aktif dalam melakukan kegiatan. Hal ini terlihat dari banyaknya orang yang berpartisipasi dalam kegiatan angkatan. Untuk itu keaktifan angkatan saya bernilai 7.', 'Inisiatif angkatan saya sudah baik, angkatan saya selalu melakukan inisiatif saat terdapat suatu hal yang kiranya memang perlu dilaksanakan. Dari skala 1-10, inisiatif angkatan saya, saya beri nilai 8.', 'Menurut saya kesadaran angkatan saya terhadap kemahasiswaan terpusat belum maksimal. Banyak yang tidak mengikuti kegiatan kemahasiswaan terpusat, maupun mengetahui info tentang kemahasiswaan terpusat. Nilai 5.', 'Sebenarnya, kesadaran angkatan saya terhadap masalah bangsa dan masyarakat tidak juga dibilang minim, hanya saja sampai saat ini, saya rasa angkatan saya memang hanya melakukan satu kegiatan terhadap masyarakat. Namun bukan berarti angkatan saya tidak peduli terhadap masyarakat. Nilai 6.'),
(99, 126, 'Karena ingin masuk HMIF dan ingin belajar banyak masalah keorganisasian.', 'Trombositosis, Maag, Sariawan, Panas Dalam, Demam, Flu, Batuk.', 'Sariawan', 'A', '081282345008', '08127817903', 'Kebersamaannya cukup baik namun belum mencakup semua anggota. Meski begitu, masih perlu ditingkatkan.\r\n7', 'Keaktifannya, Alhamdulillah sudah cukup baik. Bisa dilihat saat kumpul angkatan. Meski begitu saya tidak menjamin karena masih ada beberapa yang belum hadir karena ada berbagai urusan.\r\n7', 'Inisiatifnya merupakan inisiatif parsial. Hanya dimiliki sebagian, dan sisanya hanya menjadi pengikut yang setia.\r\n4', 'Melihat dari jumlah yang aktif dalam OSKM, saya rasa cukup banyak yang mengikuti. Ini bisa menjadi parameter tersendiri mengenai kepahaman tentang kemahasiswaan. Meski saya sadari ada beberapa yang apatis. 6', 'Kesadaran mungkin kurang. Saya tidak tahu kenapa. Bisa jadi memang kurang peduli, atau memang kurang update mengenai isu-isu yang sedang berkembang di masyarakat.'),
(100, 127, '- Ingin masuk ke HMIF\r\n- Ingin mengenal lebih baik teman dalam satu angkatan\r\n- Ingin mempererat hubungan dengan teman\r\n', 'Demam berdarah', 'Alergi debu, bulu, dan beberapa jenis obat', 'A', '085692980485 (Alvin Sutandar)', '08121034608', 'Menurut saya, angkatan 2012 merupakan angkatan yang cukup kompak namun belum secara maksimal. Hal ini dapat dilihat dari beberapa tugas angkatan yang dapat diselesaikan dengan cukup baik namun tidak secara sempurna, misalnya masih ada beberapa anggota yang belum mengganti profile picture. Untuk kebersamaan saya beri nilai 7', 'Menurut saya, angkatan 2012 sudah cukup aktif namun belum sepenuhnya aktif. Keaktifan yang cukup baik dapat dilihat dari kegiatan forum dimana banyak yang aktif bertanya ataupun mengemukakan pendapat. Walaupun demikian, masih banyak yang tidak menghadiri kegiatan-kegiatan interaksi. Untuk keaktifan saya beri nilai 7', 'Menurut saya, angkatan 2012 merupakan angkatan yang sangat inisiatif. Hal ini dapat dilihat dari berbagai usaha untuk membuat kegiatan kumpul angkatan diluar jadwal interaksi untuk menyelesaikan tugas-tugas angkatan dan bersosialisasi. Untuk inisiatif saya beri nilai 8.', 'Menurut saya sebagian besar sudah tahu namun ada beberapa mahasiswa yang tidak tahu akan hal ini. Sekalipun sudah banyak yang tahu, belum semua mau terlibat di dalamnya. Saya beri nilai 7', 'Menurut saya, angkatan 2012 memiliki kesadaran yang cukup baik terhadap berbagai masalah. Hal ini dapat dilihat dari aksi nyata berupa pengmas. '),
(101, 128, 'Ingin masuk HMIF', 'Batuk, Pilek, Demam, dan penyakit ringan lainnya', 'Sehat saat pengisian', 'B', '085762378535', '08127817903', '80% sering datang. 75% dari sisanya kadang datang, kadang tidak. Sisanya sangat jarang terlihat. Saat sudah datang, aura friendly cukup terasa. \r\nSkala 8.\r\n\r\n*Persentase diatas hanya nilai subjektifitas', '15% sangat aktif dalam mengerjakan tugas, tetapi sadar kalau dia aktif dan aktif mempengaruhi yang lain untuk aktif. 65% dapat dipengaruhi untuk menjadi aktif. Sisanya agak sulit menjadi aktif\r\nSkala 7\r\n\r\n*Persentase diatas hanya nilai subjektifitas', '10% inisiatifnya tinggi. 75% bisa/sudah terpengaruhi untuk berinisiatif.\r\nSkala 7.\r\n\r\n*Persentase diatas hanya nilai subjektifitas', 'Topik ini amat jarang dibicarakan. Keberadaannya tahu hanya saja jarang menyentuh topik ini. \r\nSkala 5', 'Angkatan sudah bergerak cukup baik dalam pengmas sebelumnya. Hanya saja frekuensinya dalam membantu masyarakat mungkin kurang. Secara individu sudah terpikir akan masalah ini, tetapi merasa tidak dapat membantu menyelesaikan masalah jika hanya individu.\r\nSkala 7'),
(102, 129, 'ingin menjadi anggota HMIF dan belajar berorganisasi', 'Patah Tulang', '-', 'o', '', '085842857222', 'sudah cukup baik, hanya tinggal beberapa orang perlu ditingkatkan kebersamaannya. nilai 7', 'sudah cukup baik, hanya saja perlu ditingkatkan lagi mengenai etikanya. nilai 6', 'cukup baik, namun baru beberapa orang yang mempunyai inisiatif. nilai 5', 'baik, misalkan yang ikut terlibat aktif dalam oskm dan kabinet km itb. nilai 7', 'baik, sebagian besar angkatan kami ikut serta berkontribusi dalam acara pengmas stei 2012 yang lalu. nilai 7'),
(103, 130, 'cari teman, lebih knal dan dekat sama angkatan, belajar tanggung jawab dari tugas2 yang diberikan', 'dbd', 'maag,dbd', 'O', '0227513829', '02270301364', '8 karena sudah lumayan akrab datu sama lain  namun belum keseluruhan', '7 karena sebagian sudah mulai merasakanapa yg harus kita lakukan mis. dalam tugas angkatan, tapi masih ada yg terima jadi dan masih ada yang komentar ini itu, tapi tp kurang memberikan solusi', '6 karena banyak yg deadliner', '6 karena tidak banyak yg ikut oskm', '7.5 karena saya merasa kita peduli dan kita juga bisa memberikan solusi yg lumayan bagus bagi masalah yg skrng ada (bbm) namun kita masih tidak bisa memberikan aspirasi kita sepebuhnya 6'),
(104, 132, 'Ingin berkontribusi lebih untuk angkatan', 'Tidak ada', 'Tidak ada', 'o', '', '06177398080', '8 sering suporteran', '8 ', '9', '7', '8'),
(105, 133, 'Ingin masuk HMIF. Ingin lebih dekat dan mengakbrabkan diri dengan teman seangkatan.', 'Alergi dingin, maag', 'Alergi dingin, maag', 'O', '085376670868', '082169113890', '7', '6', '6', '6', '8'),
(106, 135, 'Saya ingin memiliki banyak teman, banyak link, belajar berorganisasi, mencari pengalaman dan berkontribusi bagi himpunan', 'amandel dan radang tenggorokan', 'Maag akut, darah rendah, alergi dingin', 'o', '085711661312', '081323093215', 'saya belum dapat memberi komentar karena saya belum ikut berkumpul dengan angkatan karena sakit', 'saya belum dapat memberi komentar karena saya belum ikut berkumpul dengan angkatan karena sakit', 'saya belum dapat memberi komentar karena saya belum ikut berkumpul dengan angkatan karena sakit tetapi cukup baik dengan terciptanya grup untuk berkomunikasi dan saling memberi informasi 8', 'saya belum dapat memberi komentar karena saya belum ikut berkumpul dengan angkatan karena sakit', 'saya belum dapat memberi komentar karena saya belum ikut berkumpul dengan angkatan karena sakit'),
(107, 136, 'ingin berkontribusi di himpunan dan mengenal satu angkatan Captcha', 'thypus, DBD, cacar', '-', 'B', '', '08112221334', '8, hampir sebagian besar sudah dekat hanya tinggal beberapa bagian dirangkul untuk lebih dekat', '8, aktif dalam hampir setiap kegiatan yang diselenggarakan kampus', '7, sudah cukup inisiatif untuk mengadakan acara acara angkatan', '8, kami sudah mengikuti setiap perkembangan kampus', '8, kami sudah mulai menyelenggarakan acara pengabdian masyarakat secara berkala'),
(108, 137, 'Ingin menjadi anggota HMIF', 'N/A', 'N/A', 'AB', '', '0215516877', 'Sejauh ini sudah baik, nilai menurut saya adalah 7', 'Sejauh ini sudah cukup aktif, nilainya adalah 8', 'Sejauh ini sudah cukup inisiatif, nilainya adalah 8', 'Sejauh ini sudah cukup sadar, nilainya adalah 8', 'Sejauh ini sudah cukup sadar, nilainya adalah 9'),
(109, 138, 'Ingin masuk himpunan dan berkontribusi ', 'DBD, gejala Tifus', '-', 'AB', '085693055955', '08161109418', '7. Menurut saya sudah lumayan menyatu dibandingkan awal awal masuk STEI. Dan lebih mengenal lagi semenjak diadakan interaksi serta tugas tugasnya', '7. Sudah lumayan aktif. Beberapa orang sudah ada yang bisa memimpin dan mengkoordinir angkatan.', '8. Lumayan baik inisiatif angkatan. Akhir akhir ini terlihat dari tugas tugas yang diberikan dan banyak yang memberikan ide untuk memudahkan dalam mengerjakan tugas tersebut.', '6. Masih cukup kesadaran terhadap kemahasiswaan terpusat, namun karena acara tersebut suka kurang kedengaran masih kurang yang berpartisipasi', '7. Kesadaran angkatan terhadap masalah bangsa dan masyarakat cukup baik. Dilihat dari pembicaraan saat mengobrol mengenai berita berita yang terjadi di indonesia, sudah mulai cukup kritis dan mengomentari, tinggal berpartisipasinya saja mungkin yang belum terlaksana.'),
(110, 141, 'Lebih mengenal angkatan dan kakak angkatan', 'Tyfus', '?', 'o', '', '085292571478', '7.3', '8.2', '9.1', '8.8', '7.5'),
(111, 142, 'Ingin mengenal angkatan dan mengenal kakak tingkat, dan yang paling utama agar bisa diterima di keluarga HMIF', 'Demam Berdarah', 'tidak ada', 'B', '08157146890', '081329700319', '8', '8', '9', '6', '7'),
(112, 143, 'Ingin masuk HMIF', 'Tifus', 'tidak ada', 'O', '087839896043', '081392186767', 'sudah saling peduli dan care, ingin maju bersama.\r\n9', 'aktif, setiap hari kumpul angkatan\r\n10', 'baik, sudah mau berusaha mengajak dan mengingatkan teman-teman.\r\n9', 'baik, sudah peduli dengan kemahasiswaan\r\n8', 'baik, sudah mengadakan pengmas STEI 2012.\r\n10'),
(113, 144, 'Ingin masuk himpunan (HMIF)', 'Cacar', '-', 'B', '089656579857', '08123308836', '8', '8', '7', '8', '8'),
(114, 145, 'Supaya lebih kompak', 'Tidak ada', 'Tidak ada', 'B', '082128135369', '08997943296', 'Baik, saling peduli sesama teman seangkatan. 9', 'Sudah cukup baik. 8', 'Baik-baik saja. 8', 'Baik, banyak yang mengikuti kegiatan-kegiatannya seperti diklat OSKM, dll. 9', 'Baik, sudah pernah melakukan pengabdian masyarakat. 9'),
(115, 146, 'ingin menjadi anggota HMIF', 'maag', 'tidak ada', 'O', '087821133423', '085276275036', 'selama keberjalanan masa TPB dan interaksi SPARTA, rasa kebersamaan angkatan semakin meningkat.\r\n(7)', 'sudah cukup banyak anggota yang aktif dalam  kegiatan-kegiatan kampus baik itu di kepanitiaan, unit, dan kegiatan terpusat.\r\n(7)', 'sebagian anggota sudah sadar akan pentingnya peran dan kontribusi  mereka untuk angkatan.\r\n(6)', 'kesadaran angkatan terhadap kemahasiswaan terpusat sudah cukup tinggi. Hal ini dibuktikan dengan keikutsertaan anggota dalam OSKM mencapai 48%\r\n(8) ', 'Sebagian anggota belum sadar terhadap masalah bangsa. Namun ada beberapa orang yang sudah ikut berperan langsung dalam upaya pemecahan masalah bangsa, misalnya aksi BBM.\r\n(6)'),
(116, 147, 'ingin mengenal satu angkatan dan berhimpun', '-', '-', 'AB', '-', '08128647110', 'kebersamaan angkatan kami cukup baik, 8', 'angkatan saya cukup aktif, 7', 'inisiatif angkatan saya baik, 8', 'angkatan kami lumayan sadar akan kemahasiswaan terpusat. 7', 'kesadaran angkatan kami terbilang cukup. 7'),
(117, 148, 'Mencari Teman Baru \r\nMelatih diri dalam hal berorganisasi''\r\nIkut Himpunan HMIF\r\nBerinteraksi dengan kakak-kakak himpunan HMIF', 'Thypus \r\nDemam Berdarah\r\nMaag', 'Maag', 'B', '081288394616', '08161351001', 'Nilai 8. Kebersamaan angkatan 2012 masih termasuk dalam kategori baik. Tidak ada masalah di dalam angkatan, keberjalanan acara-acara angkatan sudah termasuk baik. Namun, angkatan 2012 memerlukan waktu untuk bersinergi dan membangun komunikasi yang lebih baik agar saat ada tantangan kami bisa menjalani nya.', 'Keaktifan Angkatan saya nilai 8. Memang masih banyak orang yang belum aktif berpartisipasi tetapi mereka mempunyai alasan masing-masing. Angkatan kami ingin berhasil dalam SPARTA ini dan kami bersama-sama bergerak untuk mewujudkannya. ', '9.Sangat baik sejauh ini inisiatifnya. Banyak ide-ide yang bagus muncul dan angkatan 2012 saya rasa berani berpendapat. memang beberapa tugas terlihat gagal karena kami dalam proses adaptasi di SPARTA ini.', '7. istilah ''Kemahasiswaan terpusat'' merupakan salah satu hal yang menarik angkatan kami ke dalam kepanitiaan OSKM. Angkatan 2012 mempunyai kesadaran yang cukup baik akan kemahasiswaan terpusat', '7. Memang tidak semua orang tertarik dengan permasalahan tersebut tetapi sebagai mahasiswa pastinya kita sudah merasakan berbagai masalah yang ada. Banyak yang peduli terhadap masalah bangsa dan masyarakat namun belum banyak langkah konkret yang dilakukan untuk mengatasinya.'),
(118, 149, 'masuk himpunan', 'maag, demam', 'maag', 'o', '085322783582', '08979770058', 'cukup baik, tapi masih kurang (6)', 'angkatan kami terasa sudah aktif dalam berbagai hal (9)', 'inisiatif angkatan kami terlihat sudah baik (9)', 'cukup kritis terhadap km (8)', 'angkatan kami sudah cukup peduli dengan masalah kemasyarakatan yang ada (8)'),
(119, 151, 'Ingin menjadi anggota HMIF', 'Sinusitis', 'tidak ada', '0', '', '08111777640', 'Kebersamaan angkatan saya sangat baik, bisa dilihat bagaimana kekompakan dalam hal belajar bersama saat masa2 TPB. 9', 'Angkatan saya aktif dalam hal TPBCup, Go Green dan PengMas. 9', 'Inisiatif angkatan saya adalah tetap satu. 8', 'maaf saya kurang tahu. 7', 'kesadaran angkatan saya terhadap permasalahan tersebut bisa dikatakan baik menyangkut masalah ada nya Pengabdia Masyarakat. 9'),
(120, 153, 'bisa masuk hmif', '-', '-', '0', '', '0811632825', 'kompak dalam mengerjakan sesuatu, 8', 'aktif dalam mengerjakan sesuatu bersama.8', 'kami sangat berinisiatif dalam mengerjakannya,8', 'kami sangat sadar terhadap kemahasiswaan terpusat,9', 'sikapnya biasa saja terhadapa masalah bangsa dan masyarakat, 5'),
(121, 154, 'ingin masuk himpunan HMIF', 'cacar air', 'tidak ada', 'B', '', '08122490669', 'kebanyakan sudah solid namun masih ada beberapa yang masih memisahkan diri. skor 7.5/10', 'sebenarnya aktif namun harus ada triggernya. skor 7/10', 'kurang inisiatif, biasanya yang berinisiatif orangnya itu-itu saja. skor 6/10', 'bisa dibilang aktif, banyak yang ikut acara/kegiatan terpusat. skor 8/10', 'menurut saya sudah lumayan peduli walau masih ada sebagian orang yang apatis. skor 7/10'),
(122, 155, 'Motivasi utama saya tentu saja agar saya masuk ke Himpunan Mahasiswa Informatika. Selain itu, Sparta ini juga kesempatan yang sangat besar untuk lebih mengenal angkatan IF+STI 2012, serta sebagai tempat belajar dalam berbagai hal.', 'Tidak ada yang parah (kalau batuk/flu sudah tidak terhitung)', 'Tidak ada', 'O', '085694957838', '081806727235', 'Tentu saja masih terlihat kubu-kubu di angkatan saya. Akan tetapi, kami sudah dapat saling berbaur, apalagi pada saat pengerjaan tugas wawancara tiga hari terakhir.\r\nNilai : 6', 'Keaktifan dalam hal mengerjakan tugas cukup baik, tapi dalam interaksi masih kurang.\r\nNilai: 7', 'Menurut saya, inisiatif angkatan kami masih kurang, contohnya dalam mengerjakan tugas, kebanyakan menunggu jarkom daripada mencari info sendiri-sendiri\r\nNilai: 5', 'Jujur kalau kondisi angkatan saya kurang tahu, tapi dari beberapa orang yang saya kenal, kesadarannya masih kurang\r\nNilai: 5', 'Sama seperti poin kemahasiswaan terpusat, saya kurang tahu kondisi angkatan soal ini.\r\nNilai: 6'),
(123, 156, 'Saya ingin masuk HMIF dan berpartisipasi aktif di dalamnya', 'Tifus, Maag, Alergi makanan (mie campur telur, ada lagi tapi saya tidak mengetahui pasti), Sinusitis', 'Alergi, Sinusitis', 'A', '085876906500', '08122610195', 'Kebersamaan angkatan kami sudah sedikit membaik dibanding di semester awal TPB, saya memberi skala 6', 'Keaktifan angkatan kurang begitu kelihatan, seperti dalam hal menginterupsi di suatu forum, skala saya beri 3', 'Inisiatif angkatan saya nilai baik, meski orang - orang yang berperan aktif sedikit, saya beri skala 5', 'Kesadaran angkatan terhadap kemahasiswaan terpusat kurang begitu aktif saya nilai, hanya separuh kurang dari total kami mahasiswa IF dan STI 2012 yang mengikuti diklat OSKM terpusat, skala saya beri 5', 'Saya kurang memandang baik angkatan saya terhadap masalah bangsa, cukup banyak dari kami yang masih mementingkan studi dengan berlebihan (study oriented), saya beri skala 3'),
(124, 157, 'Ingin masuk dan aktif di himpunan', 'Penyakit pada umumnya, tidak ada yang berat', 'tidak ada', 'B', '', '08125314426', '2 sudah cukup banyak event yang diadakan untuk meningkatkan kebersamaan dalam angkatan. walaupun masih terkotak-kotak, tetapi masih dalam proses untuk menjalin kebersamaan yang lebih erat', '3 angkatan kami sudah cukup aktif dalam berbagai kegiatan walaupun dapat dikatakan yang aktif hanya oknum tertentu saja', '3 angkatan kami sudah cukup inisiatif, hanya saja masih ada kurang komunikasi sehingga terjadi kesalahan tanggap khususnya dalam hal tugas', '3 hal ini terbukti dari banyaknya anggota dari angkatan kami yang mengikuti diklat terpusat OSKM dan beberapa menjadi orang yang berpengaruh', '3 sudah cukup sering membahas permasalahan bangsa melalui berbagai media sosial walaupun yang tertarik hanya beberapa saja'),
(125, 158, 'Kenal dan dekat dengan satu angkatan, masuk himpunan HMIF, sebagai fasilitas pengembangan diri dan softskill', 'DBD', 'Alergi dingin dan debu, asma', 'B', 'Uthi 085722307324 / Shafira 087870968948', '0818856511', 'secara keseluruhan sudah lumayan baik, namun kepedulian terhadap semua orang dalam angkatan masih kurang. mungkin karena jumlah cukup banyak dan masih ada beberapa yang belum saling kenal. Nilai 7.', 'cukup aktif, STEI 2012 termasuk CAPTCHA 2012 berkontribusi aktif di kegiatan-kegiatan di luar STEI. Beberapa orang cukup menonjol dan berpengaruh di ITB 2012. Nilai 8.', 'Inisiatif cukup baik. Contohnya saat TPB STEI, pemilihan ketua angkatan berdasarkan prosedur dan terdapat komisi pemilihan ketua angkatan tidak seperti di fakultas lainnya. Contoh lainnya adalah adanya Pengmas. Nilai 7.', 'Belum cukup memuaskan. Namun menurut saya, fakutas lain pun tidak jauh berbeda. Nilai 6.', 'CAPTCHA 2012 peduli terhadap masalah bangsa dan masyarakat dan ingin memperbaiki bangsa di bidang-bidang yang kami sukai. Nilai 9.'),
(126, 159, '-Ingin Belajar dan Lebih Mengenal Teman Seangkatan\r\n-Ingin masuk HMIF', 'Maag, Typhus', 'Tidak Ada', 'B', '08164212878', '08122497555', '6, setiap kumpul angkatan belum pernah kumpul full team.', '4,  saat ditanya oleh panitia angkatan kami masih ragu atau takut untuk menjawab.', '7, saat ada masalah atau panitia memberi tugas angkatan kami berkumpul untuk membahasnya dan mencara jalan keluar yang tepat.', '9, saat kedua presiden didiskualifikasi, angkatan kami banyak yang menghadiri undangan dari Altera untuk membahas hal ini.', '7, kemarin di group STEI 2012 ada diskusi sederhana mengenai kenaikan harga BBM. Sebagian orang yang berdiskusi adalah calon peserta sparta.'),
(127, 160, 'Ingin masuk himpunan', '-', 'hipotensi', '0', '085318942109', '082161091265', '5', '8', '8', '9', '7'),
(128, 161, 'ingin kenal lebih dekat dengan HMIF', 'typhus, flu, batuk, pusing, diare, patah tulang, paru-paru basah', 'dermatitis atopik', 'o', '', '0811282189', 'Kompak kok, cuma agak mager.\r\nnilai: 9', 'yang aktif aktif, yang mager mager.\r\nnilai: 8', 'jos lah. belom kuliah aja udah bikin makrab.\r\nnilai: 10', 'sadar. tuh banyak yang nyalon panitia OSKM\r\nnilai: 7', 'ada pengmas, ada yg bikin buku, banyak blog, dsb.\r\nnilai: 7'),
(129, 162, '- Ingin lebih mengenal satu angkatan 2012 lebih jauh\r\n- Ingin mengenal kakak2 tingkat di HMIF lebih jauh\r\n- Ingin belajar ber-organisasi\r\n- Ingin belajar kepemimpinan\r\n- Ingin aktif di himpunan HMIF', '-', '-', 'B', 'Syaiful 085655106050', '081228741366', 'scara keseluruhan sudah cukup bagus, walaupun ada beberapa orang yang jarang keliatan saat kumpul angkatan\r\nnilai : 8', 'sudah cukup aktif, hal ini dapat di lihat dari pertanyaan2 yang sering diajukan saat interkasi berlangsung\r\nnilai : 7', 'sudah bagus, cepat dalam mengambil keputusan tanpa menunggu perintah\r\nnilai : 9', 'sudah cukup bagus, hal ini dapat di lihat dari sekitar 60an anggota SPARTA yang mengikuti kegiatan OSKM terpusat, dan kegiatan lainya\r\nnilai : 7', 'sudah bagus, salah satu nya dengan PENGMAS yang pernah kita lakukan \r\nnilai : 9'),
(130, 163, 'Sebagai awal dari bentuk persiapan diri agar bisa lebih berkontribusi dalam himpunan (HMIF).', 'Sinusitis', '-', 'O', '081223600428 (Bapak Kos)', '081319575918', '7. Angkatan kami sudah lumayan kompak, namun mungkin karena jumlahnya yang terlalu banyak, jadi sulit untuk membuat semua orang di angkatan benar-benar saling akrab satu sama lain.', '8. Menurut saya, angkatan kami memiliki tingkat keaktifan yang cukup baik.', '9. Secara keseluruhan, angkatan kami punya inisiatif yang tinggi dalam banyak hal.', '8. Angkatan kami bisa menunjukkan dukungan/kontribusi terhadap kemahasiswaan terpusat.', '8. Angkatan kami rata-rata cukup memahami dan concern dengan permasalahan bangsa, walaupun penerapan aksi/tindakannya belum maksimal.'),
(131, 164, 'Ingin lebih mengenal himpunan', 'Demam berdarah', '-', 'B', '(022)6623753', '08122417052', 'Cukup sering ikut dalam kumpul-kumpul angkatan (7)', 'Kurang begitu aktif (5)', 'Inisiatif angkatan sudah lumayan baik, ditandai dengan acara-acara yang bervariasi (8)', '(6)', 'Lumayan kritis dalam memikirkan masalah di negeri ini (6)'),
(132, 165, 'ingin mengenal teman satu angkatan lebih baik dan ikut himpunan', 'demam berdarah', 'batuk, radang kulit', 'A', '', '0818790086', '6, karena masih ada yang jarang berkumpul', '6, cukup aktif tapi perlu ditingkatkan karena masih banyak juga yang tidak aktif', '6, masih banyak yang kurang berinisiatif', '5, saya sendiri kurang tau', '7, mungkin soal kesadaran sudah banyak yang sadar tapi untuk memperbaiki belum tau caranya'),
(133, 167, 'Aktualisasi diri di himpunan, menambah teman, berkontribusi untuk himpunan dan ITB', 'Dislokasi bahu kanan (April 2013)\r\nGejala tifus\r\nDBD', 'Bahu kanan masih kaku. (akibat dislokasi April 2013)', 'B', '085221007994', '08122681513', '4, kurang menyatu karena banyak yang masih "selfish", kurang memaknai kebersamaan angkatan yang meliputi senang, sedih, menanggung problem bersama.', '3, hanya beberapa orang yang proaktif, sesuai kata dan tingkah. lainnya masih pasif, atau reaktif', '4, cukup baik, namun masih kurang inisatif, sering menunggu daripada mencari informasi.', '5, banyak anak calon HMIF 2012 ikut diklat oskm 2013. Namun sebelum ada acara tersebut, STEI keseluruhan masih kurang aktif.', '6, cukup baik dengan mengadakan pengmas saat masih STEI, namun kurang karena yang datang hanya setengah dari angkatan.'),
(134, 168, 'Ingin berkontribusi secara aktif di himpunan serta angkatan dan ingin mengharumkan nama HMIF di kemahasiswaan terpusat.', 'Tidak ada', 'Tidak ada', 'A', '085282628896', '081288198511', '7', '7', '8', '7', '8'),
(135, 169, 'Karena saya ingin menjadi anggota HMIF', 'maag', '-', 'O', '08996076888', '087825190640', '8.sampai saat ini saya merasakan angkatan saya jauh lebih dekat satu sama lain dari pada saat pertama-pertama. namun masih ada beberapa orang yang jarang kumpul bersama jika ada kegiatan bersama', '8. angkatan 2012 saya rasa cukup aktif. kita pernah mengadakan pengmas dan 2x mengadakan makrab. angkatan kami juga pernah beberapa kali mengadakan main bersama seperti futsal, voley, dll', '8. inisiatif angkatan 2012 sudah baik menurut saya. misalnya setelah diberikan tugas seperti saat interaksi sparta ini, setelah interaksi selesai angkatan kami mengadakan kumpul dulu bersama untuk membahas tugas nya dan menentukan PJ untuk masing-masing tugas supaya ada yang mengatur tugas tsb agar lebih mudah dilakukan. ', '7. cukup banyak STEI 2012 yang mengikuti diklat OSKM terpusat dan ada beberapa dari angkatan 2012 yang aktif di KM-ITB.', '7. menurut saya setelah angkatan kami mengadakan pengmas, itu merupakan salah satu indikator bahwa angkatan kami peduli dengan masalah di sekitar masyarakat. '),
(136, 170, 'Ingin lebih kenal dengan angkatan\r\nIngin diterima di hmif', 'Maag', 'Maag', 'o', '081322975540', '081320176855', 'Terjalin dengan baik. Contohnya sering mengadakan futsal bareng. Beberapa waktu lalu mengadakan makrab.\r\n9', 'Aktif. Banyak yang ikut dalam kepanitiaan, aktif di unit, keagamaan (contoh; MSTEI)\r\n8', 'Baik. Contohnya pengmas stei 2012 adalah pengmas pertama dalam satu angkatan.\r\n8', 'Baik. Ada yang aktif di km, banyak juga yang ikut kepanitiaan oskm 2013\r\n8', 'Cukup baik. Contohnya beberapa bulan lalu STEI 2012 mengadakan pengmas.\r\n8'),
(137, 171, 'ingin menjadi bagian dari HMIF, mengenal satu angkatan dengan baik', 'tidak ada', 'maag', 'O', '0811975500', '081310851102', '8\r\nMenurut saya, hampir semua anggota sudah mulai kenal dan membantu satu sama lain', '7\r\nbeberapa dari anggota sudah cukup aktif dalam bertanya dan mengemukakan pendapat dalam setiap forum atau kumpul angkatan', '7\r\ncukup banyak dari kami yang mau inisiatif dalam memberikan solusi pada setiap tugas atau permasalahan yang kami dapatkan, walaupun jumlahnya belum terlalu banyak', '7\r\nKesadarannya sudah cukup baik terhadap kegiatan kemahasiswaan', '5\r\nMenurut saya kesadaran angkatan terhadap masalah bangsa belum cukup baik karena masih ada yang belum peduli'),
(138, 172, 'Ingin masuk hmif', 'Ginjal', 'Tidak ada', 'B', '085714597575', '0818133044', '7', '6', '8', '6', '7'),
(139, 173, 'Ingin mengikuti himpunan dan ingin kenal lebih dekat dengan angkatan, juga mendapatkan pengalaman baru.', 'Demam berdarah, maag', 'Maag (sebelumnya belum pernah, tapi akhir-akhir ini sering kena), kondisi fisik yang tidak memungkinkan untuk melakukan aktivitas berat (obesitas).', 'A', '', '08156119393', 'skala 8\r\nMeskipun pada awalnya masih terdapat kotak-kotak, namun kotak-kotak tersebut mau dan mulai berbaur dengan kotak-kotak lainnya. Yang saya lihat sekarang adalah angkatan ini mulai terbuka dan semakin erat.', 'skala 8\r\nAngkatan saya bisa terbilang aktif. Hal ini dapat dilihat dari banyaknya yang mengikuti diklat OSKM ataupun kegiatan lainnya (seperti unit, pentas). Selain itu, juga mengikuti TPB Cup dan supporteran angkatan ini sangat bagus, bahkan meraih juara.', 'skala 7\r\nInisiatif terbesar angkatan saya adalah ketika mengadakan pengmas bulan lalu. Dari situ bisa dilihat keinisiatifan angkatan ini untuk peduli pada masyarakat. Selain itu juga inisiatif dalam mengadakan makrab sebagai perpisahan sebelum penjurusan.', 'skala 7\r\nTerhadap kemahasiswaan terpusat, angkatan saya cukup aktif dengan melihat terdapat 66 orang yang mengikuti diklat terpusat. Selain itu, cukup banyak juga yang menjadi bagian dari Kabinet Mahasiswa juga kepanitiaan-kepanitiaan terpusat.', 'skala 8\r\nAngkatan saya telah mengadakan pengmas bulan lalu. Hal ini merupakan satu langkah besar angkatan ini untuk peduli terhadap masalah bangsa dan masyarakat. Selain itu banyak diantara kami yang terlihat sering menolong orang lain yang membutuhkan.'),
(140, 174, 'Mendekatkan diri kepada angkatan, mencoba melihat hal-hal yang bisa dilakukan dalam HMIF', 'Amandel', 'Maag', 'A', '', '0817828222', 'untuk IF-STI, menurut saya kebersamaannya sudah mulai membaik, terutama dibandingkan ketika bersama-sama dengan STEI 2012. sekarang ini mulai pada membaur satu sama yang lain. mungkin masih perlu perhatian untuk beberapa anak, terutama yang berasal dari daerah-daerah luar Jawa (yang belum balik ke Bandung). 7', 'mungkin harus didefinisikan, ''aktif'' dalam hal apa? kalo secara kemahasiswaan, saya melihat anak-anak dalam angkatan rata-rata cukup aktif, soalnya hanya segelintir yang non-unit/tidak aktif dalam KM/organisasi apapun, itupun sebagian beralasan ingin masuk dan berfokus pada HMIF. jadi menurut saya, angkatan kami cukup aktif. 8.', 'dorongan untuk inisiatif angkatan selama ini yang saya perhatikan lebih baik dibandingkan ketika masih satu STEI 2012. yang saya lihat, justru di sini lebih banyak anak-anak yang mencoba inisiatif (selain anak-anak yang memang sudah/pernah aktif ketika masih TPB). 8', 'sejujurnya, bisa dibilang angkatan kami - termasuk saya - cukup aktif dalam kegiatan kemahasiswaan terpusat. dari beberapa teman yang saya ketahui, mereka setidaknya cukup aktif dalam memberikan voting (ketika pemilu raya KM - referendum itu isu lain), ikut dalam acara-acara KM seperti olim, dan bahkan ada beberapa anak yang saya tahu memang aktif dalam kabinet KM. menurut saya, walaupun tidak semuanya ikut, ini bukti yang menunjukkan bahwa angkatan kami cukup sadar tentang kemahasiswaan terpusat. 7', 'kalo yang ini saya mungkin katakan masih terbilang cukup. anak-anak yang saya tahu cukup aware dengan berita yang tersiar, terutama soal isu-isu yang cukup penting. mungkin kekurangannya adalah isu-isu tersebut kurang dibawa dalam diskusi sehari-hari, jadinya hanya ibarat pengetahuan belaka/hanya sebagian yang ''ngeh''. 5'),
(141, 175, 'Mau masuk himpunan dan belajar berorganisasi', 'Tifus', 'Cedera kaki\r\nDarah rendah', 'A', '081585833579', '08988881551', '8', '9', '8', '7', '9'),
(142, 176, 'Kewajiban, mengenal dekat satu angkatan, belajar hal baru', 'Asma', '-', 'o', '08157033551', '08157008669', '7', '9', '7', '5', '4'),
(143, 177, 'Syarat wajib masuk himpunan', '-', '-', 'O+', '', '081319279337', 'Angkatan 2012 merupakan angkata yang cukup solid. Penilaian : 7', 'Angkatan 2012 termasuk angkatan yang cukup aktif.\r\nPenilaian : 7', 'Sangat Inisiatif\r\nPenilaian : 8', 'Cukup sadar terhadap kemahasiswaan terpusat\r\nPenilaian : 6', 'Bisa dibilang sadar\r\npenilaian : 6'),
(144, 178, 'Himpunan merupakan sesuatu yang saling menguntungkan', 'Asma, Hipotensi', 'Tidak ada', 'B', '', '08161361653', 'Kebanyakan sudah dapat bersatu dengan baik, namun ada beberapa yang susah untuk diajak bersatu, 8', 'Angkatan IF dan STI 2012 banyak yang aktif di organisasi lain, 8', 'Inisiatif dan keinginan angkatan IF dan STI 2012 untuk ikut serta dalam berbagai acara di kampus sudah baik, namun hanya beberapa 7', 'Saya melihat beberapa teman IF dan STI 2012 yang mengikuti diklat OSKM, 7', 'Saya kurang tahu tentang pengetahuan mereka tentang masalah bangsa dan masyarakat. Ada beberapa yang sering menuliskan tentang kepeduliannya terhadap masalah negara, 6'),
(145, 180, 'ingin mengenal angkatan 2012 yang masuk ke prodi IF dan STI, mengenal budaya yang dipegang HMIF, lebih melebur ke dalam angkatan dan membuatnya menjadi angkatan yang kompak', 'usus buntu', 'tidak ada', 'A+', '085866226379', '0811181637', 'cukup baik karena sudah mampu unutk menyelenggarakan acara bersama dalam bentuk makrab, pengmas, dan lain-lain. 8', 'untuk keaktifan masih didominasi oleh beberapa orang saja, namun yang lain pun masih memberikan aspirasi walaupun dengan bentuk seperti celetukan yang sesuai dengan opik diskusi.. 7', 'unutk inisiatif, angkatan kami cukup baik terlihat dari sikap kami dalam mengerjakan tugas yang diberikan. 7', 'untuk hal ini, saya rasa belum begitu sadar.. 6', 'angkatan kami punya banyak wawasan mengenai masalah masyarakat yang sedang terjadi. hal ini terlihat nyata saat forum diskusi yang membahas persoalaan yang sama saat PLO maupun diklat OSKM.. 8'),
(146, 181, '-', '-', '-', 'A', '-', '022165135182', '5', '5', '5', '5', '5'),
(147, 182, 'Menambah pengalaman', 'Tidak ada', 'Tidak ada', 'B', '', '08128838118', 'Baik-baik saja', 'Baik-baik saja', 'Baik-baik saja', 'Baik-baik saja', 'Baik-baik saja');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE IF NOT EXISTS `timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `timestamp_published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `tipe_pengumpulan_tugas` int(2) NOT NULL,
  `deskripsi` text NOT NULL,
  `timestamp_published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_deadline` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure for view `medik`
--
DROP TABLE IF EXISTS `medik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`spar2web`@`localhost` SQL SECURITY DEFINER VIEW `medik` AS select `peserta`.`nama_lengkap` AS `nama_lengkap`,`peserta`.`nim_stei` AS `nim_stei`,`tambahan`.`penyakit_pernah` AS `penyakit_pernah`,`tambahan`.`penyakit_sedang` AS `penyakit_sedang`,`tambahan`.`golongan_darah` AS `golongan_darah` from (`peserta` join `tambahan` on((`peserta`.`id` = `tambahan`.`id_peserta`))) order by `peserta`.`nim_stei`;

-- --------------------------------------------------------

--
-- Structure for view `sakit`
--
DROP TABLE IF EXISTS `sakit`;

CREATE ALGORITHM=MERGE DEFINER=`spar2web`@`localhost` SQL SECURITY DEFINER VIEW `sakit` AS select `tambahan`.`id` AS `id`,`tambahan`.`id_peserta` AS `id_peserta`,`tambahan`.`motivasi` AS `motivasi`,`tambahan`.`penyakit_pernah` AS `penyakit_pernah`,`tambahan`.`penyakit_sedang` AS `penyakit_sedang`,`tambahan`.`golongan_darah` AS `golongan_darah`,`tambahan`.`nomor_kerabat` AS `nomor_kerabat`,`tambahan`.`nomor_ortu` AS `nomor_ortu`,`tambahan`.`kebersamaan` AS `kebersamaan`,`tambahan`.`keaktifan` AS `keaktifan`,`tambahan`.`inisiatif` AS `inisiatif`,`tambahan`.`kesadaran_mahasiswa` AS `kesadaran_mahasiswa`,`tambahan`.`kesadaran_bangsa` AS `kesadaran_bangsa`,`peserta`.`id_akun` AS `id_akun`,`peserta`.`id_kelompok` AS `id_kelompok`,`peserta`.`nama_lengkap` AS `nama_lengkap`,`peserta`.`nama_panggilan` AS `nama_panggilan`,`peserta`.`tempat_lahir` AS `tempat_lahir`,`peserta`.`tanggal_lahir` AS `tanggal_lahir`,`peserta`.`jurusan` AS `jurusan`,`peserta`.`nim_stei` AS `nim_stei`,`peserta`.`nim_jurusan` AS `nim_jurusan`,`peserta`.`alamat` AS `alamat`,`peserta`.`alamat_latitude` AS `alamat_latitude`,`peserta`.`alamat_longitude` AS `alamat_longitude`,`peserta`.`alamat_zoom` AS `alamat_zoom`,`peserta`.`alamat_rumah` AS `alamat_rumah`,`peserta`.`no_telp` AS `no_telp`,`peserta`.`email` AS `email` from (`tambahan` join `peserta` on((`tambahan`.`id` = `peserta`.`id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
