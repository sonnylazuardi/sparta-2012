<?php
$this->breadcrumbs=array(
	'Timelines'=>array('index'),
	$model->id,
);

?>

<h1>View Timeline #<?php echo $model->id; ?></h1>

<div class="gadget-4">
	<div class="gadget-container">
		<?php if (Yii::app()->user->checkAccess('admin')): ?>
		   <br>
		   <a href="<?php echo Yii::app()->createUrl('/timeline/update', array('id'=>$model->id)) ?>"><span data-icon="&#xe003" aria-hidden="true" style="float: right; clear: right;">&nbsp;&nbsp;&nbsp;</span></a>	
	    <?php endif ?>
		<div class="gadget-content">
             <div id="text-content"class="content">
	            <label><span id="article-date"><?php echo date('Y/m/d', strtotime($model->timestamp_published)) ?></span></label>
				<h1 id="article-title"><?php echo $model->judul ?></h1>

				<div id="article-content">
						<?php echo $model->deskripsi ?>
				</div>
			</div>
		</div>	 
	</div>
</div>