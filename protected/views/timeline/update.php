<?php
$this->breadcrumbs=array(
	'Timelines'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Timeline','url'=>array('index')),
	array('label'=>'Create Timeline','url'=>array('create')),
	array('label'=>'View Timeline','url'=>array('view','id'=>$model->id)),
);
?>

<div class="row">
	<div class="well span6 offset3">
		<div class="text-container">
	<div id="text-content" class="content">
		<h1>Update <?php echo $model->judul; ?><span><?php echo CHtml::link("Delete",array('delete','id'=>$model->id),array('class'=>'delete')); ?></span></h1>
		
		<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
		
	</div>
</div>
	</div>
</div>
<center><a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="button">Kembali</a></center>