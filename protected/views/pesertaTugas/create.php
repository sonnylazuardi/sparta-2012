<?php 
/* @var $this PesertaTugasController */
/* @var $model PesertaTugas */

$this->breadcrumbs=array(
	'Peserta Tugases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PesertaTugas', 'url'=>array('index')),
	array('label'=>'Manage PesertaTugas', 'url'=>array('admin')),
);
?>

<h1>Create PesertaTugas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>