<?php
/* @var $this PesertaTugasController */
/* @var $model PesertaTugas */ 

$this->breadcrumbs=array(
	'Peserta Tugases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PesertaTugas', 'url'=>array('index')),
	array('label'=>'Create PesertaTugas', 'url'=>array('create')),
	array('label'=>'Update PesertaTugas', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PesertaTugas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PesertaTugas', 'url'=>array('admin')),
);
?>

<h1>View PesertaTugas #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_tugas',
		'id_peserta',
		'link',
		'timestamp',
		'pranala_tugas_unggah',
		'status',
	),
)); ?>
