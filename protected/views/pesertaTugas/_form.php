<?php
/* @var $this PesertaTugasController */
/* @var $model PesertaTugas */ 
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'peserta-tugas-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<?php if($model->uploadedfile == null) echo "wkwk"?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'id_tugas'); ?>
		<?php echo $form->textField($model,'id_tugas'); ?>
		<?php echo $form->error($model,'id_tugas'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'id_peserta'); ?>
		<?php echo $form->textField($model,'id_peserta'); ?>
		<?php echo $form->error($model,'id_peserta'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'uploadedfile'); ?>
		<?php echo $form->fileField($model,'uploadedfile'); ?>
		<?php echo $form->error($model,'uploadedfile'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->