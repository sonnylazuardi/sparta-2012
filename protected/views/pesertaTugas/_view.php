<?php 
/* @var $this PesertaTugasController */
/* @var $data PesertaTugas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tugas')); ?>:</b>
	<?php echo CHtml::encode($data->id_tugas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_peserta')); ?>:</b>
	<?php echo CHtml::encode($data->id_peserta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
	<?php echo CHtml::encode($data->link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pranala_tugas_unggah')); ?>:</b>
	<?php echo CHtml::encode($data->pranala_tugas_unggah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>