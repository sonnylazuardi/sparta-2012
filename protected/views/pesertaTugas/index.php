<?php 
/* @var $this PesertaTugasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Peserta Tugases',
);

$this->menu=array(
	array('label'=>'Create PesertaTugas', 'url'=>array('create')),
	array('label'=>'Manage PesertaTugas', 'url'=>array('admin')),
);
?>

<h1>Peserta Tugases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
