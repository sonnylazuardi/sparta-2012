<?php
/* @var $this PesertaTugasController */
/* @var $model PesertaTugas */ 

$this->breadcrumbs=array(
	'Peserta Tugases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PesertaTugas', 'url'=>array('index')),
	array('label'=>'Create PesertaTugas', 'url'=>array('create')),
	array('label'=>'View PesertaTugas', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PesertaTugas', 'url'=>array('admin')),
);
?>

<h1>Update PesertaTugas <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>