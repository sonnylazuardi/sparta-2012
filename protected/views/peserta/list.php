<div id="profile-peserta" class="row">
	<div id="list-peserta" class="large-10 columns small-centered">
		<div class="row">
			<input type="search" class="search center small-8 columns small-centered" placeholder="Search by Name or NIM"\>
			<div class="small-11 small-centered columns">
			<span class="sort small-6 large-3 columns butt" data-sort="nama">Sort by Name</span>
    		<span class="sort small-6 large-3 columns butt" data-sort="nim">Sort by NIM</span>
    		<!--<span class="sort small-4 large-2 columns butt" id="filter-IF">Show IF</span>
    		<span class="sort small-4 large-2 columns butt" id="filter-STI">Show STI</span>
    		<span class="sort small-4 large-2 columns butt" id="filter-none">Show All</span>
    		-->
    		</div>
		</div>
		
		<ul class="row list no-bullet">
		<?php foreach ($model as $peserta): ?>
		<a href="<?php echo Yii::app()->createUrl('/peserta/view', array('id'=>$peserta->id)) ?>">
		  <li class="profile list">
		    <div class="profile-container gadget-container">
		        <div class="profile-pic" style="background:url(<?php echo ($peserta->profpicImage); ?>) no-repeat;"></div>
		        <div class="profile-desc">
		          <h4 class="nama"><?php echo $peserta->nama_lengkap ?></h4>
		          <h5 class="nim"><?php echo $peserta->nim_stei ?></h5>
		          <p class="jurusan"><?php echo $peserta->jurusan ?></p>
		        </div>

		    </div>
		  </li> 
		</a>
		<?php endforeach ?>
		
		</ul>
		<footer>
		<p id="copyright">Total: <?php echo count($model)?></p>
		</footer>
	</div>
</div>
<script>
	var options = {
    valueNames: [ 'nama', 'nim', 'jurusan' ]
};

var pesertaList = new List('list-peserta', options);

    $('#filter-IF').click(function() {
        featureList.filter(function(item) {
            if (item.values().jurusan == "Teknik Informatika") {
                return true;
            } else {
                return false;
            }
        });
        return false;
    });

    $('#filter-STI').click(function() {
        featureList.filter(function(item) {
            if (item.values().jurusan == "Sistem Teknologi Informasi") {
                return true;
            } else {
                return false;
            }
        });
        return false;
    });
    $('#filter-none').click(function() {
        featureList.filter();
        return false;
    });
</script>