
  

<section id="profile-peserta" class="row">
  <div class="profile view">
    <div class="profile-container gadget-container">
    
      <div class="profile-pic" style="background:url(<?php echo ($model->profpicImage); ?>) no-repeat;"></div>
        <div class="profile-desc">
          <h1 class="nama"><?php echo $model->nama_lengkap ?></h1>
          <h3 class="nim"><?php echo $model->nim_stei ?></h3>
          <p class="jurusan"><?php echo $model->jurusan ?></p>
          <?php if (Yii::app()->user->checkAccess('admin')): ?>
          <p class="telephone"><?php echo $model->no_telp ?></p>
          <p class="email"><?php echo $model->email ?></p>
          <p class="golongan_darah"><?php echo $model->tambahan->golongan_darah ?></p>
          <p class="penyakit_sedang"><?php echo $model->tambahan->penyakit_sedang ?></p>
          <?php endif  ?>
        </div>

    </div>
  </div>
  
  <div id="map" class="view">
    <div id="map-container"class="gadget-container clear">
      <div id="map-area"></div>
    </div>
  </div>
<?php if (Yii::app()->user->checkAccess('admin')): ?>
  <div class="gadget-4">
    <div class="gadget-container">
      <header class="gadget-header">
        <span>Kebersamaan</span>
      </header>
      <div class="gadget-content">
        <p><?php echo $model->tambahan->kebersamaan ?></p>
      </div>
    </div>
  </div> 
  <div class="gadget-1">
    <div class="gadget-container">
      <header class="gadget-header">
        <span>Keaktifan</span>
      </header>
      <div class="gadget-content">
        <p><?php echo $model->tambahan->keaktifan ?></p>
      </div>
    </div>
  </div> 
  <div class="gadget-1">
    <div class="gadget-container">
      <header class="gadget-header">
        <span>Inisiatif</span>
      </header>
      <div class="gadget-content">
        <p><?php echo $model->tambahan->inisiatif ?></p>
      </div>
    </div>
  </div> 
  <div class="gadget-1">
    <div class="gadget-container">
      <header class="gadget-header">
        <span>Kesadaran Mahasiswa</span>
      </header>
      <div class="gadget-content">
        <p><?php echo $model->tambahan->kesadaran_mahasiswa ?></p>
      </div>
    </div>
  </div>
  <div class="gadget-1">
    <div class="gadget-container">
      <header class="gadget-header">
        <span>Kesadaran Bangsa</span>
      </header>
      <div class="gadget-content">
        <p><?php echo $model->tambahan->kesadaran_bangsa ?></p>
      </div>
    </div>
  </div>  
<?php endif ?>  
</section>
<!--Scripts dari external source-->
  <script src="<?php echo Yii::app()->baseUrl ?>/js/clock.js"></script>
  <script>
    var maplace = new Maplace({
      map_div: '#map-area'
      ,locations: [
        {
          lat: <?php echo $model->alamat_latitude ?>, 
          lon: <?php echo $model->alamat_longitude ?>,
          zoom: 16
        }]
      ,styles: {
        'Other style': 
        [
          {
            stylers: [
              { hue: "#00ffe6" },
              { saturation: -20 }]
          }, 
          {
            featureType: "road",
            elementType: "geometry",
            stylers: 
              [
                { lightness: 100 },
                { visibility: "simplified" }
              ]
          }, 
          {
          featureType: "road",
          elementType: "labels",
          stylers: 
            [
              { visibility: "off" }
            ]
          }],
        'Night': 
        [
          {
            featureType: 'all',
            stylers: 
            [
              { invert_lightness: 'true' }
            ]
          }],
        'Greyscale': 
        [
          {              
            featureType: 'all',
            stylers: 
            [
              { saturation: -100 },
              { gamma: 0.50 }
            ]
          }]}
      ,controls_on_map: false
    }); 
    maplace.Load(); 
    var currentHours = <?php echo date('H') ?>;
    var currentMinutes = <?php echo date('i') ?>;
    var currentSeconds = <?php echo date('s') ?>;
    function updateClock ( )
    {
      currentSeconds++;
      if (currentSeconds > 59) {
        currentSeconds = 0;
        currentMinutes++;
        if (currentMinutes > 59) {
          currentMinutes = 0;
          currentHours++;
          if (currentHours > 24) {
            // ajax
          }
        }
      }
      // Pad the minutes and seconds with leading zeros, if required
      currentMinutes = ( currentMinutes.length < 2 ? "0" : "" ) + currentMinutes;
      currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

      // Compose the string for display
      var currentTimeString = currentHours + ":" + currentMinutes;
      var currentSecondString = currentSeconds;
      
      // Update the time display
      document.getElementById("hour-minute").firstChild.nodeValue = currentTimeString;
      document.getElementById("second").firstChild.nodeValue = currentSecondString;
    }
    updateClock(); setInterval('updateClock()', 1000 );
  </script>
<!--Scripts END-->
