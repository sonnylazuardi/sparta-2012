<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$peserta =  Akun::model()->findByPk(Yii::app()->user->id)->peserta;
?>

<section id="profile-peserta" class="row">
  <div id="clock" class="hide-for-small">
    <div class="gadget-container">
      <div class="gadget-content">
        <span data-icon="&#xe002" aria-hidden="true"></span>
        <h1>Today</h1>
        <span id="hour-minute"><?php echo date('H:i') ?></span><span id="second"><?php echo date('s') ?></span>
        <p id="date"><?php echo date('D j F Y') ?></p>
      </div>
    </div>
  </div>
  <div class="profile">
    <div class="profile-container gadget-container">
    
        <div class="profile-pic" style="background:url(<?php echo ($peserta->profpicImage); ?>) no-repeat;"></div>
        <div class="profile-desc">
          <h1 class="nama"><?php echo $peserta->nama_lengkap ?></h1>
          <h3 class="nim"><?php echo $peserta->nim_stei ?></h3>
          <p class="jurusan"><?php echo $peserta->jurusan ?></p>
          <a href="<?php echo Yii::app()->createUrl('/peserta/profil') ?>"><span class="transparent bottom-corner" data-icon="&#xe000" aria-hidden="true"></span></a>
        </div>

    </div>
  </div>
  
  <div id="map">
    <div id="map-container"class="gadget-container clear">
      <div id="map-area"></div>
    </div>
  </div>  

  <?php $this->widget('TimelineWidget', array('timelineData'=>$timelineData)); ?>

<div id="poin" class="gadget-2">
  <div class="gadget-container">
    <header class="gadget-header">
      <span>Poin Angkatan</span>
    </header>
    <div class="gadget-content">
      <p>
      <?php
        $this->Widget('ext.chartpoin.ChartPoinSparta', array('data'=>$data, 'start'=>$start))
      ?>
    </p>
    </div>
  </div>
</div>
<div class="gadget-2">
    <div class="text-container">
      <header class="gadget-header">
        <span>Articles</span>
      </header>
      <ul id="sidebar-list">
        <?php $this->widget('bootstrap.widgets.TbListView',array(
          'dataProvider'=>$dataProvider,
          'itemsTagName'=>'ul',
          'template'=>'{items}',
          'itemView'=>'_left',
        )); ?>  
      </ul>
    </div>
</div>

</section>
<!--Scripts dari external source-->
  <script src="<?php echo Yii::app()->baseUrl ?>/js/clock.js"></script>
  <script>
    var maplace = new Maplace({
      map_div: '#map-area'
      ,locations: [
        {
          lat: <?php echo $peserta->alamat_latitude ?>, 
          lon: <?php echo $peserta->alamat_longitude ?>,
          zoom: 16
        }]
      ,styles: {
        'Other style': 
        [
          {
            stylers: [
              { hue: "#00ffe6" },
              { saturation: -20 }]
          }, 
          {
            featureType: "road",
            elementType: "geometry",
            stylers: 
              [
                { lightness: 100 },
                { visibility: "simplified" }
              ]
          }, 
          {
          featureType: "road",
          elementType: "labels",
          stylers: 
            [
              { visibility: "off" }
            ]
          }],
        'Night': 
        [
          {
            featureType: 'all',
            stylers: 
            [
              { invert_lightness: 'true' }
            ]
          }],
        'Greyscale': 
        [
          {              
            featureType: 'all',
            stylers: 
            [
              { saturation: -100 },
              { gamma: 0.50 }
            ]
          }]}
      ,controls_on_map: false
    }); 
    maplace.Load(); 
    var currentHours = <?php echo date('H') ?>;
    var currentMinutes = <?php echo date('i') ?>;
    var currentSeconds = <?php echo date('s') ?>;
    function updateClock ( )
    {
      currentSeconds++;
      if (currentSeconds > 59) {
        currentSeconds = 0;
        currentMinutes++;
        if (currentMinutes > 59) {
          currentMinutes = 0;
          currentHours++;
          if (currentHours > 24) {
            // ajax
          }
        }
      }
      // Pad the minutes and seconds with leading zeros, if required
      currentMinutes = ( currentMinutes.length < 2 ? "0" : "" ) + currentMinutes;
      currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

      // Compose the string for display
      var currentTimeString = currentHours + ":" + currentMinutes;
      var currentSecondString = currentSeconds;
      
      // Update the time display
      document.getElementById("hour-minute").firstChild.nodeValue = currentTimeString;
      document.getElementById("second").firstChild.nodeValue = currentSecondString;
    }
    updateClock(); setInterval('updateClock()', 1000 );
  </script>

  
<!--Scripts END-->