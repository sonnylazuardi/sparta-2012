<h1>Tambahan Peserta</h1>

<?php echo $form->textAreaRow($model3,'motivasi', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'penyakit_pernah', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'penyakit_sedang', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model3,'golongan_darah', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model3,'nomor_kerabat', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model3,'nomor_ortu', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'kebersamaan', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'keaktifan', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'inisiatif', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'kesadaran_mahasiswa', array('class'=>'span6')); ?>
<?php echo $form->textAreaRow($model3,'kesadaran_bangsa', array('class'=>'span6')); ?>

<div class="form-actions">
	<a href="#profil" onclick="$('#profil').click()" class="button">Kembali</a>
	<button type="submit" class="success button">Simpan</button>
</div>