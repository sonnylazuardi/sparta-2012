<li class="<?php echo $data->kategori ?>">
	<?php 
	$myLink = "
		<h5 class=\"text-title\">
			".$data->judul."
		</h5>
		<label class=\"date\">
			".date('j F Y', strtotime($data->timestamp_published))."
		</label>
";
		echo CHtml::ajaxLink($myLink, 
		array('/post/view','id'=>$data->id), 
		array(
			'beforeSend' => 'function(){
				$("#post-content").addClass("loading");}',
			'success' => 'function(data){
				$("#post-content").removeClass("loading");
				$("#post-content").html(data);
			}',
		)); ?>
</li>
