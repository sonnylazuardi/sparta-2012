
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'post-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'judul',array('class'=>'span6','maxlength'=>100)); ?>

	<?php 
	$arrayKategori = array(
		'pengumuman'=>'pengumuman',
		'tugas'=>'tugas',
		'apresiasi'=>'apresiasi',
	);
	?>
	<?php echo $form->dropDownListRow($model,'kategori',$arrayKategori,array('class'=>'span6','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'deskripsi',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->dropDownListRow($model,'status',array(0=>'Hidden',1=>'Publish',2=>'HMIF Only'),array('class'=>'span6')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>

	</div>

<?php $this->endWidget(); ?>


<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/redactor.js"></script>

	<?php 
	$this->widget('ext.editor.editorku', array(
		'filespath' => Yii::app()->basePath."/../upload/images/ckk/",
		'filesurl' => Yii::app()->baseUrl."/upload/images/ckk/",
		'boleh' => !Yii::app()->user->IsGuest,
	));
 ?>
 