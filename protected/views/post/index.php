<div class="row">
	<div id="list-box" class="large-3 columns">
		<div class="text-container">
			<header>
				<span>Articles</span>
				<!--IF Admin-->
				<?php if (Yii::app()->user->checkAccess('admin')): ?>
					<a href="<?php echo Yii::app()->createUrl('/post/create') ?>"><span data-icon="&#xe004" aria-hidden="true"></span></a>
				<?php endif ?>
				<!--END IF-->
			</header>
			<ul id="sidebar-list">
				<?php $this->widget('bootstrap.widgets.TbListView',array(
					'dataProvider'=>$dataProvider,
					'itemsTagName'=>'ul',
					'template'=>'{items}',
					'itemView'=>'_left',
				)); ?>	
			</ul>
		</div>
	</div>
	<div id="article-box" class="large-9 columns">
		<div class="text-container full-container">
			<div id="post-content">
				<?php 
					$model = Post::model()->loadLastPost();
					$this->renderPartial('view', array('model'=>$model));
				 ?>
			</div>	 
		</div>
	</div>
</div>