<label><span id="article-category"><?php echo $data->kategori ?></span> / <span id="article-date"><?php echo date('Y/m/d', strtotime($data->timestamp_published)) ?></span></label>
<h1 id="article-title"><?php echo $data->judul ?></h1>

<div id="article-content">
	<?php echo $data->deskripsi ?>
</div>
</div>