<?php
$this->breadcrumbs=array(
	'Posts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Post','url'=>array('index')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
?>
<div class="row">
	<div class="well span6 offset3">
	<div class="text-container">
	<div id="text-content" class="content">
		<h1>Create Post</h1>

		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	</div>
 </div>
</div>
<center><a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="button">Kembali</a></center>