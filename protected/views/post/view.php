<div class="text-container full-container">
			<div id="post-content">
				
			
<header>
	<span>Articles</span>
	<?php if (Yii::app()->user->checkAccess('admin')): ?>
		<a href="<?php echo Yii::app()->createUrl('/post/update', array('id'=>$model->id)) ?>"><span data-icon="&#xe003" aria-hidden="true"></span></a>	
	<?php endif ?>
</header>
<div id="text-content"class="content">
	<label><span id="article-category"><?php echo $model->kategori ?></span> / <span id="article-date"><?php echo date('Y/m/d', strtotime($model->timestamp_published)) ?></span></label>
	<h1 id="article-title"><?php echo $model->judul ?></h1>

	<div id="article-content">
		<?php echo $model->deskripsi ?>
	</div>
</div>
</div>	 
		</div>