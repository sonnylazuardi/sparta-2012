<h1>Registrasi Akun</h1>
<?php echo $form->textFieldRow($model,'username', array('class'=>'span6')); ?>
<?php echo $form->passwordFieldRow($model,'password', array('class'=>'span6')); ?>
<?php echo $form->passwordFieldRow($model,'password2', array('class'=>'span6')); ?>

<?php if(CCaptcha::checkRequirements()): ?>
  <div>
  <?php $this->widget('CCaptcha'); ?>
  <?php echo $form->textFieldRow($model,'verifyCode',array( 'class' => 'span6')); ?>
  </div>
<?php endif; ?>

<div class="form-actions">
	<a href="#profil" onclick="$('#profil').click()" class="button">Lanjut</a>
</div>
