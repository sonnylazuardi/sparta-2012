<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div class="gadget-4">
	<div class="gadget-container">
		<header class="gadget-header center">
			<span>Error <?php echo $code; ?></span>
		</header>
		<div class="gadget-content center">
		<?php echo CHtml::encode($message); ?>
		</div>
	</div>
</div>
