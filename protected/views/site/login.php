<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
?>
<div class="row">
	<header class="large-12 center" id="sparta-header">
	  <div class="container">
	    <img id="sparta-logo" src="<?php echo Yii::app()->baseUrl ?>/images/logo.png" alt="Logo Sparta">
	    <span><h1 id="registrasi">LOG IN</h1></span>
	  </div>
	</header>
</div>

<div class="row center" id="sparta-register">

			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>
				
			<?php echo $form->textFieldRow($model,'username',array('class'=>'large-6 small-9 columns small-centered')); ?>

			<?php echo $form->passwordFieldRow($model,'password',array('class'=>'large-6 small-9 columns small-centered')); ?>

			<?php echo $form->checkBoxRow($model,'rememberMe'); ?>

			<div class="form-actions">
				<button type="submit" class="success">Login</button>
			</div>
				
			<?php $this->endWidget(); ?>
	
</div>
