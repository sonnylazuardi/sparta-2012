<!--header-->
	<!DOCTYPE html>
	<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

	<head>
		<meta charset="utf-8" />
	  <meta name="viewport" content="width=device-width" />
	  <title>SPARTA | Join The Troop</title>  
	  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/comingsoon.css" />
	  <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/custom.modernizr.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script> 
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fittext.js"></script>
	</head>
	<body>
<!--header END-->

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<div id="sparta-header">
  <div class="container">
	    <img id="sparta-logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="Logo Sparta">
	    <h1 id="sparta">SPARTA</h1>
	    <h2 id="hmif">HMIF 2012</h2>
	</div>
</div>

<script src="jquery.fittext.js"></script>
<script>
  $("#sparta").fitText(0.5, { minFontSize: '80px', maxFontSize: '160px' });
  $("#hmif").fitText(0.7, { minFontSize: '56.8px', maxFontSize: '113.6px' });
</script>
<!--footer-->	
  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? '<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/zepto' : '<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery') +
  '.js><\/script>')
  </script>
  
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.alerts.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.clearing.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.cookie.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.dropdown.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.forms.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.joyride.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.magellan.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.orbit.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.placeholder.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.reveal.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.section.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.tooltips.js"></script>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.topbar.js"></script>
	
  
  <script>
    $(document).foundation();
  </script>
	</body>
	</html>
<!--footer END-->