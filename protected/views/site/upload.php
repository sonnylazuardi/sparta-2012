<header id="sparta-header">
  <div class="container">
    <img id="sparta-logo" src="<?php echo Yii::app()->baseUrl ?>/images/logo.png" alt="Logo Sparta">
    <h1 id="upload">Upload Tugas</h1>
  </div>
</header>
 
<div class="row" id="sparta-register">
 <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'register-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
<div class="row">
<div class="well span6 offset3">
 <?php echo $form->errorSummary($model); ?>
 <?php echo $form->errorSummary($model2); ?>
 <?php echo $form->errorSummary($model3); ?>
<div class="section-container auto" data-section data-options="deep_linking: false">
  <section>
    <p class="title" data-section-title><a href="#akun" id="akun">Akun</a></p>
    <div class="content" data-section-content data-slug="akun">
      <?php $this->renderPartial('upload/_individu', array('form'=>$form, 'model'=>$model, 'model5'=>$model5)); ?>
    </div>
  </section>
  <section>
    <p class="title" data-section-title><a href="#profil" id="profil">Profil</a></p>
    <div class="content" data-section-content data-slug="profil"> 
      <?php $this->renderPartial('upload/_kelompok', array('form'=>$form, 'model'=>$model)); ?>
    </div>
  </section>
  <section>
    <p class="title" data-section-title><a href="#tambahan" id="tambahan">Tambahan</a></p>
    <div class="content" data-section-content data-slug="tambahan">
      <?php $this->renderPartial('upload/_angkatan', array('form'=>$form, 'model'=>$model)); ?>
    </div>
  </section>
</div>
</div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    function nextTab(elem) {
      $(elem + ' li.active')
        .next()
        .find('a[data-toggle="tab"]')
        .click();
    }
    function prevTab(elem) {
      $(elem + ' li.active')
        .prev()
        .find('a[data-toggle="tab"]')
        .click();
    }
    
</script>

</div><!-- page -->
