<h1>Profil Peserta</h1>
<?php echo $form->textFieldRow($model2,'nama_lengkap', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model2,'tempat_lahir', array('class'=>'span6','placeholder'=>'Bandung')); ?>
<?php echo $form->dateField($model2,'tanggal_lahir', array('class'=>'span6','placeholder'=>'mm/dd/yyyy')); ?>
<?php 
	$arrayJurusan = array(
		'Teknik Informatika'=>'Teknik Informatika',
		'Sistem Teknologi Informasi'=>'Sistem Teknologi Informasi',
	);
?>
<?php echo $form->dropDownListRow($model2,'jurusan', $arrayJurusan, array('class'=>'span6','placeholder'=>'Teknik Informatika')); ?>
<?php echo $form->textFieldRow($model2,'nim_stei', array('class'=>'span6','placeholder'=>'16511007')); ?>
<?php // echo $form->textFieldRow($model2,'nim_jurusan', array('class'=>'span6','placeholder'=>'13511007')); ?>

<?php //echo $form->textFieldRow($model2,'alamat_bandung', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model2,'alamat_rumah', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model2,'no_telp', array('class'=>'span6','placeholder'=>'08151231341')); ?>
<?php echo $form->textFieldRow($model2,'email', array('class'=>'span6')); ?>
<?php echo $form->textFieldRow($model2,'alamat', array('class'=>'span6')); ?>

<?php
	Yii::import('ext.LocationPicker.Location');
	$this->widget ( 'ext.LocationPicker.LocationWidget',
	array (
	  'model' => $model4,
	  'map_key' => 'AIzaSyD6F2jTe35JXiJMs7VyzZJ2sYUMGfEeQ_Q',
	));
?>

<div class="form-actions">
    <a href="#akun" onclick="$('#akun').click()" class="button">Kembali</a>
    <a href="#tambahan" onclick="$('#tambahan').click()" class="button">Lanjut</a>
</div>

