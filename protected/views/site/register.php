<header id="sparta-header">
  <div class="container">
    <img id="sparta-logo" src="<?php echo Yii::app()->baseUrl ?>/images/logo.png" alt="Logo Sparta">
    <h1 id="registrasi">REGISTRASI</h1>
  </div>
</header>
 
<div class="row" id="sparta-register">
 <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'register-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
<div class="row">
<div class="large-8 small-centered columns">
 <?php // echo $form->errorSummary($model); ?>
 <?php // echo $form->errorSummary($model2); ?>
 <?php // echo $form->errorSummary($model3); ?>
<div class="section-container auto" data-section data-options="deep_linking: false">
  <section>
    <p class="title" data-section-title><a href="#akun" id="akun">Akun</a></p>
    <div class="content" data-section-content data-slug="akun">
      <?php $this->renderPartial('_akun', array('form'=>$form, 'model'=>$model)); ?>
    </div>
  </section>
  <section>
    <p class="title" data-section-title><a href="#profil" id="profil">Profil</a></p>
    <div class="content" data-section-content data-slug="profil"> 
      <?php $this->renderPartial('_peserta', array('form'=>$form, 'model2'=>$model2,'model4'=>$model4)); ?>
    </div>
  </section>
  <section>
    <p class="title" data-section-title><a href="#tambahan" id="tambahan">Tambahan</a></p>
    <div class="content" data-section-content data-slug="tambahan">
      <?php $this->renderPartial('_tambahan', array('form'=>$form, 'model3'=>$model3)); ?>
    </div>
  </section>
</div>
</div>
</div>
<?php $this->endWidget(); ?>
  <script>
  $(function () {
    $("#registrasi").fitText(0.6, { minFontSize: '30px', maxFontSize: '160px' });
  });
  $(document).ready( function() {
    $('.gllpSearchButton').click();
  });
  </script>
</div><!-- page -->
