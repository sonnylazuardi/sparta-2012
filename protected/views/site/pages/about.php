<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>SPARTA 2013</h1>

<div class="row">
	<div class="span6">
		<table class="table table-bordered">
			<tr>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
			</tr>
			<tr>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
			</tr>
			<tr>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
			</tr>
			<tr>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
			</tr>
			<tr>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
				<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, saepe odit ipsam quia culpa voluptatem nihil mollitia nobis consequatur. Architecto, placeat, alias obcaecati nisi ex temporibus dolor voluptatibus provident nemo.</td>
			</tr>
		</table>
	</div>
	<div class="span6">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, aliquid, totam adipisci sequi nobis laboriosam explicabo veritatis illum repellat est corrupti nulla nesciunt ad repudiandae nam quia labore sit commodi.</p>
		
		<div class="well">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, vitae, eligendi, magnam natus sunt possimus reprehenderit id accusamus neque perferendis ad voluptas delectus facere nisi ducimus nesciunt quia maiores tempore?</p>
			<a href="#" class="btn btn-success btn-large">THIS IS SPARTA</a>
		</div>

		<div class="row">
			<div class="span3">
				<div class="well">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, vitae, eligendi, magnam natus sunt possimus reprehenderit id accusamus neque perferendis ad voluptas delectus facere nisi ducimus nesciunt quia maiores tempore?</p>
					<a href="#" class="btn btn-success btn-large">THIS IS SPARTA</a>
				</div>
			</div>
			<div class="span3">
				<div class="well">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, vitae, eligendi, magnam natus sunt possimus reprehenderit id accusamus neque perferendis ad voluptas delectus facere nisi ducimus nesciunt quia maiores tempore?</p>
					<a href="#" class="btn btn-success btn-large">THIS IS SPARTA</a>
				</div>
			</div>
		</div>
	</div>
</div>
