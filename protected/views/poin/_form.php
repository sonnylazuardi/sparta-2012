<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'poin-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Perhatikan bahwa anda tidak diperkenankan memilih Peserta dan Kelompok secara bersamaan (salah satu harus null).</p>
	<p class="help-block"><b>Keterangan:</b> <br>
	                                          Peserta dan Kelompok = "Tidak ada yang dipilih" -> Poin Angkatan <br>
                                              Kelompok tidak kosong -> Poin Kelompok <br>
                                              Peserta tidak kosong -> Poin Peserta / Individual </p>		
	<?php echo $form->errorSummary($model); ?>
	
	<?php 
	   $pesertaNameList = array();
	   $kelompokNameList = array();
	   
	   /* Initialize value and key */
	   $pesertaNameList[null] = "Tidak ada yang dipilih"; /* Default value */
	   $kelompokNameList[null] = "Tidak ada yang dipilih"; /* Default value */
	   $timestampList[null] = "Baru"; /* Default value */
	   
	   foreach ($pesertaProvider as &$peserta) { /* Iterate through all elements */
	      if (substr($peserta->nim_stei, 0, 5) == "16512") /* Showing NIM 16512* only */
	        $pesertaNameList[$peserta->id] = $peserta->nama_lengkap . " - " . $peserta->nim_stei;
	   }
	   
	   foreach ($kelompokProvider as &$kelompok) { /* Iterate through all elements */
	      $kelompokNameList[$kelompok->id] = $kelompok->nama;
	   }

	   $temptimestamp = "";
	   foreach ($timestampProvider as &$timestamp) { /* Iterate through all elements */
	      if ($temptimestamp != $timestamp->timestamp){ /* Ensure same timestamp is being shown only once */
	        $timestampList[$timestamp->timestamp] = $timestamp->timestamp;
		    $temptimestamp = $timestamp->timestamp;
		  }
	   }	 	   
	?>
	
	<?php echo $form->dropDownListRow($model,'id_peserta', $pesertaNameList , array('class'=>'span6','placeholder'=>'Kosong')); ?>

	<?php echo $form->dropDownListRow($model,'id_kelompok', $kelompokNameList , array('class'=>'span6','placeholder'=>'Kosong')); ?>
	
	<?php echo $form->textFieldRow($model,'deskripsi',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'poin',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'timestamp', $timestampList , array('class'=>'span6','placeholder'=>'Kosong')); ?>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	
	</div>
	
<?php $this->endWidget(); ?>
