<?php

$this->breadcrumbs=array(
	'Poin',
);
?>
<div class="gadget-4">
	<div class="gadget-container">
		<header class="gadget-header">
			<?php if (Yii::app()->user->checkAccess('admin')): ?>
					<a href="<?php echo Yii::app()->createUrl('/poin/create') ?>"><span data-icon="&#xe004" aria-hidden="true"></span></a>
			<?php endif ?>
			<span style="margin-left:0.2em">Poin Angkatan</span>
		</header>
		<div class="gadget-content">
			<p>
			<?php
				$this->Widget('ext.chartpoin.ChartPoinSparta', array('data'=>$data, 'start'=>$start))
			?>
		</p>
		</div>
	</div>
</div>

<div class="row">
	<?php $this->widget('bootstrap.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_list',
		'template' => "{items}",
	)); ?>
</div>



