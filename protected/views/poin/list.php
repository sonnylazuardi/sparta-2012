<?php
$this->breadcrumbs=array(
	'Poin'=>array('index'),
	'List',
);

?>
<div class="gadget-center">
	<div class="gadget-container">
		<div class="gadget-content">
			<h1>List Poin</h1>

			<div class="row">
			<?php $this->widget('bootstrap.widgets.TbListView',array(
				'dataProvider'=>$dataProvider,
				'itemView'=>'_list',
			)); ?>
			</div>
		</div>
	</div>
</div>

