<?php
$this->breadcrumbs=array(
	'Poin'=>array('index'),
	'Create',
);

?>
<div class="gadget-4">
	<div class="gadget-container">
		<div class="gadget-content">
		<h1>Create Poin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 
                                               'pesertaProvider'=>$pesertaProvider,
											   'kelompokProvider'=>$kelompokProvider,
											   'timestampProvider'=>$timestampProvider)); ?>	
											   
		</div>
	</div>
</div>
