<div class="list gadget-2">

<div class="gadget-container">
	<div class="gadget-content">
 	<p>
	<span><?php echo CHtml::encode($data->getAttributeLabel('id')); ?> : </span>
	<?php echo CHtml::encode($data->id) ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('id_peserta')); ?> : </span>
	<?php echo CHtml::encode($data->id_peserta); ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('id_kelompok')); ?> : </span>
	<?php echo CHtml::encode($data->id_kelompok); ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('penerima')); ?> : </span>
	<?php echo CHtml::encode($data->penerima); ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('deskripsi')); ?> : </span>
	<?php echo CHtml::encode($data->deskripsi); ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('poin')); ?> : </span>
	<?php echo CHtml::encode($data->poin); ?>
	<br />

	<span><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?> : </span>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />
	<?php if (Yii::app()->user->checkAccess('admin')): ?>
	<?php echo CHtml::link("Delete",array('delete','id'=>$data->id),array('class'=>'button alert')); ?>
    <?php endif ?>
    </p>
    </div>
</div>
</div>