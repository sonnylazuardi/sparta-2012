<div id="text-content"class="content">
	<label><span id="article-category"><?php echo $model->kategori ?></span> / <span id="article-date"><?php echo date('Y/m/d', strtotime($model->timestamp_published)) ?></span></label>
	<a href="<?php echo Yii::app()->createUrl('/post/index') ?>"><h1 id="article-title"><?php echo $model->judul ?></h1></a>

	<div id="article-content">
		<?php echo $model->deskripsi ?>
	</div>
</div>