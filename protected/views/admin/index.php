<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$admin =  Akun::model()->findByPk(Yii::app()->user->id);
?>

<section id="profile-admin" class="row">


<!--<center>
<?php
	//$this->Widget('ext.TimelineJS.SpartaTimeline', array('options' => $timelineData))
?>
       <div id="my-timeline"></div></center>
-->
  <?php $this->widget('TimelineWidget', array('timelineData'=>$timelineData)); ?>

  <div id="poin" class="gadget-2">
  <div class="gadget-container">
    <header class="gadget-header">

      <span>Poin Angkatan</span>
        <!--IF Admin-->
          <a href="<?php echo Yii::app()->createUrl('/poin/list') ?>"><span class="right" data-icon="&#xe00a" aria-hidden="true"></span></a>
          <a href="<?php echo Yii::app()->createUrl('/poin/create') ?>"><span  class="right" data-icon="&#xe004" aria-hidden="true"></span></a>
        <!--END IF-->
    </header>
    <div class="gadget-content">
      <p>
      <?php
        $this->Widget('ext.chartpoin.ChartPoinSparta', array('data'=>$data, 'start'=>$start))
      ?>
    </p>
    </div>
  </div>
</div>
<div class="gadget-2">
    <div class="text-container">
      <header class="gadget-header">
        <span>Articles</span>
        <!--IF Admin-->
          <a href="<?php echo Yii::app()->createUrl('/post/create') ?>"><span class="right" data-icon="&#xe004" aria-hidden="true"></span></a>
        <!--END IF-->
      </header>
      <ul id="sidebar-list">
        <?php $this->widget('bootstrap.widgets.TbListView',array(
          'dataProvider'=>$dataProvider,
          'itemsTagName'=>'ul',
          'template'=>'{items}',
          'itemView'=>'_left',
        )); ?>  
      </ul>
    </div>
</div>
</section>
<!--Scripts dari external source-->
  <script src="<?php echo Yii::app()->baseUrl ?>/js/clock.js"></script>
  <script>
    var currentHours = <?php echo date('H') ?>;
    var currentMinutes = <?php echo date('i') ?>;
    var currentSeconds = <?php echo date('s') ?>;
    function updateClock ( )
    {
      currentSeconds++;
      if (currentSeconds > 59) {
        currentSeconds = 0;
        currentMinutes++;
        if (currentMinutes > 59) {
          currentMinutes = 0;
          currentHours++;
          if (currentHours > 24) {
            // ajax
          }
        }
      }
      // Pad the minutes and seconds with leading zeros, if required
      currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
      currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

      // Compose the string for display
      var currentTimeString = currentHours + ":" + currentMinutes;
      var currentSecondString = currentSeconds;
      
      // Update the time display
      document.getElementById("hour-minute").firstChild.nodeValue = currentTimeString;
      document.getElementById("second").firstChild.nodeValue = currentSecondString;
    }
    updateClock(); setInterval('updateClock()', 1000 );
  </script>