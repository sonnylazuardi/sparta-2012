<?php
/* @var $this TugasController */
/* @var $model Tugas */
/* @var $form CActiveForm */ 
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipe_pengumpulan_tugas'); ?>
		<?php echo $form->textField($model,'tipe_pengumpulan_tugas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deskripsi'); ?>
		<?php echo $form->textArea($model,'deskripsi',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'timestamp_published'); ?>
		<?php echo $form->textField($model,'timestamp_published'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'timestamp_modified'); ?>
		<?php echo $form->textField($model,'timestamp_modified'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time_deadline'); ?>
		<?php echo $form->textField($model,'time_deadline'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->