<?php
/* @var $this TugasController */ 
/* @var $model Tugas */

$this->breadcrumbs=array(
	'Tugases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tugas', 'url'=>array('index')),
	array('label'=>'Manage Tugas', 'url'=>array('admin')),
);
?>

<h1>Create Tugas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>