<?php
/* @var $this TugasController */
/* @var $data Tugas */
?> 

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('judul')); ?>:</b>
	<?php echo CHtml::encode($data->judul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_pengumpulan_tugas')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_pengumpulan_tugas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deskripsi')); ?>:</b>
	<?php echo CHtml::encode($data->deskripsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp_published')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp_published); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp_modified')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp_modified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_deadline')); ?>:</b>
	<?php echo CHtml::encode($data->time_deadline); ?>
	<br />
	<div class="form-actions">
		<?php echo CHtml::link('Submit Tugas', 
					array('pesertatugas/create', 'id'=>$data->id), 
					array('class'=>'success button')); ?>
	</div>
	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>