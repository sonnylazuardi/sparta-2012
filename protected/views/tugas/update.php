<?php
/* @var $this TugasController */
/* @var $model Tugas */
 
$this->breadcrumbs=array(
	'Tugases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tugas', 'url'=>array('index')),
	array('label'=>'Create Tugas', 'url'=>array('create')),
	array('label'=>'View Tugas', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Tugas', 'url'=>array('admin')),
);
?>

<h1>Update Tugas <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>