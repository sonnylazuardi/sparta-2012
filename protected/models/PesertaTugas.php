<?php
  
/**
 * This is the model class for table "peserta_tugas".
 *
 * The followings are the available columns in table 'peserta_tugas':
 * @property integer $id
 * @property integer $id_tugas
 * @property integer $id_peserta
 * @property string $link
 * @property string $timestamp
 * @property string $pranala_tugas_unggah
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Tugas $idTugas
 * @property Peserta $idPeserta
 */
class PesertaTugas extends CActiveRecord
{
	public $uploadedfile;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PesertaTugas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peserta_tugas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tugas, id_peserta','required'),
			array('id_tugas, id_peserta, status', 'numerical', 'integerOnly'=>true),
			array('link', 'length', 'max'=>128),
			array('pranala_tugas_unggah', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_tugas, id_peserta, link, timestamp, pranala_tugas_unggah, status', 'safe', 'on'=>'search'),
			array('uploadedfile','file','types'=>'zip','safe'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTugas' => array(self::BELONGS_TO, 'Tugas', 'id_tugas'),
			'idPeserta' => array(self::BELONGS_TO, 'Peserta', 'id_peserta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_tugas' => 'Id Tugas',
			'id_peserta' => 'Id Peserta',
			'link' => 'Link',
			'timestamp' => 'Timestamp',
			'pranala_tugas_unggah' => 'Pranala Tugas Unggah',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_tugas',$this->id_tugas);
		$criteria->compare('id_peserta',$this->id_peserta);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('pranala_tugas_unggah',$this->pranala_tugas_unggah,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}