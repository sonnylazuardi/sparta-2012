<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id
 * @property string $judul
 * @property string $deskripsi
 * @property string $timestamp_published
 * @property string $timestamp_modified
 * @property integer $status
 */
class Post extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, deskripsi, status, kategori', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('judul', 'length', 'max'=>100),
			array('kategori', 'length', 'max'=>15),
			array('timestamp_modified', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, judul, deskripsi, kategori, timestamp_published, timestamp_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'deskripsi' => 'Deskripsi',
			'timestamp_published' => 'Timestamp Published',
			'timestamp_modified' => 'Timestamp Modified',
			'status' => 'Status',
			'kategori' => 'Kategori',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('timestamp_published',$this->timestamp_published,true);
		$criteria->compare('timestamp_modified',$this->timestamp_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->timestamp_published=$this->timestamp_modified=date("Y-m-d H:i:s");
			}
			else
				$this->timestamp_modified=date("Y-m-d H:i:s");
			return true;
		}
		else
			return false;
	}
	public function loadLastPost()
	{
		$model = Post::model()->find(array('order'=>'id desc'));
		return $model;
	}
}