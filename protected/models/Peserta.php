<?php

/**
 * This is the model class for table "peserta".
 *
 * The followings are the available columns in table 'peserta':
 * @property integer $id
 * @property integer $id_akun
 * @property integer $id_kelompok
 * @property string $nama_lengkap
 * @property string $nama_panggilan
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $jurusan
 * @property integer $nim_stei
 * @property integer $nim_jurusan
 * @property string $alamat
 * @property string $no_telp
 * @property string $penyakit
 *
 * The followings are the available model relations:
 * @property Inbox[] $inboxes
 * @property Akun $idAkun
 * @property Kelompok $idKelompok
 * @property PesertaTugas[] $pesertaTugases
 * @property Poin[] $poins
 */
class Peserta extends CActiveRecord
{
	public $profpic;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Peserta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peserta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_akun, nama_lengkap, tempat_lahir, tanggal_lahir, nim_stei, alamat_rumah, no_telp, email, alamat', 'required'),
			array('id_akun, id_kelompok, nim_stei, nim_jurusan', 'numerical', 'integerOnly'=>true),
			array('nama_lengkap, jurusan, nama_panggilan, alamat, alamat_latitude, alamat_longitude, alamat_zoom, alamat_rumah', 'length', 'max'=>100),
			array('nim_stei', 'unique', 'className'=>'Peserta'),
			array('email', 'email'),
			array('tempat_lahir', 'length', 'max'=>50),
			array('no_telp', 'length', 'max'=>20),
			array('penyakit', 'safe'),
			array('profpic', 'file', 'allowEmpty' => true, 'types' => 'jpg','safe'=>true, 'maxSize' => 1024 * 800),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_akun, id_kelompok, nama_lengkap, nama_panggilan, tempat_lahir, tanggal_lahir, jurusan, nim_stei, nim_jurusan, alamat, no_telp, penyakit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inboxes' => array(self::HAS_MANY, 'Inbox', 'id_peserta'),
			'akun' => array(self::BELONGS_TO, 'Akun', 'id_akun'),
			'tambahan' => array(self::HAS_ONE, 'Tambahan', 'id_peserta'),
			'kelompok' => array(self::BELONGS_TO, 'Kelompok', 'id_kelompok'),
			'pesertaTugas' => array(self::HAS_MANY, 'PesertaTugas', 'id_peserta'),
			'poins' => array(self::HAS_MANY, 'Poin', 'id_peserta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_akun' => 'Id Akun',
			'id_kelompok' => 'Id Kelompok',
			'nama_lengkap' => 'Nama Lengkap',
			'nama_panggilan' => 'Nama Panggilan',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'jurusan' => 'Jurusan',
			'nim_stei' => 'Nim Stei',
			'nim_jurusan' => 'Nim Jurusan',
			'no_telp' => 'Nomor HP',
			'penyakit' => 'Penyakit',
			'alamat' => 'Alamat Bandung',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_akun',$this->id_akun);
		$criteria->compare('id_kelompok',$this->id_kelompok);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('nama_panggilan',$this->nama_panggilan,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('jurusan',$this->jurusan);
		$criteria->compare('nim_stei',$this->nim_stei);
		$criteria->compare('nim_jurusan',$this->nim_jurusan);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('penyakit',$this->penyakit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getProfpicImage()
	{
		$path = '/upload/images/profpic/'.$this->nim_stei.'.jpg';
		if (file_exists(getcwd().$path))
			return Yii::app()->baseUrl.$path;
		else 
			return Yii::app()->baseUrl.'/images/default.png';
			//return 'http://www.gravatar.com/avatar/'.md5(strtolower($this->email)).'?s=300&r=G'; 
	}

}