<?php

/**
 * This is the model class for table "tambahan".
 *
 * The followings are the available columns in table 'tambahan':
 * @property integer $id
 * @property integer $id_peserta
 * @property string $motivasi
 * @property string $penyakit_pernah
 * @property string $penyakit_sedang
 * @property string $golongan_darah
 * @property string $nomor_kerabat
 * @property string $nomor_ortu
 * @property string $kebersamaan
 * @property string $keaktifan
 * @property string $inisiatif
 * @property string $kesadaran_mahasiswa
 * @property string $kesadaran_bangsa
 *
 * The followings are the available model relations:
 * @property Peserta $idPeserta
 */
class Tambahan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tambahan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tambahan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_peserta, motivasi, penyakit_pernah, penyakit_sedang, golongan_darah, nomor_ortu, kebersamaan, keaktifan, inisiatif, kesadaran_mahasiswa, kesadaran_bangsa', 'required'),
			array('id_peserta', 'numerical', 'integerOnly'=>true),
			array('golongan_darah', 'length', 'max'=>2),
			array('nomor_kerabat, nomor_ortu', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_peserta, motivasi, penyakit_pernah, penyakit_sedang, golongan_darah, nomor_kerabat, nomor_ortu, kebersamaan, keaktifan, inisiatif, kesadaran_mahasiswa, kesadaran_bangsa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'peserta' => array(self::BELONGS_TO, 'Peserta', 'id_peserta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_peserta' => 'Id Peserta',
			'motivasi' => 'Motivasi mengikuti Sparta',
			'penyakit_pernah' => 'Penyakit yang Pernah Dialami',
			'penyakit_sedang' => 'Penyakit yang Sedang Dialami',
			'golongan_darah' => 'Golongan Darah',
			'nomor_kerabat' => 'Nomor Darurat (Teman Dekat di Luar Peserta/Ibu Kos)',
			'nomor_ortu' => 'Nomor Orang Tua',
			'kebersamaan' => 'Bagaimana kebersamaan angkatan kamu? Jelaskan, dan beri nilai dalam skala 1-10 (1 Buruk - 10 Baik)',
			'keaktifan' => 'Bagaimana keaktifan angkatan kamu ? Jelaskan, dan beri nilai dalam skala 1-10 (1 Buruk - 10 Baik)',
			'inisiatif' => 'Bagaimana inisiatif angkatan kamu ? Jelaskan, dan beri nilai dalam skala 1-10 (1 Buruk - 10 Baik)',
			'kesadaran_mahasiswa' => 'Bagaimana kesadaran angkatan kamu terhadap kemahasiswaan terpusat? Jelaskan dan beri nilai dalam skala 1-10 (1 Buruk - 10 Baik)',
			'kesadaran_bangsa' => 'Bagaimana kesadaran angkatan kamu terhadap masalah bangsa dan masyarakat? Jelaskan dan beri nilai dalam skala 1-10 (1 Buruk - 10 Baik)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_peserta',$this->id_peserta);
		$criteria->compare('motivasi',$this->motivasi,true);
		$criteria->compare('penyakit_pernah',$this->penyakit_pernah,true);
		$criteria->compare('penyakit_sedang',$this->penyakit_sedang,true);
		$criteria->compare('golongan_darah',$this->golongan_darah,true);
		$criteria->compare('nomor_kerabat',$this->nomor_kerabat,true);
		$criteria->compare('nomor_ortu',$this->nomor_ortu,true);
		$criteria->compare('kebersamaan',$this->kebersamaan,true);
		$criteria->compare('keaktifan',$this->keaktifan,true);
		$criteria->compare('inisiatif',$this->inisiatif,true);
		$criteria->compare('kesadaran_mahasiswa',$this->kesadaran_mahasiswa,true);
		$criteria->compare('kesadaran_bangsa',$this->kesadaran_bangsa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}