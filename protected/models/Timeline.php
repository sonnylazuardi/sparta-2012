<?php

/**
 * This is the model class for table "timeline".
 *
 * The followings are the available columns in table 'timeline':
 * @property integer $id
 * @property string $judul
 * @property string $deskripsi
 * @property string $timestamp_published
 * @property string $timestamp_modified
 * @property integer $status
 */
class Timeline extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Timeline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'timeline';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, deskripsi, timestamp_published', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('judul', 'length', 'max'=>100),
			array('timestamp_modified', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, judul, deskripsi, timestamp_published, timestamp_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'deskripsi' => 'Deskripsi',
			'timestamp_published' => 'Timestamp Published',
			'timestamp_modified' => 'Timestamp Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('timestamp_published',$this->timestamp_published,true);
		$criteria->compare('timestamp_modified',$this->timestamp_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* Timestamp modifier */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
		    /* timestamp_modified is now being used to store timestamp_published timestamp */
		    $this->timestamp_modified=date("Y-m-d H:i:s");
			return true;
		}
		else
			return false;
	}
}