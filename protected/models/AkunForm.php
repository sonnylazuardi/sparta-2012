<?php
/**
 * Register form model
 */
class AkunForm extends CFormModel
{
	public $username;
	public $password;
	public $password2;
	public $verifyCode;
	
	public function rules()
	{
		return array(
			array('username', 'match', 'allowEmpty' => false, 'pattern' => '/[A-Za-z0-9\x80-\xFF]+$/'),
			array('username', 'length', 'max'=>50),
			array('username', 'unique', 'className' => 'Akun' ),
			array('username, password, password2', 'required'),
			array('username, password, password2', 'length', 'min' => 6, 'max' => 32),
			array('password2', 'compare', 'compareAttribute'=>'password'),
			array('password2, verifyCode, ', 'safe'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}
	
	/**
	 * Attribute values
	 *
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Username',
			'password' => 'Password',
			'password2' => 'Ulangi Password',
		);
	}
	
}