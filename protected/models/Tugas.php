<?php
 
/**
 * This is the model class for table "tugas".
 *
 * The followings are the available columns in table 'tugas':
 * @property integer $id
 * @property string $judul
 * @property integer $tipe_pengumpulan_tugas
 * @property string $deskripsi
 * @property string $timestamp_published
 * @property string $timestamp_modified
 * @property string $time_deadline
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property PesertaTugas[] $pesertaTugases
 */
class Tugas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tugas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tugas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, tipe_pengumpulan_tugas, deskripsi, time_deadline, status', 'required'),
			array('tipe_pengumpulan_tugas, status', 'numerical', 'integerOnly'=>true),
			array('judul', 'length', 'max'=>100),
			array('timestamp_modified', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, judul, tipe_pengumpulan_tugas, deskripsi, timestamp_published, timestamp_modified, time_deadline, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pesertaTugases' => array(self::HAS_MANY, 'PesertaTugas', 'id_tugas'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'tipe_pengumpulan_tugas' => 'Tipe Pengumpulan Tugas',
			'deskripsi' => 'Deskripsi',
			'timestamp_published' => 'Timestamp Published',
			'timestamp_modified' => 'Timestamp Modified',
			'time_deadline' => 'Time Deadline',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('tipe_pengumpulan_tugas',$this->tipe_pengumpulan_tugas);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('timestamp_published',$this->timestamp_published,true);
		$criteria->compare('timestamp_modified',$this->timestamp_modified,true);
		$criteria->compare('time_deadline',$this->time_deadline,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* Timestamp modifier */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->timestamp_published=$this->timestamp_modified=date("Y-m-d H:i:s");
			}
			else
				$this->timestamp_modified=date("Y-m-d H:i:s");
			return true;
		}
		else
			return false;
	}
}