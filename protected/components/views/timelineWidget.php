<div class="gadget-4">
 <div class="gadget-container">
    <header class="gadget-header">
      <span>Timeline</span>
    </header>
    <div class="gadget-content">
      <ul id='timeline'>
	   <?php
	        $total = 0;
			function percentage($val1, $val2, $precision) 
			{
				$res = round( ($val1 / $val2) * 100, $precision );
				
				return $res;
			}
			foreach ($timelineData as &$row)
			   $total = $total + 1;
			   
	        $counter = 0;
	        if (Yii::app()->user->checkAccess('admin')){
				$total+=1;
			}
			if ($total == 0) $width = 100;
			  else $width = 100/($total);
			if ($total <= 4)
				$tag='h4';
			elseif ($total <= 6)
				$tag='h5';
			else
				$tag='h6';
	         foreach ($timelineData as &$row){
			   $counter = $counter + 1;
			   		
				   echo "<li class='timeline-entry' style='width:$width%'>";
				   if (Yii::app()->user->checkAccess('admin'))
				   	echo "<input class='timeline-radio' id='timeline-trigger".$counter."' name='trigger' type='radio'>";
					 else
					echo "<input checked='checked' class='timeline-radio' id='timeline-trigger".$counter."' name='trigger' type='radio'>";
				   

				   echo "<label for='timeline-trigger".$counter."'>";
				   $click_link = Yii::app()->createUrl('', array('timeline'=>$row->id));
				   echo "<a href=\"".$click_link."\">";	
				   echo "<$tag class=\"center\">". $row->judul ."</$tag>";
				   echo "</a>";
				   echo "</label>";
				   echo " <span class='timeline-date'>". date_format(date_create($row->timestamp_published), 'd F Y') ."</span>";
				   echo " <span class='timeline-circle'></span>";
				   echo "</li>";
			 }
	    ?>
	    <?php if (Yii::app()->user->checkAccess('admin')): ?>
	    <li class="timeline-entry" style="width:<?php echo $width.'%' ?>">
	    <input checked='checked' class="timeline-radio" id="timeline-trigger<?php echo $counter+1;?>" name="trigger" type="radio">
		    <label class="narrow" for="timeline-trigger<?php echo $counter+1;?>">
		    	<a href="<?php echo Yii::app()->createUrl('/timeline/create') ?>">
		    		<span data-icon="&#xe004" aria-hidden="true"></span>
		    	</a>
		    </label> 
		    <span class="timeline-date">Add New</span> 
	    <span class="timeline-circle"></span></li>
	    <?php endif ?>
      </ul>
    </div>
  </div>
</div>