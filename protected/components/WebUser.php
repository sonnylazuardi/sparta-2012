<?php
/**
 * Overload of CWebUser to set some more methods.
 */
class WebUser extends CWebUser
{
	public function getRole() {
	}
	public function checkAccess($operation, $params=array())
  {
      if (empty($this->id)) {
          // Not identified => no rights
          return false;
      }
      $role = $this->getState("role");
      if ($role === 'admin') {
          return true; // admin role has access to everything
      }
    	return ($operation === $role); // allow access if the operation request is the current user's role
  }
}