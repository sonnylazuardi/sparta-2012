<?php
/*
 * TimelineWidget class file.
 * Showing Timeline dynamically
 */

 class TimelineWidget extends CWidget
 {
    public $timelineData;
	
	public function run()
	{
        $this->render('timelineWidget', array('timelineData'=>$this->timelineData));
	}
 }

?>