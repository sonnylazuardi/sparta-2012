<?php
/**
 * SpartaTimeline class file.
 * Converting TimelineJS to Yii Widget
 *
 * @author Iskandar Setiadi <iskandarsetiadi.students.itb.ac.id>
 * @email : iskandarsetiadi@students.itb.ac.id
 * Half of the content is modified from JSInputWidget by K. O. Frimpong
 */
 
 class SpartaTimeline extends CWidget{
 
   /**
     * @var array the initial JavaScript options that should be passed to the UI.
     */
    public $options = array();

    /**
     * @var array the HTML attributes that should be rendered in the HTML tag representing the UI widget.
     */
    public $htmlOptions = array();
 
     /**
     * @var CModel the data model associated with this widget.
     */
    public $model;

    /**
     * @var string the attribute associated with this widget.
     * The name can contain square brackets (e.g. 'name[1]') which is used to collect tabular data input.
     */
    public $attribute;

    /**
     * @var string the input name. This must be set if {@link model} is not set.
     */
    public $name;

    /**
     * @var string the input value
     */
    public $value;
 
     /**
     * @return array the name and the ID of the input.
     */
    protected function resolveNameID() {
        if ($this->name !== null)
            $name = $this->name;
        else if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];
        else if ($this->hasModel())
            $name = CHtml::activeName($this->model, $this->attribute);
        else
            throw new CException(Yii::t('zii', '{class} must specify "model" and "attribute" or "name" property values.', array('{class}' => get_class($this))));

        if (($id = $this->getId(false)) === null) {
            if (isset($this->htmlOptions['id']))
                $id = $this->htmlOptions['id'];
            else
                $id = CHtml::getIdByName($name);
        }

        return array($name, $id);
    }

    /**
     * @return boolean whether this widget is associated with a data model.
     */
    protected function hasModel() {
        return $this->model instanceof CModel && $this->attribute !== null;
    }
	
	/**
	 * Renders the widget.
	 */
	public function run() {
		$id = $this->getId();
		$this->htmlOptions['id'] = $id;

		echo CHtml::openTag('div', $this->htmlOptions);
		echo CHtml::closeTag('div');

		// check if options parameter is a json string
		if(is_string($this->options)) {
			if(!$this->options = CJSON::decode($this->options))
				throw new CException('The options parameter is not valid JSON.');
			// TODO translate exception message
		}

		$jsOptions = CJavaScript::encode($this->options);
		/* Test JSON data - manually */
		$script="   
		var story_jsonp_data = {
			\"timeline\":
			{
				\"headline\":\"SPARTA HMIF 2012\",
				\"type\":\"default\",
				\"text\":\"Timeline & Informasi penting mengenai SPARTA HMIF 2012\",
				\"startDate\":\"2013,6,15\",
				\"date\": [
					{
						\"startDate\":\"2013,6,20\",
						\"headline\":\"Day 0\",
						\"text\":\"<p>Day 0 SPARTA</p>\",
						\"asset\":
						{
							\"media\":\"http://youtu.be/u4XpeU9erbg\",
							\"credit\":\"\",
							\"caption\":\"<p>Pada hari yang cerah ini, bla bla bla.</p>\"
						}
					},
            {
                \"startDate\":\"2013,6,22\",
                \"headline\":\"Day 1\",
                \"text\":\"<p>Day 1 SPARTA</p>\",
                \"asset\":
                {
                    \"media\":\"http://en.wikipedia.org/wiki/A_Trip_to_the_Moon\",
                    \"credit\":\"\",
                    \"caption\":\"Pada hari yang cerah ini, bla bla bla.\"
                }
            },
            {
                \"startDate\":\"2013,6,24\",
                \"headline\":\"Day 2\",
                \"text\":\"<p>Day 2 SPARTA</p>\",
                \"asset\":
                {
                    \"media\":\"https://maps.google.com/maps/ms?msid=218289454544166038899.0004e04ae14f35f8786e7&msa=0&ll=-6.89293,107.611009&spn=0.001616,0.002476&iwloc=0004e04ae1518e3b3d0ec\",
                    \"credit\":\"\",
                    \"caption\":\"Pada hari yang cerah ini, bla bla bla.\"
                }
            }			
				   
				]
			}
		}
		$(document).ready(function() {
                    createStoryJS({
                    type:       'timeline',
                    width:      '950',
                    height:     '550',
                    source:     story_jsonp_data,
                    embed_id:   'my-timeline'
                });
            });";
		$this->registerScripts(__CLASS__ . '#' . $id, $script);
	}

		/**
	 * Publishes and registers the necessary script files.
	 *
	 * @param string the id of the script to be inserted into the page
	 * @param string the embedded script to be inserted into the page
	 */
	protected function registerScripts($id, $embeddedScript) {
		$cssbasePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR;
		$jsbasePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR;
		$cssbaseUrl = Yii::app()->getAssetManager()->publish($cssbasePath, false, 1, YII_DEBUG);
		$jsbaseUrl = Yii::app()->getAssetManager()->publish($jsbasePath, false, 1, YII_DEBUG);
		
		/* GetClientScript */
		$cs = Yii::app()->getClientScript();

        /* Register All */		
		$csscriptFile = YII_DEBUG ? '/timeline.css' : '/timeline.css';
		$jsonescriptFile = YII_DEBUG ? '/timeline-min.js' : '/timeline-min.js';
		$jstwoscriptFile = YII_DEBUG ? '/storyjs-embed.js' : '/storyjs-embed.js';
			
		$cs->registerCssFile($cssbaseUrl . $csscriptFile);	
		$cs->registerScriptFile($jsbaseUrl . $jsonescriptFile);
		$cs->registerScriptFile($jsbaseUrl . $jstwoscriptFile);		
		
		$cs->registerScript($id, $embeddedScript, CClientScript::POS_HEAD);
	}
} 
 ?>