<?php

class ChartPoinSparta extends CWidget {

	public $data = array();
	public $start = 0;

	
	public function init(){
		$totalpoin = 0;
		foreach ($this->data as $row){
			$totalpoin += $row->getAttribute('poin');
		}
		echo '<h1 id="total-poin">';
		echo $totalpoin;
		echo '<span> Poin</span>	</h1>';
	}
	public function run(){
		$is_admin = Yii::app()->user->checkAccess('admin');
		if ($is_admin)
			$deleteUrl =  Yii::app()->createUrl('/poin/list');
		$deleteClass = "right";
		$poins = array();
		$timestamps = array();
		$descs = array();
		
		$total = $this->start;
		$lasttime = "";
		$lastdesc = "";
		$n = 1;
		
		foreach ($this->data as $row){	
			$curr = $row->getAttribute('timestamp');
			if($lasttime=="") $lasttime = $curr;
			
			if($lasttime!=$curr){
				array_push($poins, $total);
				array_push($timestamps, $lasttime);
				$descs[$lasttime] = $lastdesc;
				$lastdesc = "";
				$lasttime = $curr;
				$n = 1;
			}
			$poin = intval($row->getAttribute('poin'));
			$sign = "+";
			if ($poin <= 0) $sign = "";
			
			$total += $poin;
			$lastdesc .= "<hr/>".($n).". ";
			
			if($row->getAttribute('id_peserta')!=null){
				$lastdesc .= "Peserta : ".$row->idPeserta->getAttribute('nama_lengkap')." - ";
			}elseif($row->getAttribute('id_kelompok')!=null){
				$lastdesc .= "Kelompok : ".$row->idKelompok->getAttribute('nama')." - ";
			} else $lastdesc .= "Angkatan - ";
			
			$lastdesc .= $row->getAttribute('deskripsi')." <h4>(".$sign.$poin." poin)</h4>";
			$n++;
		}
		
		array_push($poins, $total);
		array_push($timestamps, $lasttime);
		$descs[$lasttime] = $lastdesc;
		
		$descobj = "";
		$n = 0;
		foreach ($descs as $key=>$value){
			$descobj .= "'".$key."' : '".$value."'";
			$n++;
			if ($n < count($descs)){
				$descobj .=",";
			}
		}
		
		$this->Widget('ext.highcharts.HighchartsWidget', array(
		   'options'=>array(
			  'title' => array('text' => ''),
			  'chart' => array('backgroundColor' => null),
			  'credits' => array('enabled' => false),
			  'xAxis' => array(
				 'categories' => $timestamps
			  ),
			  'yAxis' => array(
				 'title' => array('text' => ' ')
			  ),
			  'series' => array(
				 array('name' => 'Poin', 'data' => $poins)
			  ),
			  'tooltip' => array(
					'formatter' => 'js:function(){
										data = {'.$descobj.'};
											return  "<h4>" + this.x + "</h4>" + "<p>"+data[this.x]+"</p>";
									}',
					'useHTML'=>true				
			  ),
			  'plotOptions' => array(
			  		'line' => array(
			  			'color'=> '#FEFEFE'
			  			)
			  	),
		   )
		));
	}
}

?>