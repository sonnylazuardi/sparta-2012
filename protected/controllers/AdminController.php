<?php

class AdminController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex() {
		$condition='';
		
		if(isset($_GET['after'])) {
			//belom
			$start = 0; //ambil dari query
		}else{
			$start = 0;
		}
		
		if(isset($_GET['before'])) {
			//belom
		}
		$dataProvider=new CActiveDataProvider('Post', array('criteria'=>array('order'=>'id desc'),'pagination'=>false));
		$data = Poin::model()->with('idPeserta', 'idKelompok')->findAll(array('order'=>'timestamp', 'condition'=>$condition));
		$timelineData = Timeline::model()->findAll(array('order'=>'timestamp_published'));
		$this->render('index', array('data'=>$data, 'start'=>$start,'dataProvider'=>$dataProvider, 'timelineData'=>$timelineData,));
	}
}