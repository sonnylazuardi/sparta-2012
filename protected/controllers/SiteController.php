<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xE17818,
				'foreColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if (Yii::app()->user->checkAccess('admin'))
			$this->redirect(array('/admin/index'));
		else if (Yii::app()->user->checkAccess('peserta'))
			$this->redirect(array('/peserta/index'));
		else 
			$this->redirect(array('login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/orange';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionRegister()
	{
		$this->layout = '//layouts/orange';
	    $model = new AkunForm;
	    $model2 = new Peserta;
	    $model3 = new Tambahan;
	    $model4 = new Location();

	    if(isset($_POST['AkunForm']))
	    {
	      $model->attributes=$_POST['AkunForm'];
	      $model2->attributes = $_POST['Peserta'];
	      $model3->attributes = $_POST['Tambahan'];
	      $model4->attributes = $_POST['Location'];
	     	if ($model->validate()) {
	     		$model1 = new Akun;
	     		$model1->scenario = 'register';
	     		$model1->attributes=$_POST['AkunForm'];
		      $model1->role = 'peserta';
		      $model1->salt = $model1->generateSalt();
		      $model1->password = $model1->hashPassword($model1->password, $model1->salt);
		      $model1->status = 1;
		      $model1->timestamp = date('Y-m-d H:i:s');
		      if($model1->save())
		      {	
						$model2->id_akun = $model1->id;
						$model2->alamat_latitude = $model4->latitude;
						$model2->alamat_longitude = $model4->longitude;
						$model2->alamat_zoom = $model4->zoom;
						if ($model2->save()) {
							$model3->id_peserta = $model2->id;
							if ($model3->save()) {
								Yii::app()->user->setFlash('success', '<strong>Selamat!</strong> Anda berhasil mendaftar');
			      		$this->redirect(Yii::app()->user->returnUrl);
			      	} else {$model2->delete(); $model1->delete();}
						} else $model1->delete();
		      }
		    }
	    }
	    $this->render('register',array('model'=>$model, 'model2'=>$model2, 'model3'=>$model3, 'model4'=>$model4));
	}
	public function actionUpload()
	{
		$this->layout = '//layouts/orange';
	    $model = new AkunForm;
	    $model2 = new Peserta;
	    $model3 = new Tambahan;
	    $model4 = new Location();
		$model5 = new Timeline;
	    if(isset($_POST['AkunForm']))
	    {
	      $model->attributes=$_POST['AkunForm'];
	      $model2->attributes = $_POST['Peserta'];
	      $model3->attributes = $_POST['Tambahan'];
	      $model4->attributes = $_POST['Location'];
	     	if ($model->validate()) {
	     		$model1 = new Akun;
	     		$model1->scenario = 'register';
	     		$model1->attributes=$_POST['AkunForm'];
		      $model1->role = 'peserta';
		      $model1->salt = $model1->generateSalt();
		      $model1->password = $model1->hashPassword($model1->password, $model1->salt);
		      $model1->status = 1;
		      if($model1->save())
		      {	
						$model2->id_akun = $model1->id;
						$model2->alamat_latitude = $model4->latitude;
						$model2->alamat_longitude = $model4->longitude;
						if ($model2->save()) {
							$model3->id_peserta = $model2->id;
							if ($model3->save()) {
								Yii::app()->user->setFlash('success', '<strong>Selamat!</strong> Anda berhasil mendaftar silakan login dengan akun Anda.');
			      		$this->redirect(Yii::app()->user->returnUrl);
			      	} else {$model2->delete(); $model1->delete();}
						} else $model1->delete();
		      }
		    }
	    }
	    $this->render('upload',array('model'=>$model, 'model2'=>$model2, 'model3'=>$model3, 'model4'=>$model4, 'model5' =>$model5));
		}

	public function actionComingsoon()
	{
		// Yii::app()->user->setFlash('success', '<strong>Selamat!</strong> Anda berhasil mendaftar silakan login dengan akun Anda.');
		$this->renderPartial('comingsoon');
	}
}
