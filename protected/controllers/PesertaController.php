<?php

class PesertaController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function accessRules()
	{
		return array(

			array('allow',
				'actions'=>array('view','list'),
				'users'=>array('*'), //saat udah fix, ubah jadi user all
			),

			array('allow',
				'actions'=>array('index', 'profil'),
				'roles'=>array('peserta'),
			),
			array('allow', 
				'actions'=>array('report'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex() {
		$condition='';
		
		if(isset($_GET['after'])) {
			//belom
			$start = 0; //ambil dari query
		}else{
			$start = 0;
		}
		
		if(isset($_GET['before'])) {
			//belom
		}
		$dataProvider=new CActiveDataProvider('Post', array('criteria'=>array('order'=>'id desc', 'condition'=>'status = 1'),'pagination'=>false));
		$data = Poin::model()->with('idPeserta', 'idKelompok')->findAll(array('order'=>'timestamp', 'condition'=>$condition));
		$timelineData = Timeline::model()->findAll(array('order'=>'timestamp_published'));
		$this->render('index', array('data'=>$data, 'start'=>$start,'dataProvider'=>$dataProvider, 'timelineData'=>$timelineData,));
		}
	public function actionReport() {
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$c = new CDbCriteria;
		$c->order = 'nama_lengkap asc';
		$model = Peserta::model()->findAll($c);
		$data[1] = array('id','nama_lengkap','tempat_lahir','tanggal_lahir','jurusan','nim_stei','no_telp','email','motivasi','penyakit_pernah','penyakit_sedang','golongan_darah','nomor_kerabat','nomor_ortu','kebersamaan','keaktifan','inisiatif','kesadaran_mahasiswa','kesadaran_bangsa');
		$ctr = 2;
		foreach ($model as $peserta) {
			if ($peserta->tambahan) {
				$data[$ctr] = array(
						$peserta->id,
						$peserta->nama_lengkap,
						$peserta->tempat_lahir,
						$peserta->tanggal_lahir,
						$peserta->jurusan,
						$peserta->nim_stei,
						$peserta->no_telp,
						$peserta->email,
						$peserta->tambahan->motivasi,
						$peserta->tambahan->penyakit_pernah,
						$peserta->tambahan->penyakit_sedang,
						$peserta->tambahan->golongan_darah,
						$peserta->tambahan->nomor_kerabat,
						$peserta->tambahan->nomor_ortu,
						$peserta->tambahan->kebersamaan,
						$peserta->tambahan->keaktifan,
						$peserta->tambahan->inisiatif,
						$peserta->tambahan->kesadaran_mahasiswa, $peserta->tambahan->kesadaran_bangsa,
				);
			}
			$ctr++;
		}
		$xls = new JPhpExcel('UTF-8', false, 'Data Peserta');
		$xls->addArray($data);
		$xls->generateXML('data_sparta');
		exit;
	}
	public function actionList(){
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$c = new CDbCriteria;
		$c->order = 'nama_lengkap asc';
		$c->addCondition('nim_stei like "16512%"');
		$model = Peserta::model()->findAll($c);
		$this->render('list',array('model'=>$model));	
	}
	public function actionView($id)
	{
			$this->render('view',array('model'=>$this->loadModel($id),));
	}
	public function loadModel($id)
	{
		$model=Peserta::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionProfil()
	{
		$this->layout = '//layouts/column1';

    $model2 = Peserta::model()->findByAttributes(array('id_akun'=>Yii::app()->user->id));
    if ($model2 == null) {
    	throw new CHttpException(404, "Profil tidak lengkap atau tidak ditemukan");
    }
    $model4 = new Location();
   	$model4->latitude = $model2->alamat_latitude;
		$model4->longitude = $model2->alamat_longitude;
		$model4->zoom = $model2->alamat_zoom;
    if(isset($_POST['Peserta']))
    {
      $model2->attributes = $_POST['Peserta'];
      $model4->attributes = $_POST['Location'];
      $model2->setScenario('profil');	
      if ($model2->validate()) {
	      $model2->profpic = CUploadedFile::getInstance($model2, 'profpic');
				$model2->alamat_latitude = $model4->latitude;
				$model2->alamat_longitude = $model4->longitude;
				$model2->alamat_zoom = $model4->zoom;
				if ($model2->save()) {
					if (is_object($model2->profpic)) {
						$path = getcwd().'/upload/images/profpic/'.$model2->nim_stei.'.jpg';
						$model2->profpic->saveAs($path);
						Yii::app()->phpThumb->makeThumb($path, 245, 240);
					}
					Yii::app()->user->setFlash('success', '<strong>Selamat!</strong> Profil berhasil diupdate');
	      	$this->redirect(Yii::app()->user->returnUrl);
				} 
			}
    }
    $this->render('profil',array('model2'=>$model2, 'model4'=>$model4));
	}
	public function actionUpload(){
    $file = CUploadedFile::getInstanceByName('file');
    // Do your business ... save on file system for example,
    // and/or do some db operations for example
    $nim_stei = Akun::model()->findByPk(Yii::app()->user->id)->peserta->nim_stei;
    $file->saveAs('upload/images/profpic/'.$nim_stei.'.jpg');
    // return the new file path
    echo Yii::app()->baseUrl.'/images/'.$file->getName();
	}
}