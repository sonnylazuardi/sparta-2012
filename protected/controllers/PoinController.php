<?php

class PoinController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create', 'update', 'admin', and 'delete' actions
				'actions'=>array('create','update','admin','delete', 'list'),
				'roles'=>array('admin'),
			),
			array('allow', // allow authenticated user to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
		$this->actionView();
	}
	
	public function actionView()
	{
		$condition='';
		
		if(isset($_GET['after'])) {
			//belom
			$start = 0; //ambil dari query
		}else{
			$start = 0;
		}
		
		if(isset($_GET['before'])) {
			//belom
		}

		$dataProvider=new CActiveDataProvider('Poin');
		$data = Poin::model()->with('idPeserta', 'idKelompok')->findAll(array('order'=>'timestamp', 'condition'=>$condition));
		$this->render('view', array('data'=>$data, 'dataProvider'=>$dataProvider,'start'=>$start));
	}
	
	/*
	 * Create a new Poin record.
	 */
	 public function actionCreate()
	 {
		$model=new Poin;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Poin']))
		{
			$model->attributes=$_POST['Poin'];
			$model->penerima = 1; /* Lazy to alter database */
			if (($model->id_peserta == 0) || ($model->id_kelompok == 0)){
			  if($model->save())
				$this->redirect(array('view'));
			}
		}
		
		/* Providing lists of Peserta */
		$c = new CDbCriteria;
		$c->order = 'nama_lengkap asc';
		$pesertaProvider = Peserta::model()->findAll($c);
		
		/* Providing lists of Kelompok */
		$c->order = 'nama asc';
		$kelompokProvider = Kelompok::model()->findAll($c);
		
		/* Providing lists of Timestamp */
		$c->order = 'id desc';
		$timestampProvider = Poin::model()->findAll($c);

		$this->render('create',array(
			'model'=>$model,
			'pesertaProvider'=>$pesertaProvider,
			'kelompokProvider'=>$kelompokProvider,
			'timestampProvider'=>$timestampProvider,
		));	    
	 }
	 
	 /**
	 * Lists all models.
	 */
	public function actionList()
	{
		$dataProvider=new CActiveDataProvider('Poin');
		$this->render('list',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'list' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	        /* Additional note : This is unsafe scheme of deleting entries */
			$this->loadModel($id)->delete();

			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('list'));

	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Poin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


}